import { __decorate } from "tslib";
import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnChanges, OnInit, Output, SimpleChange, SimpleChanges, ViewChild, DoCheck, IterableDiffer, IterableDiffers, KeyValueDiffer, KeyValueDiffers, } from '@angular/core';
import { PlotlyService } from '../plotly.service';
// @dynamic
let PlotComponent = class PlotComponent {
    constructor(plotly, iterableDiffers, keyValueDiffers) {
        this.plotly = plotly;
        this.iterableDiffers = iterableDiffers;
        this.keyValueDiffers = keyValueDiffers;
        this.defaultClassName = 'js-plotly-plot';
        this.revision = 0;
        this.debug = false;
        this.useResizeHandler = false;
        this.updateOnLayoutChange = true;
        this.updateOnDataChange = true;
        this.updateOnlyWithRevision = false;
        this.initialized = new EventEmitter();
        this.update = new EventEmitter();
        this.purge = new EventEmitter();
        this.error = new EventEmitter();
        this.afterExport = new EventEmitter();
        this.afterPlot = new EventEmitter();
        this.animated = new EventEmitter();
        this.animatingFrame = new EventEmitter();
        this.animationInterrupted = new EventEmitter();
        this.autoSize = new EventEmitter();
        this.beforeExport = new EventEmitter();
        this.buttonClicked = new EventEmitter();
        this.click = new EventEmitter();
        this.plotly_click = new EventEmitter();
        this.clickAnnotation = new EventEmitter();
        this.deselect = new EventEmitter();
        this.doubleClick = new EventEmitter();
        this.framework = new EventEmitter();
        this.hover = new EventEmitter();
        this.legendClick = new EventEmitter();
        this.legendDoubleClick = new EventEmitter();
        this.relayout = new EventEmitter();
        this.restyle = new EventEmitter();
        this.redraw = new EventEmitter();
        this.selected = new EventEmitter();
        this.selecting = new EventEmitter();
        this.sliderChange = new EventEmitter();
        this.sliderEnd = new EventEmitter();
        this.sliderStart = new EventEmitter();
        this.transitioning = new EventEmitter();
        this.transitionInterrupted = new EventEmitter();
        this.unhover = new EventEmitter();
        this.relayouting = new EventEmitter();
        this.treemapclick = new EventEmitter();
        this.sunburstclick = new EventEmitter();
        this.eventNames = ['afterExport', 'afterPlot', 'animated', 'animatingFrame', 'animationInterrupted', 'autoSize',
            'beforeExport', 'buttonClicked', 'clickAnnotation', 'deselect', 'doubleClick', 'framework', 'hover',
            'legendClick', 'legendDoubleClick', 'relayout', 'restyle', 'redraw', 'selected', 'selecting', 'sliderChange',
            'sliderEnd', 'sliderStart', 'transitioning', 'transitionInterrupted', 'unhover', 'relayouting', 'treemapclick',
            'sunburstclick'];
    }
    ngOnInit() {
        this.createPlot().then(() => {
            const figure = this.createFigure();
            this.initialized.emit(figure);
        });
        if (this.plotly.debug && this.click.observers.length > 0) {
            const msg = 'DEPRECATED: Reconsider using `(plotly_click)` instead of `(click)` to avoid event conflict. '
                + 'Please check https://github.com/plotly/angular-plotly.js#FAQ';
            console.error(msg);
        }
    }
    ngOnDestroy() {
        if (typeof this.resizeHandler === 'function') {
            this.getWindow().removeEventListener('resize', this.resizeHandler);
            this.resizeHandler = undefined;
        }
        const figure = this.createFigure();
        this.purge.emit(figure);
        PlotlyService.remove(this.plotlyInstance);
    }
    ngOnChanges(changes) {
        let shouldUpdate = false;
        const revision = changes.revision;
        if (revision && !revision.isFirstChange()) {
            shouldUpdate = true;
        }
        const debug = changes.debug;
        if (debug && !debug.isFirstChange()) {
            shouldUpdate = true;
        }
        if (shouldUpdate) {
            this.updatePlot();
        }
        this.updateWindowResizeHandler();
    }
    ngDoCheck() {
        if (this.updateOnlyWithRevision) {
            return false;
        }
        let shouldUpdate = false;
        if (this.updateOnLayoutChange) {
            if (this.layoutDiffer) {
                const layoutHasDiff = this.layoutDiffer.diff(this.layout);
                if (layoutHasDiff) {
                    shouldUpdate = true;
                }
            }
            else if (this.layout) {
                this.layoutDiffer = this.keyValueDiffers.find(this.layout).create();
            }
            else {
                this.layoutDiffer = undefined;
            }
        }
        if (this.updateOnDataChange) {
            if (this.dataDiffer) {
                const dataHasDiff = this.dataDiffer.diff(this.data);
                if (dataHasDiff) {
                    shouldUpdate = true;
                }
            }
            else if (Array.isArray(this.data)) {
                this.dataDiffer = this.iterableDiffers.find(this.data).create(this.dataDifferTrackBy);
            }
            else {
                this.dataDiffer = undefined;
            }
        }
        if (shouldUpdate && this.plotlyInstance) {
            this.updatePlot();
        }
    }
    getWindow() {
        return window;
    }
    getClassName() {
        let classes = [this.defaultClassName];
        if (Array.isArray(this.className)) {
            classes = classes.concat(this.className);
        }
        else if (this.className) {
            classes.push(this.className);
        }
        return classes.join(' ');
    }
    createPlot() {
        return this.plotly.newPlot(this.plotEl.nativeElement, this.data, this.layout, this.config, this.frames).then(plotlyInstance => {
            this.plotlyInstance = plotlyInstance;
            this.getWindow().gd = this.debug ? plotlyInstance : undefined;
            this.eventNames.forEach(name => {
                const eventName = `plotly_${name.toLowerCase()}`;
                plotlyInstance.on(eventName, (data) => this[name].emit(data));
            });
            plotlyInstance.on('plotly_click', (data) => {
                this.click.emit(data);
                this.plotly_click.emit(data);
            });
            this.updateWindowResizeHandler();
        }, err => {
            console.error('Error while plotting:', err);
            this.error.emit(err);
        });
    }
    createFigure() {
        const p = this.plotlyInstance;
        const figure = {
            data: p.data,
            layout: p.layout,
            frames: p._transitionData ? p._transitionData._frames : null
        };
        return figure;
    }
    updatePlot() {
        if (!this.plotlyInstance) {
            const error = new Error(`Plotly component wasn't initialized`);
            this.error.emit(error);
            throw error;
        }
        const layout = Object.assign({}, this.layout);
        return this.plotly.update(this.plotlyInstance, this.data, layout, this.config, this.frames).then(() => {
            const figure = this.createFigure();
            this.update.emit(figure);
        }, err => {
            console.error('Error while updating plot:', err);
            this.error.emit(err);
        });
    }
    updateWindowResizeHandler() {
        if (this.useResizeHandler) {
            if (this.resizeHandler === undefined) {
                this.resizeHandler = () => this.plotly.resize(this.plotlyInstance);
                this.getWindow().addEventListener('resize', this.resizeHandler);
            }
        }
        else {
            if (typeof this.resizeHandler === 'function') {
                this.getWindow().removeEventListener('resize', this.resizeHandler);
                this.resizeHandler = undefined;
            }
        }
    }
    dataDifferTrackBy(_, item) {
        const obj = Object.assign({}, item, { uid: '' });
        return JSON.stringify(obj);
    }
};
PlotComponent.ctorParameters = () => [
    { type: PlotlyService },
    { type: IterableDiffers },
    { type: KeyValueDiffers }
];
__decorate([
    ViewChild('plot', { static: true })
], PlotComponent.prototype, "plotEl", void 0);
__decorate([
    Input()
], PlotComponent.prototype, "data", void 0);
__decorate([
    Input()
], PlotComponent.prototype, "layout", void 0);
__decorate([
    Input()
], PlotComponent.prototype, "config", void 0);
__decorate([
    Input()
], PlotComponent.prototype, "frames", void 0);
__decorate([
    Input()
], PlotComponent.prototype, "style", void 0);
__decorate([
    Input()
], PlotComponent.prototype, "divId", void 0);
__decorate([
    Input()
], PlotComponent.prototype, "revision", void 0);
__decorate([
    Input()
], PlotComponent.prototype, "className", void 0);
__decorate([
    Input()
], PlotComponent.prototype, "debug", void 0);
__decorate([
    Input()
], PlotComponent.prototype, "useResizeHandler", void 0);
__decorate([
    Input()
], PlotComponent.prototype, "updateOnLayoutChange", void 0);
__decorate([
    Input()
], PlotComponent.prototype, "updateOnDataChange", void 0);
__decorate([
    Input()
], PlotComponent.prototype, "updateOnlyWithRevision", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "initialized", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "update", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "purge", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "error", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "afterExport", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "afterPlot", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "animated", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "animatingFrame", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "animationInterrupted", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "autoSize", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "beforeExport", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "buttonClicked", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "click", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "plotly_click", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "clickAnnotation", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "deselect", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "doubleClick", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "framework", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "hover", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "legendClick", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "legendDoubleClick", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "relayout", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "restyle", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "redraw", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "selected", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "selecting", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "sliderChange", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "sliderEnd", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "sliderStart", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "transitioning", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "transitionInterrupted", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "unhover", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "relayouting", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "treemapclick", void 0);
__decorate([
    Output()
], PlotComponent.prototype, "sunburstclick", void 0);
PlotComponent = __decorate([
    Component({
        selector: 'plotly-plot',
        template: `<div #plot [attr.id]="divId" [className]="getClassName()" [ngStyle]="style"></div>`,
        providers: [PlotlyService]
    })
], PlotComponent);
export { PlotComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGxvdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hbmd1bGFyLXBsb3RseS5qcy8iLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3Bsb3QvcGxvdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFDSCxTQUFTLEVBQ1QsVUFBVSxFQUNWLFlBQVksRUFDWixLQUFLLEVBQ0wsU0FBUyxFQUNULFNBQVMsRUFDVCxNQUFNLEVBQ04sTUFBTSxFQUNOLFlBQVksRUFDWixhQUFhLEVBQ2IsU0FBUyxFQUNULE9BQU8sRUFDUCxjQUFjLEVBQ2QsZUFBZSxFQUNmLGNBQWMsRUFDZCxlQUFlLEdBQ2xCLE1BQU0sZUFBZSxDQUFDO0FBRXZCLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUdsRCxXQUFXO0FBTVgsSUFBYSxhQUFhLEdBQTFCLE1BQWEsYUFBYTtJQXFFdEIsWUFDVyxNQUFxQixFQUNyQixlQUFnQyxFQUNoQyxlQUFnQztRQUZoQyxXQUFNLEdBQU4sTUFBTSxDQUFlO1FBQ3JCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUNoQyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUF2RWpDLHFCQUFnQixHQUFHLGdCQUFnQixDQUFDO1FBZ0JyQyxhQUFRLEdBQVcsQ0FBQyxDQUFDO1FBRXJCLFVBQUssR0FBWSxLQUFLLENBQUM7UUFDdkIscUJBQWdCLEdBQVksS0FBSyxDQUFDO1FBRWxDLHlCQUFvQixHQUFHLElBQUksQ0FBQztRQUM1Qix1QkFBa0IsR0FBRyxJQUFJLENBQUM7UUFDMUIsMkJBQXNCLEdBQUcsS0FBSyxDQUFDO1FBRTlCLGdCQUFXLEdBQUcsSUFBSSxZQUFZLEVBQWlCLENBQUM7UUFDaEQsV0FBTSxHQUFHLElBQUksWUFBWSxFQUFpQixDQUFDO1FBQzNDLFVBQUssR0FBRyxJQUFJLFlBQVksRUFBaUIsQ0FBQztRQUMxQyxVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQVMsQ0FBQztRQUVsQyxnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDakMsY0FBUyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDL0IsYUFBUSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDOUIsbUJBQWMsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3BDLHlCQUFvQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDMUMsYUFBUSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDOUIsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2xDLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNuQyxVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzQixpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDbEMsb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3JDLGFBQVEsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzlCLGdCQUFXLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNqQyxjQUFTLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMvQixVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzQixnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDakMsc0JBQWlCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN2QyxhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM5QixZQUFPLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM3QixXQUFNLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM1QixhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM5QixjQUFTLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMvQixpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDbEMsY0FBUyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDL0IsZ0JBQVcsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2pDLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNuQywwQkFBcUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzNDLFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzdCLGdCQUFXLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNqQyxpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDbEMsa0JBQWEsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRXRDLGVBQVUsR0FBRyxDQUFDLGFBQWEsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLGdCQUFnQixFQUFFLHNCQUFzQixFQUFFLFVBQVU7WUFDN0csY0FBYyxFQUFFLGVBQWUsRUFBRSxpQkFBaUIsRUFBRSxVQUFVLEVBQUUsYUFBYSxFQUFFLFdBQVcsRUFBRSxPQUFPO1lBQ25HLGFBQWEsRUFBRSxtQkFBbUIsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLGNBQWM7WUFDNUcsV0FBVyxFQUFFLGFBQWEsRUFBRSxlQUFlLEVBQUUsdUJBQXVCLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBRSxjQUFjO1lBQzlHLGVBQWUsQ0FBQyxDQUFDO0lBTWpCLENBQUM7SUFFTCxRQUFRO1FBQ0osSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDeEIsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQ25DLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2xDLENBQUMsQ0FBQyxDQUFDO1FBR0gsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3RELE1BQU0sR0FBRyxHQUFHLDhGQUE4RjtrQkFDcEcsOERBQThELENBQUM7WUFDckUsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUN0QjtJQUNMLENBQUM7SUFFRCxXQUFXO1FBQ1AsSUFBSSxPQUFPLElBQUksQ0FBQyxhQUFhLEtBQUssVUFBVSxFQUFFO1lBQzFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGFBQW9CLENBQUMsQ0FBQztZQUMxRSxJQUFJLENBQUMsYUFBYSxHQUFHLFNBQVMsQ0FBQztTQUNsQztRQUVELE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN4QixhQUFhLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsV0FBVyxDQUFDLE9BQXNCO1FBQzlCLElBQUksWUFBWSxHQUFHLEtBQUssQ0FBQztRQUV6QixNQUFNLFFBQVEsR0FBaUIsT0FBTyxDQUFDLFFBQVEsQ0FBQztRQUNoRCxJQUFJLFFBQVEsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsRUFBRTtZQUN2QyxZQUFZLEdBQUcsSUFBSSxDQUFDO1NBQ3ZCO1FBRUQsTUFBTSxLQUFLLEdBQWlCLE9BQU8sQ0FBQyxLQUFLLENBQUM7UUFDMUMsSUFBSSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLEVBQUU7WUFDakMsWUFBWSxHQUFHLElBQUksQ0FBQztTQUN2QjtRQUVELElBQUksWUFBWSxFQUFFO1lBQ2QsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQ3JCO1FBRUQsSUFBSSxDQUFDLHlCQUF5QixFQUFFLENBQUM7SUFDckMsQ0FBQztJQUVELFNBQVM7UUFDTCxJQUFJLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtZQUM3QixPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUVELElBQUksWUFBWSxHQUFHLEtBQUssQ0FBQztRQUV6QixJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUMzQixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ25CLE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDMUQsSUFBSSxhQUFhLEVBQUU7b0JBQ2YsWUFBWSxHQUFHLElBQUksQ0FBQztpQkFDdkI7YUFDSjtpQkFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ3BCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQ3ZFO2lCQUFNO2dCQUNILElBQUksQ0FBQyxZQUFZLEdBQUcsU0FBUyxDQUFDO2FBQ2pDO1NBQ0o7UUFFRCxJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUN6QixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ2pCLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEQsSUFBSSxXQUFXLEVBQUU7b0JBQ2IsWUFBWSxHQUFHLElBQUksQ0FBQztpQkFDdkI7YUFDSjtpQkFBTSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNqQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7YUFDekY7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7YUFDL0I7U0FDSjtRQUVELElBQUksWUFBWSxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDckMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQ3JCO0lBQ0wsQ0FBQztJQUVELFNBQVM7UUFDTCxPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBRUQsWUFBWTtRQUNSLElBQUksT0FBTyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFFdEMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUMvQixPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDNUM7YUFBTSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDdkIsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDaEM7UUFFRCxPQUFPLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUVELFVBQVU7UUFDTixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUU7WUFDMUgsSUFBSSxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUM7WUFDckMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztZQUU5RCxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDM0IsTUFBTSxTQUFTLEdBQUcsVUFBVSxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQztnQkFDakQsY0FBYyxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxJQUFTLEVBQUUsRUFBRSxDQUFFLElBQUksQ0FBQyxJQUFJLENBQXdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDL0YsQ0FBQyxDQUFDLENBQUM7WUFFSCxjQUFjLENBQUMsRUFBRSxDQUFDLGNBQWMsRUFBRSxDQUFDLElBQVMsRUFBRSxFQUFFO2dCQUM1QyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDakMsQ0FBQyxDQUFDLENBQUM7WUFFSCxJQUFJLENBQUMseUJBQXlCLEVBQUUsQ0FBQztRQUNyQyxDQUFDLEVBQUUsR0FBRyxDQUFDLEVBQUU7WUFDTCxPQUFPLENBQUMsS0FBSyxDQUFDLHVCQUF1QixFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFlBQVk7UUFDUixNQUFNLENBQUMsR0FBUSxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQ25DLE1BQU0sTUFBTSxHQUFrQjtZQUMxQixJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUk7WUFDWixNQUFNLEVBQUUsQ0FBQyxDQUFDLE1BQU07WUFDaEIsTUFBTSxFQUFFLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJO1NBQy9ELENBQUM7UUFFRixPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBRUQsVUFBVTtRQUNOLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3RCLE1BQU0sS0FBSyxHQUFHLElBQUksS0FBSyxDQUFDLHFDQUFxQyxDQUFDLENBQUM7WUFDL0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdkIsTUFBTSxLQUFLLENBQUM7U0FDZjtRQUVELE1BQU0sTUFBTSxxQkFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFaEMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDbEcsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQ25DLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzdCLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRTtZQUNMLE9BQU8sQ0FBQyxLQUFLLENBQUMsNEJBQTRCLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDekIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQseUJBQXlCO1FBQ3JCLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ3ZCLElBQUksSUFBSSxDQUFDLGFBQWEsS0FBSyxTQUFTLEVBQUU7Z0JBQ2xDLElBQUksQ0FBQyxhQUFhLEdBQUcsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUNuRSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxhQUFvQixDQUFDLENBQUM7YUFDMUU7U0FDSjthQUFNO1lBQ0gsSUFBSSxPQUFPLElBQUksQ0FBQyxhQUFhLEtBQUssVUFBVSxFQUFFO2dCQUMxQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxhQUFvQixDQUFDLENBQUM7Z0JBQzFFLElBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDO2FBQ2xDO1NBQ0o7SUFDTCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsQ0FBUyxFQUFFLElBQVM7UUFDbEMsTUFBTSxHQUFHLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDakQsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQy9CLENBQUM7Q0FFSixDQUFBOztZQTlLc0IsYUFBYTtZQUNKLGVBQWU7WUFDZixlQUFlOztBQWhFUjtJQUFsQyxTQUFTLENBQUMsTUFBTSxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDOzZDQUFvQjtBQUU3QztJQUFSLEtBQUssRUFBRTsyQ0FBc0I7QUFDckI7SUFBUixLQUFLLEVBQUU7NkNBQWlDO0FBQ2hDO0lBQVIsS0FBSyxFQUFFOzZDQUFpQztBQUNoQztJQUFSLEtBQUssRUFBRTs2Q0FBbUM7QUFDbEM7SUFBUixLQUFLLEVBQUU7NENBQW1DO0FBRWxDO0lBQVIsS0FBSyxFQUFFOzRDQUFnQjtBQUNmO0lBQVIsS0FBSyxFQUFFOytDQUFzQjtBQUNyQjtJQUFSLEtBQUssRUFBRTtnREFBK0I7QUFDOUI7SUFBUixLQUFLLEVBQUU7NENBQXdCO0FBQ3ZCO0lBQVIsS0FBSyxFQUFFO3VEQUFtQztBQUVsQztJQUFSLEtBQUssRUFBRTsyREFBNkI7QUFDNUI7SUFBUixLQUFLLEVBQUU7eURBQTJCO0FBQzFCO0lBQVIsS0FBSyxFQUFFOzZEQUFnQztBQUU5QjtJQUFULE1BQU0sRUFBRTtrREFBaUQ7QUFDaEQ7SUFBVCxNQUFNLEVBQUU7NkNBQTRDO0FBQzNDO0lBQVQsTUFBTSxFQUFFOzRDQUEyQztBQUMxQztJQUFULE1BQU0sRUFBRTs0Q0FBbUM7QUFFbEM7SUFBVCxNQUFNLEVBQUU7a0RBQWtDO0FBQ2pDO0lBQVQsTUFBTSxFQUFFO2dEQUFnQztBQUMvQjtJQUFULE1BQU0sRUFBRTsrQ0FBK0I7QUFDOUI7SUFBVCxNQUFNLEVBQUU7cURBQXFDO0FBQ3BDO0lBQVQsTUFBTSxFQUFFOzJEQUEyQztBQUMxQztJQUFULE1BQU0sRUFBRTsrQ0FBK0I7QUFDOUI7SUFBVCxNQUFNLEVBQUU7bURBQW1DO0FBQ2xDO0lBQVQsTUFBTSxFQUFFO29EQUFvQztBQUNuQztJQUFULE1BQU0sRUFBRTs0Q0FBNEI7QUFDM0I7SUFBVCxNQUFNLEVBQUU7bURBQW1DO0FBQ2xDO0lBQVQsTUFBTSxFQUFFO3NEQUFzQztBQUNyQztJQUFULE1BQU0sRUFBRTsrQ0FBK0I7QUFDOUI7SUFBVCxNQUFNLEVBQUU7a0RBQWtDO0FBQ2pDO0lBQVQsTUFBTSxFQUFFO2dEQUFnQztBQUMvQjtJQUFULE1BQU0sRUFBRTs0Q0FBNEI7QUFDM0I7SUFBVCxNQUFNLEVBQUU7a0RBQWtDO0FBQ2pDO0lBQVQsTUFBTSxFQUFFO3dEQUF3QztBQUN2QztJQUFULE1BQU0sRUFBRTsrQ0FBK0I7QUFDOUI7SUFBVCxNQUFNLEVBQUU7OENBQThCO0FBQzdCO0lBQVQsTUFBTSxFQUFFOzZDQUE2QjtBQUM1QjtJQUFULE1BQU0sRUFBRTsrQ0FBK0I7QUFDOUI7SUFBVCxNQUFNLEVBQUU7Z0RBQWdDO0FBQy9CO0lBQVQsTUFBTSxFQUFFO21EQUFtQztBQUNsQztJQUFULE1BQU0sRUFBRTtnREFBZ0M7QUFDL0I7SUFBVCxNQUFNLEVBQUU7a0RBQWtDO0FBQ2pDO0lBQVQsTUFBTSxFQUFFO29EQUFvQztBQUNuQztJQUFULE1BQU0sRUFBRTs0REFBNEM7QUFDM0M7SUFBVCxNQUFNLEVBQUU7OENBQThCO0FBQzdCO0lBQVQsTUFBTSxFQUFFO2tEQUFrQztBQUNqQztJQUFULE1BQU0sRUFBRTttREFBbUM7QUFDbEM7SUFBVCxNQUFNLEVBQUU7b0RBQW9DO0FBN0RwQyxhQUFhO0lBTHpCLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxhQUFhO1FBQ3ZCLFFBQVEsRUFBRSxvRkFBb0Y7UUFDOUYsU0FBUyxFQUFFLENBQUMsYUFBYSxDQUFDO0tBQzdCLENBQUM7R0FDVyxhQUFhLENBb1B6QjtTQXBQWSxhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgICBDb21wb25lbnQsXG4gICAgRWxlbWVudFJlZixcbiAgICBFdmVudEVtaXR0ZXIsXG4gICAgSW5wdXQsXG4gICAgT25EZXN0cm95LFxuICAgIE9uQ2hhbmdlcyxcbiAgICBPbkluaXQsXG4gICAgT3V0cHV0LFxuICAgIFNpbXBsZUNoYW5nZSxcbiAgICBTaW1wbGVDaGFuZ2VzLFxuICAgIFZpZXdDaGlsZCxcbiAgICBEb0NoZWNrLFxuICAgIEl0ZXJhYmxlRGlmZmVyLFxuICAgIEl0ZXJhYmxlRGlmZmVycyxcbiAgICBLZXlWYWx1ZURpZmZlcixcbiAgICBLZXlWYWx1ZURpZmZlcnMsXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBQbG90bHlTZXJ2aWNlIH0gZnJvbSAnLi4vcGxvdGx5LnNlcnZpY2UnO1xuaW1wb3J0IHsgUGxvdGx5IH0gZnJvbSAnLi4vcGxvdGx5LmludGVyZmFjZSc7XG5cbi8vIEBkeW5hbWljXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3Bsb3RseS1wbG90JyxcbiAgICB0ZW1wbGF0ZTogYDxkaXYgI3Bsb3QgW2F0dHIuaWRdPVwiZGl2SWRcIiBbY2xhc3NOYW1lXT1cImdldENsYXNzTmFtZSgpXCIgW25nU3R5bGVdPVwic3R5bGVcIj48L2Rpdj5gLFxuICAgIHByb3ZpZGVyczogW1Bsb3RseVNlcnZpY2VdLFxufSlcbmV4cG9ydCBjbGFzcyBQbG90Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMsIE9uRGVzdHJveSwgRG9DaGVjayB7XG4gICAgcHJvdGVjdGVkIGRlZmF1bHRDbGFzc05hbWUgPSAnanMtcGxvdGx5LXBsb3QnO1xuXG4gICAgcHVibGljIHBsb3RseUluc3RhbmNlOiBQbG90bHkuUGxvdGx5SFRNTEVsZW1lbnQ7XG4gICAgcHVibGljIHJlc2l6ZUhhbmRsZXI/OiAoaW5zdGFuY2U6IFBsb3RseS5QbG90bHlIVE1MRWxlbWVudCkgPT4gdm9pZDtcbiAgICBwdWJsaWMgbGF5b3V0RGlmZmVyOiBLZXlWYWx1ZURpZmZlcjxzdHJpbmcsIGFueT47XG4gICAgcHVibGljIGRhdGFEaWZmZXI6IEl0ZXJhYmxlRGlmZmVyPFBsb3RseS5EYXRhPjtcblxuICAgIEBWaWV3Q2hpbGQoJ3Bsb3QnLCB7c3RhdGljOiB0cnVlfSkgcGxvdEVsOiBFbGVtZW50UmVmO1xuXG4gICAgQElucHV0KCkgZGF0YT86IFBsb3RseS5EYXRhW107XG4gICAgQElucHV0KCkgbGF5b3V0PzogUGFydGlhbDxQbG90bHkuTGF5b3V0PjtcbiAgICBASW5wdXQoKSBjb25maWc/OiBQYXJ0aWFsPFBsb3RseS5Db25maWc+O1xuICAgIEBJbnB1dCgpIGZyYW1lcz86IFBhcnRpYWw8UGxvdGx5LkNvbmZpZz5bXTtcbiAgICBASW5wdXQoKSBzdHlsZT86IHsgW2tleTogc3RyaW5nXTogc3RyaW5nIH07XG5cbiAgICBASW5wdXQoKSBkaXZJZD86IHN0cmluZztcbiAgICBASW5wdXQoKSByZXZpc2lvbjogbnVtYmVyID0gMDtcbiAgICBASW5wdXQoKSBjbGFzc05hbWU/OiBzdHJpbmcgfCBzdHJpbmdbXTtcbiAgICBASW5wdXQoKSBkZWJ1ZzogYm9vbGVhbiA9IGZhbHNlO1xuICAgIEBJbnB1dCgpIHVzZVJlc2l6ZUhhbmRsZXI6IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIEBJbnB1dCgpIHVwZGF0ZU9uTGF5b3V0Q2hhbmdlID0gdHJ1ZTtcbiAgICBASW5wdXQoKSB1cGRhdGVPbkRhdGFDaGFuZ2UgPSB0cnVlO1xuICAgIEBJbnB1dCgpIHVwZGF0ZU9ubHlXaXRoUmV2aXNpb24gPSBmYWxzZTtcblxuICAgIEBPdXRwdXQoKSBpbml0aWFsaXplZCA9IG5ldyBFdmVudEVtaXR0ZXI8UGxvdGx5LkZpZ3VyZT4oKTtcbiAgICBAT3V0cHV0KCkgdXBkYXRlID0gbmV3IEV2ZW50RW1pdHRlcjxQbG90bHkuRmlndXJlPigpO1xuICAgIEBPdXRwdXQoKSBwdXJnZSA9IG5ldyBFdmVudEVtaXR0ZXI8UGxvdGx5LkZpZ3VyZT4oKTtcbiAgICBAT3V0cHV0KCkgZXJyb3IgPSBuZXcgRXZlbnRFbWl0dGVyPEVycm9yPigpO1xuXG4gICAgQE91dHB1dCgpIGFmdGVyRXhwb3J0ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBhZnRlclBsb3QgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIGFuaW1hdGVkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBhbmltYXRpbmdGcmFtZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgYW5pbWF0aW9uSW50ZXJydXB0ZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIGF1dG9TaXplID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBiZWZvcmVFeHBvcnQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIGJ1dHRvbkNsaWNrZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIGNsaWNrID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBwbG90bHlfY2xpY2sgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIGNsaWNrQW5ub3RhdGlvbiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgZGVzZWxlY3QgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIGRvdWJsZUNsaWNrID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBmcmFtZXdvcmsgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIGhvdmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBsZWdlbmRDbGljayA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgbGVnZW5kRG91YmxlQ2xpY2sgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIHJlbGF5b3V0ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSByZXN0eWxlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSByZWRyYXcgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIHNlbGVjdGVkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBzZWxlY3RpbmcgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIHNsaWRlckNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgc2xpZGVyRW5kID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBzbGlkZXJTdGFydCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgdHJhbnNpdGlvbmluZyA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgdHJhbnNpdGlvbkludGVycnVwdGVkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSB1bmhvdmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSByZWxheW91dGluZyA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgdHJlZW1hcGNsaWNrID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBzdW5idXJzdGNsaWNrID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgcHVibGljIGV2ZW50TmFtZXMgPSBbJ2FmdGVyRXhwb3J0JywgJ2FmdGVyUGxvdCcsICdhbmltYXRlZCcsICdhbmltYXRpbmdGcmFtZScsICdhbmltYXRpb25JbnRlcnJ1cHRlZCcsICdhdXRvU2l6ZScsXG4gICAgICAgICdiZWZvcmVFeHBvcnQnLCAnYnV0dG9uQ2xpY2tlZCcsICdjbGlja0Fubm90YXRpb24nLCAnZGVzZWxlY3QnLCAnZG91YmxlQ2xpY2snLCAnZnJhbWV3b3JrJywgJ2hvdmVyJyxcbiAgICAgICAgJ2xlZ2VuZENsaWNrJywgJ2xlZ2VuZERvdWJsZUNsaWNrJywgJ3JlbGF5b3V0JywgJ3Jlc3R5bGUnLCAncmVkcmF3JywgJ3NlbGVjdGVkJywgJ3NlbGVjdGluZycsICdzbGlkZXJDaGFuZ2UnLFxuICAgICAgICAnc2xpZGVyRW5kJywgJ3NsaWRlclN0YXJ0JywgJ3RyYW5zaXRpb25pbmcnLCAndHJhbnNpdGlvbkludGVycnVwdGVkJywgJ3VuaG92ZXInLCAncmVsYXlvdXRpbmcnLCAndHJlZW1hcGNsaWNrJyxcbiAgICAgICAgJ3N1bmJ1cnN0Y2xpY2snXTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwdWJsaWMgcGxvdGx5OiBQbG90bHlTZXJ2aWNlLFxuICAgICAgICBwdWJsaWMgaXRlcmFibGVEaWZmZXJzOiBJdGVyYWJsZURpZmZlcnMsXG4gICAgICAgIHB1YmxpYyBrZXlWYWx1ZURpZmZlcnM6IEtleVZhbHVlRGlmZmVycyxcbiAgICApIHsgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMuY3JlYXRlUGxvdCgpLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgZmlndXJlID0gdGhpcy5jcmVhdGVGaWd1cmUoKTtcbiAgICAgICAgICAgIHRoaXMuaW5pdGlhbGl6ZWQuZW1pdChmaWd1cmUpO1xuICAgICAgICB9KTtcblxuXG4gICAgICAgIGlmICh0aGlzLnBsb3RseS5kZWJ1ZyAmJiB0aGlzLmNsaWNrLm9ic2VydmVycy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICBjb25zdCBtc2cgPSAnREVQUkVDQVRFRDogUmVjb25zaWRlciB1c2luZyBgKHBsb3RseV9jbGljaylgIGluc3RlYWQgb2YgYChjbGljaylgIHRvIGF2b2lkIGV2ZW50IGNvbmZsaWN0LiAnXG4gICAgICAgICAgICAgICAgKyAnUGxlYXNlIGNoZWNrIGh0dHBzOi8vZ2l0aHViLmNvbS9wbG90bHkvYW5ndWxhci1wbG90bHkuanMjRkFRJztcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IobXNnKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG5nT25EZXN0cm95KCkge1xuICAgICAgICBpZiAodHlwZW9mIHRoaXMucmVzaXplSGFuZGxlciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgdGhpcy5nZXRXaW5kb3coKS5yZW1vdmVFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aGlzLnJlc2l6ZUhhbmRsZXIgYXMgYW55KTtcbiAgICAgICAgICAgIHRoaXMucmVzaXplSGFuZGxlciA9IHVuZGVmaW5lZDtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGZpZ3VyZSA9IHRoaXMuY3JlYXRlRmlndXJlKCk7XG4gICAgICAgIHRoaXMucHVyZ2UuZW1pdChmaWd1cmUpO1xuICAgICAgICBQbG90bHlTZXJ2aWNlLnJlbW92ZSh0aGlzLnBsb3RseUluc3RhbmNlKTtcbiAgICB9XG5cbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XG4gICAgICAgIGxldCBzaG91bGRVcGRhdGUgPSBmYWxzZTtcblxuICAgICAgICBjb25zdCByZXZpc2lvbjogU2ltcGxlQ2hhbmdlID0gY2hhbmdlcy5yZXZpc2lvbjtcbiAgICAgICAgaWYgKHJldmlzaW9uICYmICFyZXZpc2lvbi5pc0ZpcnN0Q2hhbmdlKCkpIHtcbiAgICAgICAgICAgIHNob3VsZFVwZGF0ZSA9IHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBkZWJ1ZzogU2ltcGxlQ2hhbmdlID0gY2hhbmdlcy5kZWJ1ZztcbiAgICAgICAgaWYgKGRlYnVnICYmICFkZWJ1Zy5pc0ZpcnN0Q2hhbmdlKCkpIHtcbiAgICAgICAgICAgIHNob3VsZFVwZGF0ZSA9IHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoc2hvdWxkVXBkYXRlKSB7XG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVBsb3QoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMudXBkYXRlV2luZG93UmVzaXplSGFuZGxlcigpO1xuICAgIH1cblxuICAgIG5nRG9DaGVjaygpIHtcbiAgICAgICAgaWYgKHRoaXMudXBkYXRlT25seVdpdGhSZXZpc2lvbikge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgbGV0IHNob3VsZFVwZGF0ZSA9IGZhbHNlO1xuXG4gICAgICAgIGlmICh0aGlzLnVwZGF0ZU9uTGF5b3V0Q2hhbmdlKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5sYXlvdXREaWZmZXIpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBsYXlvdXRIYXNEaWZmID0gdGhpcy5sYXlvdXREaWZmZXIuZGlmZih0aGlzLmxheW91dCk7XG4gICAgICAgICAgICAgICAgaWYgKGxheW91dEhhc0RpZmYpIHtcbiAgICAgICAgICAgICAgICAgICAgc2hvdWxkVXBkYXRlID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMubGF5b3V0KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5sYXlvdXREaWZmZXIgPSB0aGlzLmtleVZhbHVlRGlmZmVycy5maW5kKHRoaXMubGF5b3V0KS5jcmVhdGUoKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5sYXlvdXREaWZmZXIgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy51cGRhdGVPbkRhdGFDaGFuZ2UpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmRhdGFEaWZmZXIpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBkYXRhSGFzRGlmZiA9IHRoaXMuZGF0YURpZmZlci5kaWZmKHRoaXMuZGF0YSk7XG4gICAgICAgICAgICAgICAgaWYgKGRhdGFIYXNEaWZmKSB7XG4gICAgICAgICAgICAgICAgICAgIHNob3VsZFVwZGF0ZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIGlmIChBcnJheS5pc0FycmF5KHRoaXMuZGF0YSkpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmRhdGFEaWZmZXIgPSB0aGlzLml0ZXJhYmxlRGlmZmVycy5maW5kKHRoaXMuZGF0YSkuY3JlYXRlKHRoaXMuZGF0YURpZmZlclRyYWNrQnkpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLmRhdGFEaWZmZXIgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoc2hvdWxkVXBkYXRlICYmIHRoaXMucGxvdGx5SW5zdGFuY2UpIHtcbiAgICAgICAgICAgIHRoaXMudXBkYXRlUGxvdCgpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgZ2V0V2luZG93KCk6IGFueSB7XG4gICAgICAgIHJldHVybiB3aW5kb3c7XG4gICAgfVxuXG4gICAgZ2V0Q2xhc3NOYW1lKCk6IHN0cmluZyB7XG4gICAgICAgIGxldCBjbGFzc2VzID0gW3RoaXMuZGVmYXVsdENsYXNzTmFtZV07XG5cbiAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkodGhpcy5jbGFzc05hbWUpKSB7XG4gICAgICAgICAgICBjbGFzc2VzID0gY2xhc3Nlcy5jb25jYXQodGhpcy5jbGFzc05hbWUpO1xuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuY2xhc3NOYW1lKSB7XG4gICAgICAgICAgICBjbGFzc2VzLnB1c2godGhpcy5jbGFzc05hbWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGNsYXNzZXMuam9pbignICcpO1xuICAgIH1cblxuICAgIGNyZWF0ZVBsb3QoKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIHJldHVybiB0aGlzLnBsb3RseS5uZXdQbG90KHRoaXMucGxvdEVsLm5hdGl2ZUVsZW1lbnQsIHRoaXMuZGF0YSwgdGhpcy5sYXlvdXQsIHRoaXMuY29uZmlnLCB0aGlzLmZyYW1lcykudGhlbihwbG90bHlJbnN0YW5jZSA9PiB7XG4gICAgICAgICAgICB0aGlzLnBsb3RseUluc3RhbmNlID0gcGxvdGx5SW5zdGFuY2U7XG4gICAgICAgICAgICB0aGlzLmdldFdpbmRvdygpLmdkID0gdGhpcy5kZWJ1ZyA/IHBsb3RseUluc3RhbmNlIDogdW5kZWZpbmVkO1xuXG4gICAgICAgICAgICB0aGlzLmV2ZW50TmFtZXMuZm9yRWFjaChuYW1lID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBldmVudE5hbWUgPSBgcGxvdGx5XyR7bmFtZS50b0xvd2VyQ2FzZSgpfWA7XG4gICAgICAgICAgICAgICAgcGxvdGx5SW5zdGFuY2Uub24oZXZlbnROYW1lLCAoZGF0YTogYW55KSA9PiAodGhpc1tuYW1lXSBhcyBFdmVudEVtaXR0ZXI8dm9pZD4pLmVtaXQoZGF0YSkpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIHBsb3RseUluc3RhbmNlLm9uKCdwbG90bHlfY2xpY2snLCAoZGF0YTogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5jbGljay5lbWl0KGRhdGEpO1xuICAgICAgICAgICAgICAgIHRoaXMucGxvdGx5X2NsaWNrLmVtaXQoZGF0YSk7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgdGhpcy51cGRhdGVXaW5kb3dSZXNpemVIYW5kbGVyKCk7XG4gICAgICAgIH0sIGVyciA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciB3aGlsZSBwbG90dGluZzonLCBlcnIpO1xuICAgICAgICAgICAgdGhpcy5lcnJvci5lbWl0KGVycik7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGNyZWF0ZUZpZ3VyZSgpOiBQbG90bHkuRmlndXJlIHtcbiAgICAgICAgY29uc3QgcDogYW55ID0gdGhpcy5wbG90bHlJbnN0YW5jZTtcbiAgICAgICAgY29uc3QgZmlndXJlOiBQbG90bHkuRmlndXJlID0ge1xuICAgICAgICAgICAgZGF0YTogcC5kYXRhLFxuICAgICAgICAgICAgbGF5b3V0OiBwLmxheW91dCxcbiAgICAgICAgICAgIGZyYW1lczogcC5fdHJhbnNpdGlvbkRhdGEgPyBwLl90cmFuc2l0aW9uRGF0YS5fZnJhbWVzIDogbnVsbFxuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBmaWd1cmU7XG4gICAgfVxuXG4gICAgdXBkYXRlUGxvdCgpIHtcbiAgICAgICAgaWYgKCF0aGlzLnBsb3RseUluc3RhbmNlKSB7XG4gICAgICAgICAgICBjb25zdCBlcnJvciA9IG5ldyBFcnJvcihgUGxvdGx5IGNvbXBvbmVudCB3YXNuJ3QgaW5pdGlhbGl6ZWRgKTtcbiAgICAgICAgICAgIHRoaXMuZXJyb3IuZW1pdChlcnJvcik7XG4gICAgICAgICAgICB0aHJvdyBlcnJvcjtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGxheW91dCA9IHsuLi50aGlzLmxheW91dH07XG5cbiAgICAgICAgcmV0dXJuIHRoaXMucGxvdGx5LnVwZGF0ZSh0aGlzLnBsb3RseUluc3RhbmNlLCB0aGlzLmRhdGEsIGxheW91dCwgdGhpcy5jb25maWcsIHRoaXMuZnJhbWVzKS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGZpZ3VyZSA9IHRoaXMuY3JlYXRlRmlndXJlKCk7XG4gICAgICAgICAgICB0aGlzLnVwZGF0ZS5lbWl0KGZpZ3VyZSk7XG4gICAgICAgIH0sIGVyciA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciB3aGlsZSB1cGRhdGluZyBwbG90OicsIGVycik7XG4gICAgICAgICAgICB0aGlzLmVycm9yLmVtaXQoZXJyKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgdXBkYXRlV2luZG93UmVzaXplSGFuZGxlcigpIHtcbiAgICAgICAgaWYgKHRoaXMudXNlUmVzaXplSGFuZGxlcikge1xuICAgICAgICAgICAgaWYgKHRoaXMucmVzaXplSGFuZGxlciA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5yZXNpemVIYW5kbGVyID0gKCkgPT4gdGhpcy5wbG90bHkucmVzaXplKHRoaXMucGxvdGx5SW5zdGFuY2UpO1xuICAgICAgICAgICAgICAgIHRoaXMuZ2V0V2luZG93KCkuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgdGhpcy5yZXNpemVIYW5kbGVyIGFzIGFueSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAodHlwZW9mIHRoaXMucmVzaXplSGFuZGxlciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICAgIHRoaXMuZ2V0V2luZG93KCkucmVtb3ZlRXZlbnRMaXN0ZW5lcigncmVzaXplJywgdGhpcy5yZXNpemVIYW5kbGVyIGFzIGFueSk7XG4gICAgICAgICAgICAgICAgdGhpcy5yZXNpemVIYW5kbGVyID0gdW5kZWZpbmVkO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgZGF0YURpZmZlclRyYWNrQnkoXzogbnVtYmVyLCBpdGVtOiBhbnkpOiBhbnkge1xuICAgICAgICBjb25zdCBvYmogPSBPYmplY3QuYXNzaWduKHt9LCBpdGVtLCB7IHVpZDogJycgfSk7XG4gICAgICAgIHJldHVybiBKU09OLnN0cmluZ2lmeShvYmopO1xuICAgIH1cblxufVxuIl19