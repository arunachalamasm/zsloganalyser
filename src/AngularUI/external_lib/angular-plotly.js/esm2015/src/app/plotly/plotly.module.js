var PlotlyModule_1;
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlotComponent } from '../shared/plot/plot.component';
import { PlotlyService } from '../shared/plotly.service';
import { SharedModule } from '../shared/shared.module';
let PlotlyModule = PlotlyModule_1 = class PlotlyModule {
    constructor() {
        if (!this.isValid()) {
            const msg = "Invalid PlotlyJS object. Please check https://github.com/plotly/angular-plotly.js#quick-start"
                + " to see how to add PlotlyJS to your project.";
            throw new Error(msg);
        }
        PlotlyService.setPlotly(PlotlyModule_1.plotlyjs);
    }
    isValid() {
        return PlotlyModule_1.plotlyjs !== undefined
            && typeof PlotlyModule_1.plotlyjs.plot === 'function';
    }
};
PlotlyModule.plotlyjs = {};
PlotlyModule = PlotlyModule_1 = __decorate([
    NgModule({
        imports: [CommonModule, SharedModule],
        declarations: [],
        exports: [PlotComponent]
    })
], PlotlyModule);
export { PlotlyModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGxvdGx5Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItcGxvdGx5LmpzLyIsInNvdXJjZXMiOlsic3JjL2FwcC9wbG90bHkvcGxvdGx5Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUM5RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDekQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBUXZELElBQWEsWUFBWSxvQkFBekIsTUFBYSxZQUFZO0lBR3JCO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUNqQixNQUFNLEdBQUcsR0FBRywrRkFBK0Y7a0JBQy9GLDhDQUE4QyxDQUFDO1lBQzNELE1BQU0sSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDeEI7UUFFRCxhQUFhLENBQUMsU0FBUyxDQUFDLGNBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRU8sT0FBTztRQUNYLE9BQU8sY0FBWSxDQUFDLFFBQVEsS0FBSyxTQUFTO2VBQ25DLE9BQU8sY0FBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEtBQUssVUFBVSxDQUFDO0lBQzVELENBQUM7Q0FDSixDQUFBO0FBaEJpQixxQkFBUSxHQUFRLEVBQUUsQ0FBQztBQUR4QixZQUFZO0lBTHhCLFFBQVEsQ0FBQztRQUNOLE9BQU8sRUFBRSxDQUFDLFlBQVksRUFBRSxZQUFZLENBQUM7UUFDckMsWUFBWSxFQUFFLEVBQUU7UUFDaEIsT0FBTyxFQUFFLENBQUMsYUFBYSxDQUFDO0tBQzNCLENBQUM7R0FDVyxZQUFZLENBaUJ4QjtTQWpCWSxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbmltcG9ydCB7IFBsb3RDb21wb25lbnQgfSBmcm9tICcuLi9zaGFyZWQvcGxvdC9wbG90LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBQbG90bHlTZXJ2aWNlIH0gZnJvbSAnLi4vc2hhcmVkL3Bsb3RseS5zZXJ2aWNlJztcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uL3NoYXJlZC9zaGFyZWQubW9kdWxlJztcblxuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGUsIFNoYXJlZE1vZHVsZV0sXG4gICAgZGVjbGFyYXRpb25zOiBbXSxcbiAgICBleHBvcnRzOiBbUGxvdENvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgUGxvdGx5TW9kdWxlIHtcbiAgICBwdWJsaWMgc3RhdGljIHBsb3RseWpzOiBhbnkgPSB7fTtcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBpZiAoIXRoaXMuaXNWYWxpZCgpKSB7XG4gICAgICAgICAgICBjb25zdCBtc2cgPSBcIkludmFsaWQgUGxvdGx5SlMgb2JqZWN0LiBQbGVhc2UgY2hlY2sgaHR0cHM6Ly9naXRodWIuY29tL3Bsb3RseS9hbmd1bGFyLXBsb3RseS5qcyNxdWljay1zdGFydFwiXG4gICAgICAgICAgICAgICAgICAgICAgKyBcIiB0byBzZWUgaG93IHRvIGFkZCBQbG90bHlKUyB0byB5b3VyIHByb2plY3QuXCI7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IobXNnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIFBsb3RseVNlcnZpY2Uuc2V0UGxvdGx5KFBsb3RseU1vZHVsZS5wbG90bHlqcyk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBpc1ZhbGlkKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gUGxvdGx5TW9kdWxlLnBsb3RseWpzICE9PSB1bmRlZmluZWRcbiAgICAgICAgICAgICYmIHR5cGVvZiBQbG90bHlNb2R1bGUucGxvdGx5anMucGxvdCA9PT0gJ2Z1bmN0aW9uJztcbiAgICB9XG59XG4iXX0=