export { PlotlyModule } from './src/app/plotly/plotly.module';
export { PlotlyViaWindowModule } from './src/app/plotly-via-window/plotly-via-window.module';
export { PlotComponent } from './src/app/shared/plot/plot.component';
export { PlotlyService } from './src/app/shared/plotly.service';
