import { __assign, __decorate } from "tslib";
import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnChanges, OnInit, Output, SimpleChange, SimpleChanges, ViewChild, DoCheck, IterableDiffer, IterableDiffers, KeyValueDiffer, KeyValueDiffers, } from '@angular/core';
import { PlotlyService } from '../plotly.service';
// @dynamic
var PlotComponent = /** @class */ (function () {
    function PlotComponent(plotly, iterableDiffers, keyValueDiffers) {
        this.plotly = plotly;
        this.iterableDiffers = iterableDiffers;
        this.keyValueDiffers = keyValueDiffers;
        this.defaultClassName = 'js-plotly-plot';
        this.revision = 0;
        this.debug = false;
        this.useResizeHandler = false;
        this.updateOnLayoutChange = true;
        this.updateOnDataChange = true;
        this.updateOnlyWithRevision = false;
        this.initialized = new EventEmitter();
        this.update = new EventEmitter();
        this.purge = new EventEmitter();
        this.error = new EventEmitter();
        this.afterExport = new EventEmitter();
        this.afterPlot = new EventEmitter();
        this.animated = new EventEmitter();
        this.animatingFrame = new EventEmitter();
        this.animationInterrupted = new EventEmitter();
        this.autoSize = new EventEmitter();
        this.beforeExport = new EventEmitter();
        this.buttonClicked = new EventEmitter();
        this.click = new EventEmitter();
        this.plotly_click = new EventEmitter();
        this.clickAnnotation = new EventEmitter();
        this.deselect = new EventEmitter();
        this.doubleClick = new EventEmitter();
        this.framework = new EventEmitter();
        this.hover = new EventEmitter();
        this.legendClick = new EventEmitter();
        this.legendDoubleClick = new EventEmitter();
        this.relayout = new EventEmitter();
        this.restyle = new EventEmitter();
        this.redraw = new EventEmitter();
        this.selected = new EventEmitter();
        this.selecting = new EventEmitter();
        this.sliderChange = new EventEmitter();
        this.sliderEnd = new EventEmitter();
        this.sliderStart = new EventEmitter();
        this.transitioning = new EventEmitter();
        this.transitionInterrupted = new EventEmitter();
        this.unhover = new EventEmitter();
        this.relayouting = new EventEmitter();
        this.treemapclick = new EventEmitter();
        this.sunburstclick = new EventEmitter();
        this.eventNames = ['afterExport', 'afterPlot', 'animated', 'animatingFrame', 'animationInterrupted', 'autoSize',
            'beforeExport', 'buttonClicked', 'clickAnnotation', 'deselect', 'doubleClick', 'framework', 'hover',
            'legendClick', 'legendDoubleClick', 'relayout', 'restyle', 'redraw', 'selected', 'selecting', 'sliderChange',
            'sliderEnd', 'sliderStart', 'transitioning', 'transitionInterrupted', 'unhover', 'relayouting', 'treemapclick',
            'sunburstclick'];
    }
    PlotComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createPlot().then(function () {
            var figure = _this.createFigure();
            _this.initialized.emit(figure);
        });
        if (this.plotly.debug && this.click.observers.length > 0) {
            var msg = 'DEPRECATED: Reconsider using `(plotly_click)` instead of `(click)` to avoid event conflict. '
                + 'Please check https://github.com/plotly/angular-plotly.js#FAQ';
            console.error(msg);
        }
    };
    PlotComponent.prototype.ngOnDestroy = function () {
        if (typeof this.resizeHandler === 'function') {
            this.getWindow().removeEventListener('resize', this.resizeHandler);
            this.resizeHandler = undefined;
        }
        var figure = this.createFigure();
        this.purge.emit(figure);
        PlotlyService.remove(this.plotlyInstance);
    };
    PlotComponent.prototype.ngOnChanges = function (changes) {
        var shouldUpdate = false;
        var revision = changes.revision;
        if (revision && !revision.isFirstChange()) {
            shouldUpdate = true;
        }
        var debug = changes.debug;
        if (debug && !debug.isFirstChange()) {
            shouldUpdate = true;
        }
        if (shouldUpdate) {
            this.updatePlot();
        }
        this.updateWindowResizeHandler();
    };
    PlotComponent.prototype.ngDoCheck = function () {
        if (this.updateOnlyWithRevision) {
            return false;
        }
        var shouldUpdate = false;
        if (this.updateOnLayoutChange) {
            if (this.layoutDiffer) {
                var layoutHasDiff = this.layoutDiffer.diff(this.layout);
                if (layoutHasDiff) {
                    shouldUpdate = true;
                }
            }
            else if (this.layout) {
                this.layoutDiffer = this.keyValueDiffers.find(this.layout).create();
            }
            else {
                this.layoutDiffer = undefined;
            }
        }
        if (this.updateOnDataChange) {
            if (this.dataDiffer) {
                var dataHasDiff = this.dataDiffer.diff(this.data);
                if (dataHasDiff) {
                    shouldUpdate = true;
                }
            }
            else if (Array.isArray(this.data)) {
                this.dataDiffer = this.iterableDiffers.find(this.data).create(this.dataDifferTrackBy);
            }
            else {
                this.dataDiffer = undefined;
            }
        }
        if (shouldUpdate && this.plotlyInstance) {
            this.updatePlot();
        }
    };
    PlotComponent.prototype.getWindow = function () {
        return window;
    };
    PlotComponent.prototype.getClassName = function () {
        var classes = [this.defaultClassName];
        if (Array.isArray(this.className)) {
            classes = classes.concat(this.className);
        }
        else if (this.className) {
            classes.push(this.className);
        }
        return classes.join(' ');
    };
    PlotComponent.prototype.createPlot = function () {
        var _this = this;
        return this.plotly.newPlot(this.plotEl.nativeElement, this.data, this.layout, this.config, this.frames).then(function (plotlyInstance) {
            _this.plotlyInstance = plotlyInstance;
            _this.getWindow().gd = _this.debug ? plotlyInstance : undefined;
            _this.eventNames.forEach(function (name) {
                var eventName = "plotly_" + name.toLowerCase();
                plotlyInstance.on(eventName, function (data) { return _this[name].emit(data); });
            });
            plotlyInstance.on('plotly_click', function (data) {
                _this.click.emit(data);
                _this.plotly_click.emit(data);
            });
            _this.updateWindowResizeHandler();
        }, function (err) {
            console.error('Error while plotting:', err);
            _this.error.emit(err);
        });
    };
    PlotComponent.prototype.createFigure = function () {
        var p = this.plotlyInstance;
        var figure = {
            data: p.data,
            layout: p.layout,
            frames: p._transitionData ? p._transitionData._frames : null
        };
        return figure;
    };
    PlotComponent.prototype.updatePlot = function () {
        var _this = this;
        if (!this.plotlyInstance) {
            var error = new Error("Plotly component wasn't initialized");
            this.error.emit(error);
            throw error;
        }
        var layout = __assign({}, this.layout);
        return this.plotly.update(this.plotlyInstance, this.data, layout, this.config, this.frames).then(function () {
            var figure = _this.createFigure();
            _this.update.emit(figure);
        }, function (err) {
            console.error('Error while updating plot:', err);
            _this.error.emit(err);
        });
    };
    PlotComponent.prototype.updateWindowResizeHandler = function () {
        var _this = this;
        if (this.useResizeHandler) {
            if (this.resizeHandler === undefined) {
                this.resizeHandler = function () { return _this.plotly.resize(_this.plotlyInstance); };
                this.getWindow().addEventListener('resize', this.resizeHandler);
            }
        }
        else {
            if (typeof this.resizeHandler === 'function') {
                this.getWindow().removeEventListener('resize', this.resizeHandler);
                this.resizeHandler = undefined;
            }
        }
    };
    PlotComponent.prototype.dataDifferTrackBy = function (_, item) {
        var obj = Object.assign({}, item, { uid: '' });
        return JSON.stringify(obj);
    };
    PlotComponent.ctorParameters = function () { return [
        { type: PlotlyService },
        { type: IterableDiffers },
        { type: KeyValueDiffers }
    ]; };
    __decorate([
        ViewChild('plot', { static: true })
    ], PlotComponent.prototype, "plotEl", void 0);
    __decorate([
        Input()
    ], PlotComponent.prototype, "data", void 0);
    __decorate([
        Input()
    ], PlotComponent.prototype, "layout", void 0);
    __decorate([
        Input()
    ], PlotComponent.prototype, "config", void 0);
    __decorate([
        Input()
    ], PlotComponent.prototype, "frames", void 0);
    __decorate([
        Input()
    ], PlotComponent.prototype, "style", void 0);
    __decorate([
        Input()
    ], PlotComponent.prototype, "divId", void 0);
    __decorate([
        Input()
    ], PlotComponent.prototype, "revision", void 0);
    __decorate([
        Input()
    ], PlotComponent.prototype, "className", void 0);
    __decorate([
        Input()
    ], PlotComponent.prototype, "debug", void 0);
    __decorate([
        Input()
    ], PlotComponent.prototype, "useResizeHandler", void 0);
    __decorate([
        Input()
    ], PlotComponent.prototype, "updateOnLayoutChange", void 0);
    __decorate([
        Input()
    ], PlotComponent.prototype, "updateOnDataChange", void 0);
    __decorate([
        Input()
    ], PlotComponent.prototype, "updateOnlyWithRevision", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "initialized", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "update", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "purge", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "error", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "afterExport", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "afterPlot", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "animated", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "animatingFrame", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "animationInterrupted", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "autoSize", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "beforeExport", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "buttonClicked", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "click", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "plotly_click", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "clickAnnotation", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "deselect", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "doubleClick", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "framework", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "hover", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "legendClick", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "legendDoubleClick", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "relayout", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "restyle", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "redraw", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "selected", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "selecting", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "sliderChange", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "sliderEnd", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "sliderStart", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "transitioning", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "transitionInterrupted", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "unhover", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "relayouting", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "treemapclick", void 0);
    __decorate([
        Output()
    ], PlotComponent.prototype, "sunburstclick", void 0);
    PlotComponent = __decorate([
        Component({
            selector: 'plotly-plot',
            template: "<div #plot [attr.id]=\"divId\" [className]=\"getClassName()\" [ngStyle]=\"style\"></div>",
            providers: [PlotlyService]
        })
    ], PlotComponent);
    return PlotComponent;
}());
export { PlotComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGxvdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hbmd1bGFyLXBsb3RseS5qcy8iLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3Bsb3QvcGxvdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFDSCxTQUFTLEVBQ1QsVUFBVSxFQUNWLFlBQVksRUFDWixLQUFLLEVBQ0wsU0FBUyxFQUNULFNBQVMsRUFDVCxNQUFNLEVBQ04sTUFBTSxFQUNOLFlBQVksRUFDWixhQUFhLEVBQ2IsU0FBUyxFQUNULE9BQU8sRUFDUCxjQUFjLEVBQ2QsZUFBZSxFQUNmLGNBQWMsRUFDZCxlQUFlLEdBQ2xCLE1BQU0sZUFBZSxDQUFDO0FBRXZCLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUdsRCxXQUFXO0FBTVg7SUFxRUksdUJBQ1csTUFBcUIsRUFDckIsZUFBZ0MsRUFDaEMsZUFBZ0M7UUFGaEMsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUNyQixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBdkVqQyxxQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQWdCckMsYUFBUSxHQUFXLENBQUMsQ0FBQztRQUVyQixVQUFLLEdBQVksS0FBSyxDQUFDO1FBQ3ZCLHFCQUFnQixHQUFZLEtBQUssQ0FBQztRQUVsQyx5QkFBb0IsR0FBRyxJQUFJLENBQUM7UUFDNUIsdUJBQWtCLEdBQUcsSUFBSSxDQUFDO1FBQzFCLDJCQUFzQixHQUFHLEtBQUssQ0FBQztRQUU5QixnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFpQixDQUFDO1FBQ2hELFdBQU0sR0FBRyxJQUFJLFlBQVksRUFBaUIsQ0FBQztRQUMzQyxVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQWlCLENBQUM7UUFDMUMsVUFBSyxHQUFHLElBQUksWUFBWSxFQUFTLENBQUM7UUFFbEMsZ0JBQVcsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2pDLGNBQVMsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQy9CLGFBQVEsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzlCLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNwQyx5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzFDLGFBQVEsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzlCLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNsQyxrQkFBYSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDbkMsVUFBSyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDM0IsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2xDLG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNyQyxhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM5QixnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDakMsY0FBUyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDL0IsVUFBSyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDM0IsZ0JBQVcsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2pDLHNCQUFpQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDdkMsYUFBUSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDOUIsWUFBTyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDN0IsV0FBTSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDNUIsYUFBUSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDOUIsY0FBUyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDL0IsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2xDLGNBQVMsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQy9CLGdCQUFXLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNqQyxrQkFBYSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDbkMsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzQyxZQUFPLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM3QixnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDakMsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2xDLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUV0QyxlQUFVLEdBQUcsQ0FBQyxhQUFhLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxnQkFBZ0IsRUFBRSxzQkFBc0IsRUFBRSxVQUFVO1lBQzdHLGNBQWMsRUFBRSxlQUFlLEVBQUUsaUJBQWlCLEVBQUUsVUFBVSxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQUUsT0FBTztZQUNuRyxhQUFhLEVBQUUsbUJBQW1CLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxjQUFjO1lBQzVHLFdBQVcsRUFBRSxhQUFhLEVBQUUsZUFBZSxFQUFFLHVCQUF1QixFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsY0FBYztZQUM5RyxlQUFlLENBQUMsQ0FBQztJQU1qQixDQUFDO0lBRUwsZ0NBQVEsR0FBUjtRQUFBLGlCQVlDO1FBWEcsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLElBQUksQ0FBQztZQUNuQixJQUFNLE1BQU0sR0FBRyxLQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDbkMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbEMsQ0FBQyxDQUFDLENBQUM7UUFHSCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDdEQsSUFBTSxHQUFHLEdBQUcsOEZBQThGO2tCQUNwRyw4REFBOEQsQ0FBQztZQUNyRSxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3RCO0lBQ0wsQ0FBQztJQUVELG1DQUFXLEdBQVg7UUFDSSxJQUFJLE9BQU8sSUFBSSxDQUFDLGFBQWEsS0FBSyxVQUFVLEVBQUU7WUFDMUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsYUFBb0IsQ0FBQyxDQUFDO1lBQzFFLElBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDO1NBQ2xDO1FBRUQsSUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3hCLGFBQWEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCxtQ0FBVyxHQUFYLFVBQVksT0FBc0I7UUFDOUIsSUFBSSxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBRXpCLElBQU0sUUFBUSxHQUFpQixPQUFPLENBQUMsUUFBUSxDQUFDO1FBQ2hELElBQUksUUFBUSxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRSxFQUFFO1lBQ3ZDLFlBQVksR0FBRyxJQUFJLENBQUM7U0FDdkI7UUFFRCxJQUFNLEtBQUssR0FBaUIsT0FBTyxDQUFDLEtBQUssQ0FBQztRQUMxQyxJQUFJLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsRUFBRTtZQUNqQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1NBQ3ZCO1FBRUQsSUFBSSxZQUFZLEVBQUU7WUFDZCxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7U0FDckI7UUFFRCxJQUFJLENBQUMseUJBQXlCLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRUQsaUNBQVMsR0FBVDtRQUNJLElBQUksSUFBSSxDQUFDLHNCQUFzQixFQUFFO1lBQzdCLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBRUQsSUFBSSxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBRXpCLElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFO1lBQzNCLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDbkIsSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMxRCxJQUFJLGFBQWEsRUFBRTtvQkFDZixZQUFZLEdBQUcsSUFBSSxDQUFDO2lCQUN2QjthQUNKO2lCQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDcEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDdkU7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUM7YUFDakM7U0FDSjtRQUVELElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQ3pCLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDakIsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwRCxJQUFJLFdBQVcsRUFBRTtvQkFDYixZQUFZLEdBQUcsSUFBSSxDQUFDO2lCQUN2QjthQUNKO2lCQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ2pDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQzthQUN6RjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQzthQUMvQjtTQUNKO1FBRUQsSUFBSSxZQUFZLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNyQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7U0FDckI7SUFDTCxDQUFDO0lBRUQsaUNBQVMsR0FBVDtRQUNJLE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUFFRCxvQ0FBWSxHQUFaO1FBQ0ksSUFBSSxPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUV0QyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQy9CLE9BQU8sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUM1QzthQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUN2QixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUNoQztRQUVELE9BQU8sT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBRUQsa0NBQVUsR0FBVjtRQUFBLGlCQW9CQztRQW5CRyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxjQUFjO1lBQ3ZILEtBQUksQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDO1lBQ3JDLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7WUFFOUQsS0FBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO2dCQUN4QixJQUFNLFNBQVMsR0FBRyxZQUFVLElBQUksQ0FBQyxXQUFXLEVBQUksQ0FBQztnQkFDakQsY0FBYyxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsVUFBQyxJQUFTLElBQUssT0FBQyxLQUFJLENBQUMsSUFBSSxDQUF3QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBN0MsQ0FBNkMsQ0FBQyxDQUFDO1lBQy9GLENBQUMsQ0FBQyxDQUFDO1lBRUgsY0FBYyxDQUFDLEVBQUUsQ0FBQyxjQUFjLEVBQUUsVUFBQyxJQUFTO2dCQUN4QyxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDakMsQ0FBQyxDQUFDLENBQUM7WUFFSCxLQUFJLENBQUMseUJBQXlCLEVBQUUsQ0FBQztRQUNyQyxDQUFDLEVBQUUsVUFBQSxHQUFHO1lBQ0YsT0FBTyxDQUFDLEtBQUssQ0FBQyx1QkFBdUIsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUM1QyxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN6QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxvQ0FBWSxHQUFaO1FBQ0ksSUFBTSxDQUFDLEdBQVEsSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUNuQyxJQUFNLE1BQU0sR0FBa0I7WUFDMUIsSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJO1lBQ1osTUFBTSxFQUFFLENBQUMsQ0FBQyxNQUFNO1lBQ2hCLE1BQU0sRUFBRSxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSTtTQUMvRCxDQUFDO1FBRUYsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUVELGtDQUFVLEdBQVY7UUFBQSxpQkFnQkM7UUFmRyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN0QixJQUFNLEtBQUssR0FBRyxJQUFJLEtBQUssQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDO1lBQy9ELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3ZCLE1BQU0sS0FBSyxDQUFDO1NBQ2Y7UUFFRCxJQUFNLE1BQU0sZ0JBQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRWhDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDN0YsSUFBTSxNQUFNLEdBQUcsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQ25DLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzdCLENBQUMsRUFBRSxVQUFBLEdBQUc7WUFDRixPQUFPLENBQUMsS0FBSyxDQUFDLDRCQUE0QixFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ2pELEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGlEQUF5QixHQUF6QjtRQUFBLGlCQVlDO1FBWEcsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDdkIsSUFBSSxJQUFJLENBQUMsYUFBYSxLQUFLLFNBQVMsRUFBRTtnQkFDbEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxjQUFNLE9BQUEsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxFQUF2QyxDQUF1QyxDQUFDO2dCQUNuRSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxhQUFvQixDQUFDLENBQUM7YUFDMUU7U0FDSjthQUFNO1lBQ0gsSUFBSSxPQUFPLElBQUksQ0FBQyxhQUFhLEtBQUssVUFBVSxFQUFFO2dCQUMxQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxhQUFvQixDQUFDLENBQUM7Z0JBQzFFLElBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDO2FBQ2xDO1NBQ0o7SUFDTCxDQUFDO0lBRUQseUNBQWlCLEdBQWpCLFVBQWtCLENBQVMsRUFBRSxJQUFTO1FBQ2xDLElBQU0sR0FBRyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ2pELE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUMvQixDQUFDOztnQkE1S2tCLGFBQWE7Z0JBQ0osZUFBZTtnQkFDZixlQUFlOztJQWhFUjtRQUFsQyxTQUFTLENBQUMsTUFBTSxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDO2lEQUFvQjtJQUU3QztRQUFSLEtBQUssRUFBRTsrQ0FBc0I7SUFDckI7UUFBUixLQUFLLEVBQUU7aURBQWlDO0lBQ2hDO1FBQVIsS0FBSyxFQUFFO2lEQUFpQztJQUNoQztRQUFSLEtBQUssRUFBRTtpREFBbUM7SUFDbEM7UUFBUixLQUFLLEVBQUU7Z0RBQW1DO0lBRWxDO1FBQVIsS0FBSyxFQUFFO2dEQUFnQjtJQUNmO1FBQVIsS0FBSyxFQUFFO21EQUFzQjtJQUNyQjtRQUFSLEtBQUssRUFBRTtvREFBK0I7SUFDOUI7UUFBUixLQUFLLEVBQUU7Z0RBQXdCO0lBQ3ZCO1FBQVIsS0FBSyxFQUFFOzJEQUFtQztJQUVsQztRQUFSLEtBQUssRUFBRTsrREFBNkI7SUFDNUI7UUFBUixLQUFLLEVBQUU7NkRBQTJCO0lBQzFCO1FBQVIsS0FBSyxFQUFFO2lFQUFnQztJQUU5QjtRQUFULE1BQU0sRUFBRTtzREFBaUQ7SUFDaEQ7UUFBVCxNQUFNLEVBQUU7aURBQTRDO0lBQzNDO1FBQVQsTUFBTSxFQUFFO2dEQUEyQztJQUMxQztRQUFULE1BQU0sRUFBRTtnREFBbUM7SUFFbEM7UUFBVCxNQUFNLEVBQUU7c0RBQWtDO0lBQ2pDO1FBQVQsTUFBTSxFQUFFO29EQUFnQztJQUMvQjtRQUFULE1BQU0sRUFBRTttREFBK0I7SUFDOUI7UUFBVCxNQUFNLEVBQUU7eURBQXFDO0lBQ3BDO1FBQVQsTUFBTSxFQUFFOytEQUEyQztJQUMxQztRQUFULE1BQU0sRUFBRTttREFBK0I7SUFDOUI7UUFBVCxNQUFNLEVBQUU7dURBQW1DO0lBQ2xDO1FBQVQsTUFBTSxFQUFFO3dEQUFvQztJQUNuQztRQUFULE1BQU0sRUFBRTtnREFBNEI7SUFDM0I7UUFBVCxNQUFNLEVBQUU7dURBQW1DO0lBQ2xDO1FBQVQsTUFBTSxFQUFFOzBEQUFzQztJQUNyQztRQUFULE1BQU0sRUFBRTttREFBK0I7SUFDOUI7UUFBVCxNQUFNLEVBQUU7c0RBQWtDO0lBQ2pDO1FBQVQsTUFBTSxFQUFFO29EQUFnQztJQUMvQjtRQUFULE1BQU0sRUFBRTtnREFBNEI7SUFDM0I7UUFBVCxNQUFNLEVBQUU7c0RBQWtDO0lBQ2pDO1FBQVQsTUFBTSxFQUFFOzREQUF3QztJQUN2QztRQUFULE1BQU0sRUFBRTttREFBK0I7SUFDOUI7UUFBVCxNQUFNLEVBQUU7a0RBQThCO0lBQzdCO1FBQVQsTUFBTSxFQUFFO2lEQUE2QjtJQUM1QjtRQUFULE1BQU0sRUFBRTttREFBK0I7SUFDOUI7UUFBVCxNQUFNLEVBQUU7b0RBQWdDO0lBQy9CO1FBQVQsTUFBTSxFQUFFO3VEQUFtQztJQUNsQztRQUFULE1BQU0sRUFBRTtvREFBZ0M7SUFDL0I7UUFBVCxNQUFNLEVBQUU7c0RBQWtDO0lBQ2pDO1FBQVQsTUFBTSxFQUFFO3dEQUFvQztJQUNuQztRQUFULE1BQU0sRUFBRTtnRUFBNEM7SUFDM0M7UUFBVCxNQUFNLEVBQUU7a0RBQThCO0lBQzdCO1FBQVQsTUFBTSxFQUFFO3NEQUFrQztJQUNqQztRQUFULE1BQU0sRUFBRTt1REFBbUM7SUFDbEM7UUFBVCxNQUFNLEVBQUU7d0RBQW9DO0lBN0RwQyxhQUFhO1FBTHpCLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxhQUFhO1lBQ3ZCLFFBQVEsRUFBRSwwRkFBb0Y7WUFDOUYsU0FBUyxFQUFFLENBQUMsYUFBYSxDQUFDO1NBQzdCLENBQUM7T0FDVyxhQUFhLENBb1B6QjtJQUFELG9CQUFDO0NBQUEsQUFwUEQsSUFvUEM7U0FwUFksYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gICAgQ29tcG9uZW50LFxuICAgIEVsZW1lbnRSZWYsXG4gICAgRXZlbnRFbWl0dGVyLFxuICAgIElucHV0LFxuICAgIE9uRGVzdHJveSxcbiAgICBPbkNoYW5nZXMsXG4gICAgT25Jbml0LFxuICAgIE91dHB1dCxcbiAgICBTaW1wbGVDaGFuZ2UsXG4gICAgU2ltcGxlQ2hhbmdlcyxcbiAgICBWaWV3Q2hpbGQsXG4gICAgRG9DaGVjayxcbiAgICBJdGVyYWJsZURpZmZlcixcbiAgICBJdGVyYWJsZURpZmZlcnMsXG4gICAgS2V5VmFsdWVEaWZmZXIsXG4gICAgS2V5VmFsdWVEaWZmZXJzLFxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgUGxvdGx5U2VydmljZSB9IGZyb20gJy4uL3Bsb3RseS5zZXJ2aWNlJztcbmltcG9ydCB7IFBsb3RseSB9IGZyb20gJy4uL3Bsb3RseS5pbnRlcmZhY2UnO1xuXG4vLyBAZHluYW1pY1xuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdwbG90bHktcGxvdCcsXG4gICAgdGVtcGxhdGU6IGA8ZGl2ICNwbG90IFthdHRyLmlkXT1cImRpdklkXCIgW2NsYXNzTmFtZV09XCJnZXRDbGFzc05hbWUoKVwiIFtuZ1N0eWxlXT1cInN0eWxlXCI+PC9kaXY+YCxcbiAgICBwcm92aWRlcnM6IFtQbG90bHlTZXJ2aWNlXSxcbn0pXG5leHBvcnQgY2xhc3MgUGxvdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzLCBPbkRlc3Ryb3ksIERvQ2hlY2sge1xuICAgIHByb3RlY3RlZCBkZWZhdWx0Q2xhc3NOYW1lID0gJ2pzLXBsb3RseS1wbG90JztcblxuICAgIHB1YmxpYyBwbG90bHlJbnN0YW5jZTogUGxvdGx5LlBsb3RseUhUTUxFbGVtZW50O1xuICAgIHB1YmxpYyByZXNpemVIYW5kbGVyPzogKGluc3RhbmNlOiBQbG90bHkuUGxvdGx5SFRNTEVsZW1lbnQpID0+IHZvaWQ7XG4gICAgcHVibGljIGxheW91dERpZmZlcjogS2V5VmFsdWVEaWZmZXI8c3RyaW5nLCBhbnk+O1xuICAgIHB1YmxpYyBkYXRhRGlmZmVyOiBJdGVyYWJsZURpZmZlcjxQbG90bHkuRGF0YT47XG5cbiAgICBAVmlld0NoaWxkKCdwbG90Jywge3N0YXRpYzogdHJ1ZX0pIHBsb3RFbDogRWxlbWVudFJlZjtcblxuICAgIEBJbnB1dCgpIGRhdGE/OiBQbG90bHkuRGF0YVtdO1xuICAgIEBJbnB1dCgpIGxheW91dD86IFBhcnRpYWw8UGxvdGx5LkxheW91dD47XG4gICAgQElucHV0KCkgY29uZmlnPzogUGFydGlhbDxQbG90bHkuQ29uZmlnPjtcbiAgICBASW5wdXQoKSBmcmFtZXM/OiBQYXJ0aWFsPFBsb3RseS5Db25maWc+W107XG4gICAgQElucHV0KCkgc3R5bGU/OiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9O1xuXG4gICAgQElucHV0KCkgZGl2SWQ/OiBzdHJpbmc7XG4gICAgQElucHV0KCkgcmV2aXNpb246IG51bWJlciA9IDA7XG4gICAgQElucHV0KCkgY2xhc3NOYW1lPzogc3RyaW5nIHwgc3RyaW5nW107XG4gICAgQElucHV0KCkgZGVidWc6IGJvb2xlYW4gPSBmYWxzZTtcbiAgICBASW5wdXQoKSB1c2VSZXNpemVIYW5kbGVyOiBib29sZWFuID0gZmFsc2U7XG5cbiAgICBASW5wdXQoKSB1cGRhdGVPbkxheW91dENoYW5nZSA9IHRydWU7XG4gICAgQElucHV0KCkgdXBkYXRlT25EYXRhQ2hhbmdlID0gdHJ1ZTtcbiAgICBASW5wdXQoKSB1cGRhdGVPbmx5V2l0aFJldmlzaW9uID0gZmFsc2U7XG5cbiAgICBAT3V0cHV0KCkgaW5pdGlhbGl6ZWQgPSBuZXcgRXZlbnRFbWl0dGVyPFBsb3RseS5GaWd1cmU+KCk7XG4gICAgQE91dHB1dCgpIHVwZGF0ZSA9IG5ldyBFdmVudEVtaXR0ZXI8UGxvdGx5LkZpZ3VyZT4oKTtcbiAgICBAT3V0cHV0KCkgcHVyZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPFBsb3RseS5GaWd1cmU+KCk7XG4gICAgQE91dHB1dCgpIGVycm9yID0gbmV3IEV2ZW50RW1pdHRlcjxFcnJvcj4oKTtcblxuICAgIEBPdXRwdXQoKSBhZnRlckV4cG9ydCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgYWZ0ZXJQbG90ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBhbmltYXRlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgYW5pbWF0aW5nRnJhbWUgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIGFuaW1hdGlvbkludGVycnVwdGVkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBhdXRvU2l6ZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgYmVmb3JlRXhwb3J0ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBidXR0b25DbGlja2VkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBjbGljayA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgcGxvdGx5X2NsaWNrID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBjbGlja0Fubm90YXRpb24gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIGRlc2VsZWN0ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBkb3VibGVDbGljayA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgZnJhbWV3b3JrID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBob3ZlciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgbGVnZW5kQ2xpY2sgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIGxlZ2VuZERvdWJsZUNsaWNrID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSByZWxheW91dCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgcmVzdHlsZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgcmVkcmF3ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBzZWxlY3RlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgc2VsZWN0aW5nID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBzbGlkZXJDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIHNsaWRlckVuZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgc2xpZGVyU3RhcnQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIHRyYW5zaXRpb25pbmcgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIHRyYW5zaXRpb25JbnRlcnJ1cHRlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgdW5ob3ZlciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgcmVsYXlvdXRpbmcgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIHRyZWVtYXBjbGljayA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgc3VuYnVyc3RjbGljayA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAgIHB1YmxpYyBldmVudE5hbWVzID0gWydhZnRlckV4cG9ydCcsICdhZnRlclBsb3QnLCAnYW5pbWF0ZWQnLCAnYW5pbWF0aW5nRnJhbWUnLCAnYW5pbWF0aW9uSW50ZXJydXB0ZWQnLCAnYXV0b1NpemUnLFxuICAgICAgICAnYmVmb3JlRXhwb3J0JywgJ2J1dHRvbkNsaWNrZWQnLCAnY2xpY2tBbm5vdGF0aW9uJywgJ2Rlc2VsZWN0JywgJ2RvdWJsZUNsaWNrJywgJ2ZyYW1ld29yaycsICdob3ZlcicsXG4gICAgICAgICdsZWdlbmRDbGljaycsICdsZWdlbmREb3VibGVDbGljaycsICdyZWxheW91dCcsICdyZXN0eWxlJywgJ3JlZHJhdycsICdzZWxlY3RlZCcsICdzZWxlY3RpbmcnLCAnc2xpZGVyQ2hhbmdlJyxcbiAgICAgICAgJ3NsaWRlckVuZCcsICdzbGlkZXJTdGFydCcsICd0cmFuc2l0aW9uaW5nJywgJ3RyYW5zaXRpb25JbnRlcnJ1cHRlZCcsICd1bmhvdmVyJywgJ3JlbGF5b3V0aW5nJywgJ3RyZWVtYXBjbGljaycsXG4gICAgICAgICdzdW5idXJzdGNsaWNrJ107XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHVibGljIHBsb3RseTogUGxvdGx5U2VydmljZSxcbiAgICAgICAgcHVibGljIGl0ZXJhYmxlRGlmZmVyczogSXRlcmFibGVEaWZmZXJzLFxuICAgICAgICBwdWJsaWMga2V5VmFsdWVEaWZmZXJzOiBLZXlWYWx1ZURpZmZlcnMsXG4gICAgKSB7IH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLmNyZWF0ZVBsb3QoKS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGZpZ3VyZSA9IHRoaXMuY3JlYXRlRmlndXJlKCk7XG4gICAgICAgICAgICB0aGlzLmluaXRpYWxpemVkLmVtaXQoZmlndXJlKTtcbiAgICAgICAgfSk7XG5cblxuICAgICAgICBpZiAodGhpcy5wbG90bHkuZGVidWcgJiYgdGhpcy5jbGljay5vYnNlcnZlcnMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgY29uc3QgbXNnID0gJ0RFUFJFQ0FURUQ6IFJlY29uc2lkZXIgdXNpbmcgYChwbG90bHlfY2xpY2spYCBpbnN0ZWFkIG9mIGAoY2xpY2spYCB0byBhdm9pZCBldmVudCBjb25mbGljdC4gJ1xuICAgICAgICAgICAgICAgICsgJ1BsZWFzZSBjaGVjayBodHRwczovL2dpdGh1Yi5jb20vcGxvdGx5L2FuZ3VsYXItcGxvdGx5LmpzI0ZBUSc7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKG1zZyk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBuZ09uRGVzdHJveSgpIHtcbiAgICAgICAgaWYgKHR5cGVvZiB0aGlzLnJlc2l6ZUhhbmRsZXIgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIHRoaXMuZ2V0V2luZG93KCkucmVtb3ZlRXZlbnRMaXN0ZW5lcigncmVzaXplJywgdGhpcy5yZXNpemVIYW5kbGVyIGFzIGFueSk7XG4gICAgICAgICAgICB0aGlzLnJlc2l6ZUhhbmRsZXIgPSB1bmRlZmluZWQ7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBmaWd1cmUgPSB0aGlzLmNyZWF0ZUZpZ3VyZSgpO1xuICAgICAgICB0aGlzLnB1cmdlLmVtaXQoZmlndXJlKTtcbiAgICAgICAgUGxvdGx5U2VydmljZS5yZW1vdmUodGhpcy5wbG90bHlJbnN0YW5jZSk7XG4gICAgfVxuXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xuICAgICAgICBsZXQgc2hvdWxkVXBkYXRlID0gZmFsc2U7XG5cbiAgICAgICAgY29uc3QgcmV2aXNpb246IFNpbXBsZUNoYW5nZSA9IGNoYW5nZXMucmV2aXNpb247XG4gICAgICAgIGlmIChyZXZpc2lvbiAmJiAhcmV2aXNpb24uaXNGaXJzdENoYW5nZSgpKSB7XG4gICAgICAgICAgICBzaG91bGRVcGRhdGUgPSB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgZGVidWc6IFNpbXBsZUNoYW5nZSA9IGNoYW5nZXMuZGVidWc7XG4gICAgICAgIGlmIChkZWJ1ZyAmJiAhZGVidWcuaXNGaXJzdENoYW5nZSgpKSB7XG4gICAgICAgICAgICBzaG91bGRVcGRhdGUgPSB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHNob3VsZFVwZGF0ZSkge1xuICAgICAgICAgICAgdGhpcy51cGRhdGVQbG90KCk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnVwZGF0ZVdpbmRvd1Jlc2l6ZUhhbmRsZXIoKTtcbiAgICB9XG5cbiAgICBuZ0RvQ2hlY2soKSB7XG4gICAgICAgIGlmICh0aGlzLnVwZGF0ZU9ubHlXaXRoUmV2aXNpb24pIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBzaG91bGRVcGRhdGUgPSBmYWxzZTtcblxuICAgICAgICBpZiAodGhpcy51cGRhdGVPbkxheW91dENoYW5nZSkge1xuICAgICAgICAgICAgaWYgKHRoaXMubGF5b3V0RGlmZmVyKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgbGF5b3V0SGFzRGlmZiA9IHRoaXMubGF5b3V0RGlmZmVyLmRpZmYodGhpcy5sYXlvdXQpO1xuICAgICAgICAgICAgICAgIGlmIChsYXlvdXRIYXNEaWZmKSB7XG4gICAgICAgICAgICAgICAgICAgIHNob3VsZFVwZGF0ZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmxheW91dCkge1xuICAgICAgICAgICAgICAgIHRoaXMubGF5b3V0RGlmZmVyID0gdGhpcy5rZXlWYWx1ZURpZmZlcnMuZmluZCh0aGlzLmxheW91dCkuY3JlYXRlKCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMubGF5b3V0RGlmZmVyID0gdW5kZWZpbmVkO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMudXBkYXRlT25EYXRhQ2hhbmdlKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5kYXRhRGlmZmVyKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgZGF0YUhhc0RpZmYgPSB0aGlzLmRhdGFEaWZmZXIuZGlmZih0aGlzLmRhdGEpO1xuICAgICAgICAgICAgICAgIGlmIChkYXRhSGFzRGlmZikge1xuICAgICAgICAgICAgICAgICAgICBzaG91bGRVcGRhdGUgPSB0cnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSBpZiAoQXJyYXkuaXNBcnJheSh0aGlzLmRhdGEpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kYXRhRGlmZmVyID0gdGhpcy5pdGVyYWJsZURpZmZlcnMuZmluZCh0aGlzLmRhdGEpLmNyZWF0ZSh0aGlzLmRhdGFEaWZmZXJUcmFja0J5KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kYXRhRGlmZmVyID0gdW5kZWZpbmVkO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHNob3VsZFVwZGF0ZSAmJiB0aGlzLnBsb3RseUluc3RhbmNlKSB7XG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVBsb3QoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGdldFdpbmRvdygpOiBhbnkge1xuICAgICAgICByZXR1cm4gd2luZG93O1xuICAgIH1cblxuICAgIGdldENsYXNzTmFtZSgpOiBzdHJpbmcge1xuICAgICAgICBsZXQgY2xhc3NlcyA9IFt0aGlzLmRlZmF1bHRDbGFzc05hbWVdO1xuXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMuY2xhc3NOYW1lKSkge1xuICAgICAgICAgICAgY2xhc3NlcyA9IGNsYXNzZXMuY29uY2F0KHRoaXMuY2xhc3NOYW1lKTtcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmNsYXNzTmFtZSkge1xuICAgICAgICAgICAgY2xhc3Nlcy5wdXNoKHRoaXMuY2xhc3NOYW1lKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBjbGFzc2VzLmpvaW4oJyAnKTtcbiAgICB9XG5cbiAgICBjcmVhdGVQbG90KCk6IFByb21pc2U8dm9pZD4ge1xuICAgICAgICByZXR1cm4gdGhpcy5wbG90bHkubmV3UGxvdCh0aGlzLnBsb3RFbC5uYXRpdmVFbGVtZW50LCB0aGlzLmRhdGEsIHRoaXMubGF5b3V0LCB0aGlzLmNvbmZpZywgdGhpcy5mcmFtZXMpLnRoZW4ocGxvdGx5SW5zdGFuY2UgPT4ge1xuICAgICAgICAgICAgdGhpcy5wbG90bHlJbnN0YW5jZSA9IHBsb3RseUluc3RhbmNlO1xuICAgICAgICAgICAgdGhpcy5nZXRXaW5kb3coKS5nZCA9IHRoaXMuZGVidWcgPyBwbG90bHlJbnN0YW5jZSA6IHVuZGVmaW5lZDtcblxuICAgICAgICAgICAgdGhpcy5ldmVudE5hbWVzLmZvckVhY2gobmFtZSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgZXZlbnROYW1lID0gYHBsb3RseV8ke25hbWUudG9Mb3dlckNhc2UoKX1gO1xuICAgICAgICAgICAgICAgIHBsb3RseUluc3RhbmNlLm9uKGV2ZW50TmFtZSwgKGRhdGE6IGFueSkgPT4gKHRoaXNbbmFtZV0gYXMgRXZlbnRFbWl0dGVyPHZvaWQ+KS5lbWl0KGRhdGEpKTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBwbG90bHlJbnN0YW5jZS5vbigncGxvdGx5X2NsaWNrJywgKGRhdGE6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuY2xpY2suZW1pdChkYXRhKTtcbiAgICAgICAgICAgICAgICB0aGlzLnBsb3RseV9jbGljay5lbWl0KGRhdGEpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIHRoaXMudXBkYXRlV2luZG93UmVzaXplSGFuZGxlcigpO1xuICAgICAgICB9LCBlcnIgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcignRXJyb3Igd2hpbGUgcGxvdHRpbmc6JywgZXJyKTtcbiAgICAgICAgICAgIHRoaXMuZXJyb3IuZW1pdChlcnIpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBjcmVhdGVGaWd1cmUoKTogUGxvdGx5LkZpZ3VyZSB7XG4gICAgICAgIGNvbnN0IHA6IGFueSA9IHRoaXMucGxvdGx5SW5zdGFuY2U7XG4gICAgICAgIGNvbnN0IGZpZ3VyZTogUGxvdGx5LkZpZ3VyZSA9IHtcbiAgICAgICAgICAgIGRhdGE6IHAuZGF0YSxcbiAgICAgICAgICAgIGxheW91dDogcC5sYXlvdXQsXG4gICAgICAgICAgICBmcmFtZXM6IHAuX3RyYW5zaXRpb25EYXRhID8gcC5fdHJhbnNpdGlvbkRhdGEuX2ZyYW1lcyA6IG51bGxcbiAgICAgICAgfTtcblxuICAgICAgICByZXR1cm4gZmlndXJlO1xuICAgIH1cblxuICAgIHVwZGF0ZVBsb3QoKSB7XG4gICAgICAgIGlmICghdGhpcy5wbG90bHlJbnN0YW5jZSkge1xuICAgICAgICAgICAgY29uc3QgZXJyb3IgPSBuZXcgRXJyb3IoYFBsb3RseSBjb21wb25lbnQgd2Fzbid0IGluaXRpYWxpemVkYCk7XG4gICAgICAgICAgICB0aGlzLmVycm9yLmVtaXQoZXJyb3IpO1xuICAgICAgICAgICAgdGhyb3cgZXJyb3I7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBsYXlvdXQgPSB7Li4udGhpcy5sYXlvdXR9O1xuXG4gICAgICAgIHJldHVybiB0aGlzLnBsb3RseS51cGRhdGUodGhpcy5wbG90bHlJbnN0YW5jZSwgdGhpcy5kYXRhLCBsYXlvdXQsIHRoaXMuY29uZmlnLCB0aGlzLmZyYW1lcykudGhlbigoKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBmaWd1cmUgPSB0aGlzLmNyZWF0ZUZpZ3VyZSgpO1xuICAgICAgICAgICAgdGhpcy51cGRhdGUuZW1pdChmaWd1cmUpO1xuICAgICAgICB9LCBlcnIgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcignRXJyb3Igd2hpbGUgdXBkYXRpbmcgcGxvdDonLCBlcnIpO1xuICAgICAgICAgICAgdGhpcy5lcnJvci5lbWl0KGVycik7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHVwZGF0ZVdpbmRvd1Jlc2l6ZUhhbmRsZXIoKSB7XG4gICAgICAgIGlmICh0aGlzLnVzZVJlc2l6ZUhhbmRsZXIpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnJlc2l6ZUhhbmRsZXIgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHRoaXMucmVzaXplSGFuZGxlciA9ICgpID0+IHRoaXMucGxvdGx5LnJlc2l6ZSh0aGlzLnBsb3RseUluc3RhbmNlKTtcbiAgICAgICAgICAgICAgICB0aGlzLmdldFdpbmRvdygpLmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMucmVzaXplSGFuZGxlciBhcyBhbnkpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiB0aGlzLnJlc2l6ZUhhbmRsZXIgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmdldFdpbmRvdygpLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMucmVzaXplSGFuZGxlciBhcyBhbnkpO1xuICAgICAgICAgICAgICAgIHRoaXMucmVzaXplSGFuZGxlciA9IHVuZGVmaW5lZDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIGRhdGFEaWZmZXJUcmFja0J5KF86IG51bWJlciwgaXRlbTogYW55KTogYW55IHtcbiAgICAgICAgY29uc3Qgb2JqID0gT2JqZWN0LmFzc2lnbih7fSwgaXRlbSwgeyB1aWQ6ICcnIH0pO1xuICAgICAgICByZXR1cm4gSlNPTi5zdHJpbmdpZnkob2JqKTtcbiAgICB9XG5cbn1cbiJdfQ==