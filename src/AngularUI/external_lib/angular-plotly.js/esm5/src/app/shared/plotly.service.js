import { __awaiter, __decorate, __generator, __values } from "tslib";
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import * as i0 from "@angular/core";
var PlotlyService = /** @class */ (function () {
    function PlotlyService() {
    }
    PlotlyService_1 = PlotlyService;
    PlotlyService.setModuleName = function (moduleName) {
        PlotlyService_1._moduleName = moduleName;
    };
    PlotlyService.setPlotly = function (plotly) {
        if (typeof plotly === 'object' && typeof plotly.react !== 'function') {
            throw new Error('Invalid plotly.js version. Please, use any version above 1.40.0');
        }
        PlotlyService_1._plotly = plotly;
    };
    PlotlyService.insert = function (instance) {
        var index = PlotlyService_1.instances.indexOf(instance);
        if (index === -1) {
            PlotlyService_1.instances.push(instance);
        }
        return instance;
    };
    PlotlyService.remove = function (div) {
        var index = PlotlyService_1.instances.indexOf(div);
        if (index >= 0) {
            PlotlyService_1.instances.splice(index, 1);
            PlotlyService_1._plotly.purge(div);
        }
    };
    Object.defineProperty(PlotlyService.prototype, "debug", {
        get: function () {
            return environment.production === false;
        },
        enumerable: true,
        configurable: true
    });
    PlotlyService.prototype.getInstanceByDivId = function (id) {
        var e_1, _a;
        try {
            for (var _b = __values(PlotlyService_1.instances), _c = _b.next(); !_c.done; _c = _b.next()) {
                var instance = _c.value;
                if (instance && instance.id === id) {
                    return instance;
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return undefined;
    };
    PlotlyService.prototype.getPlotly = function () {
        if (typeof PlotlyService_1._plotly === 'undefined') {
            var msg = PlotlyService_1._moduleName === 'ViaCDN'
                ? "Error loading Peer dependency plotly.js from CDN url"
                : "Peer dependency plotly.js isn't installed";
            throw new Error(msg);
        }
        return PlotlyService_1._plotly;
    };
    PlotlyService.prototype.waitFor = function (fn) {
        return new Promise(function (resolve) {
            var localFn = function () {
                fn() ? resolve() : setTimeout(localFn, 10);
            };
            localFn();
        });
    };
    // tslint:disable max-line-length
    PlotlyService.prototype.newPlot = function (div, data, layout, config, frames) {
        return __awaiter(this, void 0, void 0, function () {
            var obj;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.waitFor(function () { return _this.getPlotly() !== 'waiting'; })];
                    case 1:
                        _a.sent();
                        if (frames) {
                            obj = { data: data, layout: layout, config: config, frames: frames };
                            return [2 /*return*/, this.getPlotly().newPlot(div, obj).then(function () { return PlotlyService_1.insert(div); })];
                        }
                        return [2 /*return*/, this.getPlotly().newPlot(div, data, layout, config).then(function () { return PlotlyService_1.insert(div); })];
                }
            });
        });
    };
    PlotlyService.prototype.plot = function (div, data, layout, config, frames) {
        if (frames) {
            var obj = { data: data, layout: layout, config: config, frames: frames };
            return this.getPlotly().plot(div, obj);
        }
        return this.getPlotly().plot(div, data, layout, config);
    };
    PlotlyService.prototype.update = function (div, data, layout, config, frames) {
        if (frames) {
            var obj = { data: data, layout: layout, config: config, frames: frames };
            return this.getPlotly().react(div, obj);
        }
        return this.getPlotly().react(div, data, layout, config);
    };
    // tslint:enable max-line-length
    PlotlyService.prototype.resize = function (div) {
        return this.getPlotly().Plots.resize(div);
    };
    var PlotlyService_1;
    PlotlyService.instances = [];
    PlotlyService._plotly = undefined;
    PlotlyService._moduleName = undefined;
    PlotlyService.ɵprov = i0.ɵɵdefineInjectable({ factory: function PlotlyService_Factory() { return new PlotlyService(); }, token: PlotlyService, providedIn: "root" });
    PlotlyService = PlotlyService_1 = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], PlotlyService);
    return PlotlyService;
}());
export { PlotlyService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGxvdGx5LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hbmd1bGFyLXBsb3RseS5qcy8iLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3Bsb3RseS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTNDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQzs7QUFRN0Q7SUFBQTtLQXNHQztzQkF0R1ksYUFBYTtJQUtSLDJCQUFhLEdBQTNCLFVBQTRCLFVBQXNCO1FBQzlDLGVBQWEsQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFDO0lBQzNDLENBQUM7SUFFYSx1QkFBUyxHQUF2QixVQUF3QixNQUFXO1FBQy9CLElBQUksT0FBTyxNQUFNLEtBQUssUUFBUSxJQUFJLE9BQU8sTUFBTSxDQUFDLEtBQUssS0FBSyxVQUFVLEVBQUU7WUFDbEUsTUFBTSxJQUFJLEtBQUssQ0FBQyxpRUFBaUUsQ0FBQyxDQUFDO1NBQ3RGO1FBRUQsZUFBYSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7SUFDbkMsQ0FBQztJQUVhLG9CQUFNLEdBQXBCLFVBQXFCLFFBQWtDO1FBQ25ELElBQU0sS0FBSyxHQUFHLGVBQWEsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3hELElBQUksS0FBSyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ2QsZUFBYSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDMUM7UUFDRCxPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDO0lBRWEsb0JBQU0sR0FBcEIsVUFBcUIsR0FBNkI7UUFDOUMsSUFBTSxLQUFLLEdBQUcsZUFBYSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkQsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO1lBQ1osZUFBYSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLGVBQWEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3BDO0lBQ0wsQ0FBQztJQUVELHNCQUFXLGdDQUFLO2FBQWhCO1lBQ0ksT0FBTyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUssQ0FBQztRQUM1QyxDQUFDOzs7T0FBQTtJQUVNLDBDQUFrQixHQUF6QixVQUEwQixFQUFVOzs7WUFDaEMsS0FBdUIsSUFBQSxLQUFBLFNBQUEsZUFBYSxDQUFDLFNBQVMsQ0FBQSxnQkFBQSw0QkFBRTtnQkFBM0MsSUFBTSxRQUFRLFdBQUE7Z0JBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUU7b0JBQ2hDLE9BQU8sUUFBUSxDQUFDO2lCQUNuQjthQUNKOzs7Ozs7Ozs7UUFDRCxPQUFPLFNBQVMsQ0FBQztJQUNyQixDQUFDO0lBRU0saUNBQVMsR0FBaEI7UUFDSSxJQUFJLE9BQU8sZUFBYSxDQUFDLE9BQU8sS0FBSyxXQUFXLEVBQUU7WUFDOUMsSUFBTSxHQUFHLEdBQUcsZUFBYSxDQUFDLFdBQVcsS0FBSyxRQUFRO2dCQUM5QyxDQUFDLENBQUMsc0RBQXNEO2dCQUN4RCxDQUFDLENBQUMsMkNBQTJDLENBQUM7WUFFbEQsTUFBTSxJQUFJLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUN4QjtRQUVELE9BQU8sZUFBYSxDQUFDLE9BQU8sQ0FBQztJQUNqQyxDQUFDO0lBRVMsK0JBQU8sR0FBakIsVUFBa0IsRUFBaUI7UUFDL0IsT0FBTyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU87WUFDdkIsSUFBTSxPQUFPLEdBQUc7Z0JBQ1osRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQy9DLENBQUMsQ0FBQztZQUVGLE9BQU8sRUFBRSxDQUFDO1FBQ2QsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsaUNBQWlDO0lBQ3BCLCtCQUFPLEdBQXBCLFVBQXFCLEdBQW1CLEVBQUUsSUFBbUIsRUFBRSxNQUErQixFQUFFLE1BQStCLEVBQUUsTUFBaUM7Ozs7Ozs0QkFDOUoscUJBQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFNBQVMsRUFBRSxLQUFLLFNBQVMsRUFBOUIsQ0FBOEIsQ0FBQyxFQUFBOzt3QkFBeEQsU0FBd0QsQ0FBQzt3QkFFekQsSUFBSSxNQUFNLEVBQUU7NEJBQ0YsR0FBRyxHQUFHLEVBQUMsSUFBSSxNQUFBLEVBQUUsTUFBTSxRQUFBLEVBQUUsTUFBTSxRQUFBLEVBQUUsTUFBTSxRQUFBLEVBQUMsQ0FBQzs0QkFDM0Msc0JBQU8sSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQU0sT0FBQSxlQUFhLENBQUMsTUFBTSxDQUFDLEdBQVUsQ0FBQyxFQUFoQyxDQUFnQyxDQUFpQixFQUFDO3lCQUMxRzt3QkFFRCxzQkFBTyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFNLE9BQUEsZUFBYSxDQUFDLE1BQU0sQ0FBQyxHQUFVLENBQUMsRUFBaEMsQ0FBZ0MsQ0FBaUIsRUFBQzs7OztLQUMzSDtJQUVNLDRCQUFJLEdBQVgsVUFBWSxHQUE2QixFQUFFLElBQW1CLEVBQUUsTUFBK0IsRUFBRSxNQUErQixFQUFFLE1BQWlDO1FBQy9KLElBQUksTUFBTSxFQUFFO1lBQ1IsSUFBTSxHQUFHLEdBQUcsRUFBQyxJQUFJLE1BQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxNQUFNLFFBQUEsRUFBQyxDQUFDO1lBQzNDLE9BQU8sSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFpQixDQUFDO1NBQzFEO1FBRUQsT0FBTyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBaUIsQ0FBQztJQUM1RSxDQUFDO0lBRU0sOEJBQU0sR0FBYixVQUFjLEdBQTZCLEVBQUUsSUFBbUIsRUFBRSxNQUErQixFQUFFLE1BQStCLEVBQUUsTUFBaUM7UUFDakssSUFBSSxNQUFNLEVBQUU7WUFDUixJQUFNLEdBQUcsR0FBRyxFQUFDLElBQUksTUFBQSxFQUFFLE1BQU0sUUFBQSxFQUFFLE1BQU0sUUFBQSxFQUFFLE1BQU0sUUFBQSxFQUFDLENBQUM7WUFDM0MsT0FBTyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQWlCLENBQUM7U0FDM0Q7UUFFRCxPQUFPLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFpQixDQUFDO0lBQzdFLENBQUM7SUFDRCxnQ0FBZ0M7SUFFekIsOEJBQU0sR0FBYixVQUFjLEdBQTZCO1FBQ3ZDLE9BQU8sSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDOUMsQ0FBQzs7SUFwR2dCLHVCQUFTLEdBQStCLEVBQUUsQ0FBQztJQUMzQyxxQkFBTyxHQUFTLFNBQVMsQ0FBQztJQUMxQix5QkFBVyxHQUFnQixTQUFTLENBQUM7O0lBSDdDLGFBQWE7UUFIekIsVUFBVSxDQUFDO1lBQ1IsVUFBVSxFQUFFLE1BQU07U0FDckIsQ0FBQztPQUNXLGFBQWEsQ0FzR3pCO3dCQWhIRDtDQWdIQyxBQXRHRCxJQXNHQztTQXRHWSxhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUGxvdGx5IH0gZnJvbSAnLi9wbG90bHkuaW50ZXJmYWNlJztcbmltcG9ydCB7IGVudmlyb25tZW50IH0gZnJvbSAnLi4vLi4vZW52aXJvbm1lbnRzL2Vudmlyb25tZW50JztcblxudHlwZSBQbG90bHlOYW1lID0gJ1Bsb3RseScgfCAnVmlhQ0ROJyB8ICdWaWFXaW5kb3cnO1xuXG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgUGxvdGx5U2VydmljZSB7XG4gICAgcHJvdGVjdGVkIHN0YXRpYyBpbnN0YW5jZXM6IFBsb3RseS5QbG90bHlIVE1MRWxlbWVudFtdID0gW107XG4gICAgcHJvdGVjdGVkIHN0YXRpYyBfcGxvdGx5PzogYW55ID0gdW5kZWZpbmVkO1xuICAgIHByb3RlY3RlZCBzdGF0aWMgX21vZHVsZU5hbWU/OiBQbG90bHlOYW1lID0gdW5kZWZpbmVkO1xuXG4gICAgcHVibGljIHN0YXRpYyBzZXRNb2R1bGVOYW1lKG1vZHVsZU5hbWU6IFBsb3RseU5hbWUpIHtcbiAgICAgICAgUGxvdGx5U2VydmljZS5fbW9kdWxlTmFtZSA9IG1vZHVsZU5hbWU7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBzZXRQbG90bHkocGxvdGx5OiBhbnkpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBwbG90bHkgPT09ICdvYmplY3QnICYmIHR5cGVvZiBwbG90bHkucmVhY3QgIT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignSW52YWxpZCBwbG90bHkuanMgdmVyc2lvbi4gUGxlYXNlLCB1c2UgYW55IHZlcnNpb24gYWJvdmUgMS40MC4wJyk7XG4gICAgICAgIH1cblxuICAgICAgICBQbG90bHlTZXJ2aWNlLl9wbG90bHkgPSBwbG90bHk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBpbnNlcnQoaW5zdGFuY2U6IFBsb3RseS5QbG90bHlIVE1MRWxlbWVudCkge1xuICAgICAgICBjb25zdCBpbmRleCA9IFBsb3RseVNlcnZpY2UuaW5zdGFuY2VzLmluZGV4T2YoaW5zdGFuY2UpO1xuICAgICAgICBpZiAoaW5kZXggPT09IC0xKSB7XG4gICAgICAgICAgICBQbG90bHlTZXJ2aWNlLmluc3RhbmNlcy5wdXNoKGluc3RhbmNlKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gaW5zdGFuY2U7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyByZW1vdmUoZGl2OiBQbG90bHkuUGxvdGx5SFRNTEVsZW1lbnQpIHtcbiAgICAgICAgY29uc3QgaW5kZXggPSBQbG90bHlTZXJ2aWNlLmluc3RhbmNlcy5pbmRleE9mKGRpdik7XG4gICAgICAgIGlmIChpbmRleCA+PSAwKSB7XG4gICAgICAgICAgICBQbG90bHlTZXJ2aWNlLmluc3RhbmNlcy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgICAgICAgUGxvdGx5U2VydmljZS5fcGxvdGx5LnB1cmdlKGRpdik7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0IGRlYnVnKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gZW52aXJvbm1lbnQucHJvZHVjdGlvbiA9PT0gZmFsc2U7XG4gICAgfVxuXG4gICAgcHVibGljIGdldEluc3RhbmNlQnlEaXZJZChpZDogc3RyaW5nKTogUGxvdGx5LlBsb3RseUhUTUxFbGVtZW50IHwgdW5kZWZpbmVkIHtcbiAgICAgICAgZm9yIChjb25zdCBpbnN0YW5jZSBvZiBQbG90bHlTZXJ2aWNlLmluc3RhbmNlcykge1xuICAgICAgICAgICAgaWYgKGluc3RhbmNlICYmIGluc3RhbmNlLmlkID09PSBpZCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBpbnN0YW5jZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdW5kZWZpbmVkO1xuICAgIH1cblxuICAgIHB1YmxpYyBnZXRQbG90bHkoKSB7XG4gICAgICAgIGlmICh0eXBlb2YgUGxvdGx5U2VydmljZS5fcGxvdGx5ID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgY29uc3QgbXNnID0gUGxvdGx5U2VydmljZS5fbW9kdWxlTmFtZSA9PT0gJ1ZpYUNETidcbiAgICAgICAgICAgICAgICA/IGBFcnJvciBsb2FkaW5nIFBlZXIgZGVwZW5kZW5jeSBwbG90bHkuanMgZnJvbSBDRE4gdXJsYFxuICAgICAgICAgICAgICAgIDogYFBlZXIgZGVwZW5kZW5jeSBwbG90bHkuanMgaXNuJ3QgaW5zdGFsbGVkYDtcblxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKG1zZyk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gUGxvdGx5U2VydmljZS5fcGxvdGx5O1xuICAgIH1cblxuICAgIHByb3RlY3RlZCB3YWl0Rm9yKGZuOiAoKSA9PiBib29sZWFuKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgbG9jYWxGbiA9ICgpID0+IHtcbiAgICAgICAgICAgICAgICBmbigpID8gcmVzb2x2ZSgpIDogc2V0VGltZW91dChsb2NhbEZuLCAxMCk7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBsb2NhbEZuKCk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8vIHRzbGludDpkaXNhYmxlIG1heC1saW5lLWxlbmd0aFxuICAgIHB1YmxpYyBhc3luYyBuZXdQbG90KGRpdjogSFRNTERpdkVsZW1lbnQsIGRhdGE6IFBsb3RseS5EYXRhW10sIGxheW91dD86IFBhcnRpYWw8UGxvdGx5LkxheW91dD4sIGNvbmZpZz86IFBhcnRpYWw8UGxvdGx5LkNvbmZpZz4sIGZyYW1lcz86IFBhcnRpYWw8UGxvdGx5LkNvbmZpZz5bXSkge1xuICAgICAgICBhd2FpdCB0aGlzLndhaXRGb3IoKCkgPT4gdGhpcy5nZXRQbG90bHkoKSAhPT0gJ3dhaXRpbmcnKTtcblxuICAgICAgICBpZiAoZnJhbWVzKSB7XG4gICAgICAgICAgICBjb25zdCBvYmogPSB7ZGF0YSwgbGF5b3V0LCBjb25maWcsIGZyYW1lc307XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5nZXRQbG90bHkoKS5uZXdQbG90KGRpdiwgb2JqKS50aGVuKCgpID0+IFBsb3RseVNlcnZpY2UuaW5zZXJ0KGRpdiBhcyBhbnkpKSBhcyBQcm9taXNlPGFueT47XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gdGhpcy5nZXRQbG90bHkoKS5uZXdQbG90KGRpdiwgZGF0YSwgbGF5b3V0LCBjb25maWcpLnRoZW4oKCkgPT4gUGxvdGx5U2VydmljZS5pbnNlcnQoZGl2IGFzIGFueSkpIGFzIFByb21pc2U8YW55PjtcbiAgICB9XG5cbiAgICBwdWJsaWMgcGxvdChkaXY6IFBsb3RseS5QbG90bHlIVE1MRWxlbWVudCwgZGF0YTogUGxvdGx5LkRhdGFbXSwgbGF5b3V0PzogUGFydGlhbDxQbG90bHkuTGF5b3V0PiwgY29uZmlnPzogUGFydGlhbDxQbG90bHkuQ29uZmlnPiwgZnJhbWVzPzogUGFydGlhbDxQbG90bHkuQ29uZmlnPltdKSB7XG4gICAgICAgIGlmIChmcmFtZXMpIHtcbiAgICAgICAgICAgIGNvbnN0IG9iaiA9IHtkYXRhLCBsYXlvdXQsIGNvbmZpZywgZnJhbWVzfTtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmdldFBsb3RseSgpLnBsb3QoZGl2LCBvYmopIGFzIFByb21pc2U8YW55PjtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0aGlzLmdldFBsb3RseSgpLnBsb3QoZGl2LCBkYXRhLCBsYXlvdXQsIGNvbmZpZykgYXMgUHJvbWlzZTxhbnk+O1xuICAgIH1cblxuICAgIHB1YmxpYyB1cGRhdGUoZGl2OiBQbG90bHkuUGxvdGx5SFRNTEVsZW1lbnQsIGRhdGE6IFBsb3RseS5EYXRhW10sIGxheW91dD86IFBhcnRpYWw8UGxvdGx5LkxheW91dD4sIGNvbmZpZz86IFBhcnRpYWw8UGxvdGx5LkNvbmZpZz4sIGZyYW1lcz86IFBhcnRpYWw8UGxvdGx5LkNvbmZpZz5bXSkge1xuICAgICAgICBpZiAoZnJhbWVzKSB7XG4gICAgICAgICAgICBjb25zdCBvYmogPSB7ZGF0YSwgbGF5b3V0LCBjb25maWcsIGZyYW1lc307XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5nZXRQbG90bHkoKS5yZWFjdChkaXYsIG9iaikgYXMgUHJvbWlzZTxhbnk+O1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0UGxvdGx5KCkucmVhY3QoZGl2LCBkYXRhLCBsYXlvdXQsIGNvbmZpZykgYXMgUHJvbWlzZTxhbnk+O1xuICAgIH1cbiAgICAvLyB0c2xpbnQ6ZW5hYmxlIG1heC1saW5lLWxlbmd0aFxuXG4gICAgcHVibGljIHJlc2l6ZShkaXY6IFBsb3RseS5QbG90bHlIVE1MRWxlbWVudCk6IHZvaWQge1xuICAgICAgICByZXR1cm4gdGhpcy5nZXRQbG90bHkoKS5QbG90cy5yZXNpemUoZGl2KTtcbiAgICB9XG59XG4iXX0=