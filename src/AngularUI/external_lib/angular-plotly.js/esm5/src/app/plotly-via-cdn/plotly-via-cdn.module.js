import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlotComponent } from '../shared/plot/plot.component';
import { PlotlyService } from '../shared/plotly.service';
import { SharedModule } from '../shared/shared.module';
// @dynamic
var PlotlyViaCDNModule = /** @class */ (function () {
    function PlotlyViaCDNModule(plotlyService) {
        this.plotlyService = plotlyService;
        PlotlyService.setModuleName('ViaCDN');
    }
    PlotlyViaCDNModule_1 = PlotlyViaCDNModule;
    Object.defineProperty(PlotlyViaCDNModule, "plotlyVersion", {
        set: function (version) {
            var isOk = version === 'latest' || /^\d\.\d{1,2}\.\d{1,2}$/.test(version);
            if (!isOk) {
                throw new Error("Invalid plotly version. Please set 'latest' or version number (i.e.: 1.4.3)");
            }
            PlotlyViaCDNModule_1.loadViaCDN();
            PlotlyViaCDNModule_1._plotlyVersion = version;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PlotlyViaCDNModule, "plotlyBundle", {
        set: function (bundle) {
            var isOk = bundle === null || PlotlyViaCDNModule_1.plotlyBundleNames.indexOf(bundle) >= 0;
            if (!isOk) {
                var names = PlotlyViaCDNModule_1.plotlyBundleNames.map(function (n) { return "\"" + n + "\""; }).join(', ');
                throw new Error("Invalid plotly bundle. Please set to null for full or " + names + " for a partial bundle.");
            }
            PlotlyViaCDNModule_1._plotlyBundle = bundle;
        },
        enumerable: true,
        configurable: true
    });
    PlotlyViaCDNModule.loadViaCDN = function () {
        PlotlyService.setPlotly('waiting');
        var init = function () {
            var src = PlotlyViaCDNModule_1._plotlyBundle == null
                ? "https://cdn.plot.ly/plotly-" + PlotlyViaCDNModule_1._plotlyVersion + ".min.js"
                : "https://cdn.plot.ly/plotly-" + PlotlyViaCDNModule_1._plotlyBundle + "-" + PlotlyViaCDNModule_1._plotlyVersion + ".min.js";
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = src;
            script.onerror = function () { return console.error("Error loading plotly.js library from " + src); };
            var head = document.getElementsByTagName('head')[0];
            head.appendChild(script);
            var counter = 200; // equivalent of 10 seconds...
            var fn = function () {
                var plotly = window.Plotly;
                if (plotly) {
                    PlotlyService.setPlotly(plotly);
                }
                else if (counter > 0) {
                    counter--;
                    setTimeout(fn, 50);
                }
                else {
                    throw new Error("Error loading plotly.js library from " + src + ". Timeout.");
                }
            };
            fn();
        };
        setTimeout(init);
    };
    PlotlyViaCDNModule.forRoot = function (config) {
        var url = "https://github.com/plotly/angular-plotly.js#customizing-the-plotlyjs-bundle";
        throw new Error("[PlotlyViaCDNModule] forRoot method is deprecated. Please see: " + url);
    };
    var PlotlyViaCDNModule_1;
    PlotlyViaCDNModule._plotlyBundle = null;
    PlotlyViaCDNModule._plotlyVersion = 'latest';
    PlotlyViaCDNModule.plotlyBundleNames = ['basic', 'cartesian', 'geo', 'gl3d', 'gl2d', 'mapbox', 'finance'];
    PlotlyViaCDNModule.ctorParameters = function () { return [
        { type: PlotlyService }
    ]; };
    PlotlyViaCDNModule = PlotlyViaCDNModule_1 = __decorate([
        NgModule({
            imports: [CommonModule, SharedModule],
            declarations: [],
            exports: [PlotComponent]
        })
    ], PlotlyViaCDNModule);
    return PlotlyViaCDNModule;
}());
export { PlotlyViaCDNModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGxvdGx5LXZpYS1jZG4ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhci1wbG90bHkuanMvIiwic291cmNlcyI6WyJzcmMvYXBwL3Bsb3RseS12aWEtY2RuL3Bsb3RseS12aWEtY2RuLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFL0MsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzlELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFJdkQsV0FBVztBQU1YO0lBS0ksNEJBQW1CLGFBQTRCO1FBQTVCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzNDLGFBQWEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDMUMsQ0FBQzsyQkFQUSxrQkFBa0I7SUFTM0Isc0JBQVcsbUNBQWE7YUFBeEIsVUFBeUIsT0FBZTtZQUNwQyxJQUFNLElBQUksR0FBRyxPQUFPLEtBQUssUUFBUSxJQUFJLHdCQUF3QixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM1RSxJQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNQLE1BQU0sSUFBSSxLQUFLLENBQUMsNkVBQTZFLENBQUMsQ0FBQzthQUNsRztZQUVELG9CQUFrQixDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ2hDLG9CQUFrQixDQUFDLGNBQWMsR0FBRyxPQUFPLENBQUM7UUFDaEQsQ0FBQzs7O09BQUE7SUFFRCxzQkFBVyxrQ0FBWTthQUF2QixVQUF3QixNQUF3QjtZQUM1QyxJQUFNLElBQUksR0FBRyxNQUFNLEtBQUssSUFBSSxJQUFJLG9CQUFrQixDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDMUYsSUFBSSxDQUFDLElBQUksRUFBRTtnQkFDUCxJQUFNLEtBQUssR0FBRyxvQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxPQUFJLENBQUMsT0FBRyxFQUFSLENBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDakYsTUFBTSxJQUFJLEtBQUssQ0FBQywyREFBeUQsS0FBSywyQkFBd0IsQ0FBQyxDQUFDO2FBQzNHO1lBRUQsb0JBQWtCLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQztRQUM5QyxDQUFDOzs7T0FBQTtJQUVNLDZCQUFVLEdBQWpCO1FBQ0ksYUFBYSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUVuQyxJQUFNLElBQUksR0FBRztZQUNULElBQU0sR0FBRyxHQUFHLG9CQUFrQixDQUFDLGFBQWEsSUFBSSxJQUFJO2dCQUNoRCxDQUFDLENBQUMsZ0NBQThCLG9CQUFrQixDQUFDLGNBQWMsWUFBUztnQkFDMUUsQ0FBQyxDQUFDLGdDQUE4QixvQkFBa0IsQ0FBQyxhQUFhLFNBQUksb0JBQWtCLENBQUMsY0FBYyxZQUFTLENBQUM7WUFFbkgsSUFBTSxNQUFNLEdBQXNCLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbkUsTUFBTSxDQUFDLElBQUksR0FBRyxpQkFBaUIsQ0FBQztZQUNoQyxNQUFNLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztZQUNqQixNQUFNLENBQUMsT0FBTyxHQUFHLGNBQU0sT0FBQSxPQUFPLENBQUMsS0FBSyxDQUFDLDBDQUF3QyxHQUFLLENBQUMsRUFBNUQsQ0FBNEQsQ0FBQztZQUVwRixJQUFNLElBQUksR0FBb0IsUUFBUSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3ZFLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFekIsSUFBSSxPQUFPLEdBQUcsR0FBRyxDQUFDLENBQUMsOEJBQThCO1lBRWpELElBQU0sRUFBRSxHQUFHO2dCQUNQLElBQU0sTUFBTSxHQUFJLE1BQWMsQ0FBQyxNQUFNLENBQUM7Z0JBQ3RDLElBQUksTUFBTSxFQUFFO29CQUNSLGFBQWEsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7aUJBQ25DO3FCQUFNLElBQUksT0FBTyxHQUFHLENBQUMsRUFBRTtvQkFDcEIsT0FBTyxFQUFHLENBQUM7b0JBQ1gsVUFBVSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztpQkFDdEI7cUJBQU07b0JBQ0gsTUFBTSxJQUFJLEtBQUssQ0FBQywwQ0FBd0MsR0FBRyxlQUFZLENBQUMsQ0FBQztpQkFDNUU7WUFDTCxDQUFDLENBQUM7WUFFRixFQUFFLEVBQUUsQ0FBQztRQUNULENBQUMsQ0FBQztRQUVGLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNyQixDQUFDO0lBRU0sMEJBQU8sR0FBZCxVQUFlLE1BQWtDO1FBQzdDLElBQU0sR0FBRyxHQUFHLDZFQUE2RSxDQUFDO1FBQzFGLE1BQU0sSUFBSSxLQUFLLENBQUMsb0VBQWtFLEdBQUssQ0FBQyxDQUFDO0lBQzdGLENBQUM7O0lBbkVjLGdDQUFhLEdBQVksSUFBSSxDQUFDO0lBQzlCLGlDQUFjLEdBQVcsUUFBUSxDQUFDO0lBQzFDLG9DQUFpQixHQUF1QixDQUFDLE9BQU8sRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUFDOztnQkFFaEYsYUFBYTs7SUFMdEMsa0JBQWtCO1FBTDlCLFFBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLFlBQVksRUFBRSxZQUFZLENBQUM7WUFDckMsWUFBWSxFQUFFLEVBQUU7WUFDaEIsT0FBTyxFQUFFLENBQUMsYUFBYSxDQUFDO1NBQzNCLENBQUM7T0FDVyxrQkFBa0IsQ0FxRTlCO0lBQUQseUJBQUM7Q0FBQSxBQXJFRCxJQXFFQztTQXJFWSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcblxuaW1wb3J0IHsgUGxvdENvbXBvbmVudCB9IGZyb20gJy4uL3NoYXJlZC9wbG90L3Bsb3QuY29tcG9uZW50JztcbmltcG9ydCB7IFBsb3RseVNlcnZpY2UgfSBmcm9tICcuLi9zaGFyZWQvcGxvdGx5LnNlcnZpY2UnO1xuaW1wb3J0IHsgU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi4vc2hhcmVkL3NoYXJlZC5tb2R1bGUnO1xuXG5leHBvcnQgdHlwZSBQbG90bHlCdW5kbGVOYW1lID0gJ2Jhc2ljJyB8ICdjYXJ0ZXNpYW4nIHwgJ2dlbycgfCAnZ2wzZCcgfCAnZ2wyZCcgfCAnbWFwYm94JyB8ICdmaW5hbmNlJztcblxuLy8gQGR5bmFtaWNcbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSwgU2hhcmVkTW9kdWxlXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtdLFxuICAgIGV4cG9ydHM6IFtQbG90Q29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBQbG90bHlWaWFDRE5Nb2R1bGUge1xuICAgIHByaXZhdGUgc3RhdGljIF9wbG90bHlCdW5kbGU/OiBzdHJpbmcgPSBudWxsO1xuICAgIHByaXZhdGUgc3RhdGljIF9wbG90bHlWZXJzaW9uOiBzdHJpbmcgPSAnbGF0ZXN0JztcbiAgICBzdGF0aWMgcGxvdGx5QnVuZGxlTmFtZXM6IFBsb3RseUJ1bmRsZU5hbWVbXSA9IFsnYmFzaWMnLCAnY2FydGVzaWFuJywgJ2dlbycsICdnbDNkJywgJ2dsMmQnLCAnbWFwYm94JywgJ2ZpbmFuY2UnXTtcblxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBwbG90bHlTZXJ2aWNlOiBQbG90bHlTZXJ2aWNlKSB7XG4gICAgICAgIFBsb3RseVNlcnZpY2Uuc2V0TW9kdWxlTmFtZSgnVmlhQ0ROJyk7XG4gICAgfVxuXG4gICAgc3RhdGljIHNldCBwbG90bHlWZXJzaW9uKHZlcnNpb246IHN0cmluZykge1xuICAgICAgICBjb25zdCBpc09rID0gdmVyc2lvbiA9PT0gJ2xhdGVzdCcgfHwgL15cXGRcXC5cXGR7MSwyfVxcLlxcZHsxLDJ9JC8udGVzdCh2ZXJzaW9uKTtcbiAgICAgICAgaWYgKCFpc09rKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYEludmFsaWQgcGxvdGx5IHZlcnNpb24uIFBsZWFzZSBzZXQgJ2xhdGVzdCcgb3IgdmVyc2lvbiBudW1iZXIgKGkuZS46IDEuNC4zKWApO1xuICAgICAgICB9XG5cbiAgICAgICAgUGxvdGx5VmlhQ0ROTW9kdWxlLmxvYWRWaWFDRE4oKTtcbiAgICAgICAgUGxvdGx5VmlhQ0ROTW9kdWxlLl9wbG90bHlWZXJzaW9uID0gdmVyc2lvbjtcbiAgICB9XG5cbiAgICBzdGF0aWMgc2V0IHBsb3RseUJ1bmRsZShidW5kbGU6IFBsb3RseUJ1bmRsZU5hbWUpIHtcbiAgICAgICAgY29uc3QgaXNPayA9IGJ1bmRsZSA9PT0gbnVsbCB8fCBQbG90bHlWaWFDRE5Nb2R1bGUucGxvdGx5QnVuZGxlTmFtZXMuaW5kZXhPZihidW5kbGUpID49IDA7XG4gICAgICAgIGlmICghaXNPaykge1xuICAgICAgICAgICAgY29uc3QgbmFtZXMgPSBQbG90bHlWaWFDRE5Nb2R1bGUucGxvdGx5QnVuZGxlTmFtZXMubWFwKG4gPT4gYFwiJHtufVwiYCkuam9pbignLCAnKTtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgSW52YWxpZCBwbG90bHkgYnVuZGxlLiBQbGVhc2Ugc2V0IHRvIG51bGwgZm9yIGZ1bGwgb3IgJHtuYW1lc30gZm9yIGEgcGFydGlhbCBidW5kbGUuYCk7XG4gICAgICAgIH1cblxuICAgICAgICBQbG90bHlWaWFDRE5Nb2R1bGUuX3Bsb3RseUJ1bmRsZSA9IGJ1bmRsZTtcbiAgICB9XG5cbiAgICBzdGF0aWMgbG9hZFZpYUNETigpOiB2b2lkIHtcbiAgICAgICAgUGxvdGx5U2VydmljZS5zZXRQbG90bHkoJ3dhaXRpbmcnKTtcblxuICAgICAgICBjb25zdCBpbml0ID0gKCkgPT4ge1xuICAgICAgICAgICAgY29uc3Qgc3JjID0gUGxvdGx5VmlhQ0ROTW9kdWxlLl9wbG90bHlCdW5kbGUgPT0gbnVsbFxuICAgICAgICAgICAgICAgID8gYGh0dHBzOi8vY2RuLnBsb3QubHkvcGxvdGx5LSR7UGxvdGx5VmlhQ0ROTW9kdWxlLl9wbG90bHlWZXJzaW9ufS5taW4uanNgXG4gICAgICAgICAgICAgICAgOiBgaHR0cHM6Ly9jZG4ucGxvdC5seS9wbG90bHktJHtQbG90bHlWaWFDRE5Nb2R1bGUuX3Bsb3RseUJ1bmRsZX0tJHtQbG90bHlWaWFDRE5Nb2R1bGUuX3Bsb3RseVZlcnNpb259Lm1pbi5qc2A7XG5cbiAgICAgICAgICAgIGNvbnN0IHNjcmlwdDogSFRNTFNjcmlwdEVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcbiAgICAgICAgICAgIHNjcmlwdC50eXBlID0gJ3RleHQvamF2YXNjcmlwdCc7XG4gICAgICAgICAgICBzY3JpcHQuc3JjID0gc3JjO1xuICAgICAgICAgICAgc2NyaXB0Lm9uZXJyb3IgPSAoKSA9PiBjb25zb2xlLmVycm9yKGBFcnJvciBsb2FkaW5nIHBsb3RseS5qcyBsaWJyYXJ5IGZyb20gJHtzcmN9YCk7XG5cbiAgICAgICAgICAgIGNvbnN0IGhlYWQ6IEhUTUxIZWFkRWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdoZWFkJylbMF07XG4gICAgICAgICAgICBoZWFkLmFwcGVuZENoaWxkKHNjcmlwdCk7XG5cbiAgICAgICAgICAgIGxldCBjb3VudGVyID0gMjAwOyAvLyBlcXVpdmFsZW50IG9mIDEwIHNlY29uZHMuLi5cblxuICAgICAgICAgICAgY29uc3QgZm4gPSAoKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgcGxvdGx5ID0gKHdpbmRvdyBhcyBhbnkpLlBsb3RseTtcbiAgICAgICAgICAgICAgICBpZiAocGxvdGx5KSB7XG4gICAgICAgICAgICAgICAgICAgIFBsb3RseVNlcnZpY2Uuc2V0UGxvdGx5KHBsb3RseSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChjb3VudGVyID4gMCkge1xuICAgICAgICAgICAgICAgICAgICBjb3VudGVyIC0tO1xuICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZuLCA1MCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBFcnJvciBsb2FkaW5nIHBsb3RseS5qcyBsaWJyYXJ5IGZyb20gJHtzcmN9LiBUaW1lb3V0LmApO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIGZuKCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgc2V0VGltZW91dChpbml0KTtcbiAgICB9XG5cbiAgICBzdGF0aWMgZm9yUm9vdChjb25maWc6IFBhcnRpYWw8e3ZlcnNpb246IHN0cmluZ30+KTogbmV2ZXIge1xuICAgICAgICBjb25zdCB1cmwgPSBcImh0dHBzOi8vZ2l0aHViLmNvbS9wbG90bHkvYW5ndWxhci1wbG90bHkuanMjY3VzdG9taXppbmctdGhlLXBsb3RseWpzLWJ1bmRsZVwiO1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYFtQbG90bHlWaWFDRE5Nb2R1bGVdIGZvclJvb3QgbWV0aG9kIGlzIGRlcHJlY2F0ZWQuIFBsZWFzZSBzZWU6ICR7dXJsfWApO1xuICAgIH1cbn1cbiJdfQ==