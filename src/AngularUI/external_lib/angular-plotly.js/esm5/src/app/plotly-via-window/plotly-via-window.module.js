import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlotComponent } from '../shared/plot/plot.component';
import { PlotlyService } from '../shared/plotly.service';
import { SharedModule } from '../shared/shared.module';
var PlotlyViaWindowModule = /** @class */ (function () {
    function PlotlyViaWindowModule() {
        var plotly = window.Plotly;
        if (typeof plotly === 'undefined') {
            throw new Error("Plotly object not found on window.");
        }
        PlotlyService.setPlotly(plotly);
    }
    PlotlyViaWindowModule.forRoot = function () {
        var url = "https://github.com/plotly/angular-plotly.js#plotly-via-window-module";
        throw new Error("[PlotlyViaWindowModule] forRoot method is deprecated. Please see: " + url);
    };
    PlotlyViaWindowModule = __decorate([
        NgModule({
            imports: [CommonModule, SharedModule],
            declarations: [],
            exports: [PlotComponent]
        })
    ], PlotlyViaWindowModule);
    return PlotlyViaWindowModule;
}());
export { PlotlyViaWindowModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGxvdGx5LXZpYS13aW5kb3cubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhci1wbG90bHkuanMvIiwic291cmNlcyI6WyJzcmMvYXBwL3Bsb3RseS12aWEtd2luZG93L3Bsb3RseS12aWEtd2luZG93Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFL0MsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzlELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFRdkQ7SUFDSTtRQUNJLElBQU0sTUFBTSxHQUFJLE1BQWMsQ0FBQyxNQUFNLENBQUM7UUFFdEMsSUFBSSxPQUFPLE1BQU0sS0FBSyxXQUFXLEVBQUU7WUFDL0IsTUFBTSxJQUFJLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDO1NBQ3pEO1FBRUQsYUFBYSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRU0sNkJBQU8sR0FBZDtRQUNJLElBQU0sR0FBRyxHQUFHLHNFQUFzRSxDQUFDO1FBQ25GLE1BQU0sSUFBSSxLQUFLLENBQUMsdUVBQXFFLEdBQUssQ0FBQyxDQUFDO0lBQ2hHLENBQUM7SUFkUSxxQkFBcUI7UUFMakMsUUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsWUFBWSxFQUFFLFlBQVksQ0FBQztZQUNyQyxZQUFZLEVBQUUsRUFBRTtZQUNoQixPQUFPLEVBQUUsQ0FBQyxhQUFhLENBQUM7U0FDM0IsQ0FBQztPQUNXLHFCQUFxQixDQWVqQztJQUFELDRCQUFDO0NBQUEsQUFmRCxJQWVDO1NBZlkscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbmltcG9ydCB7IFBsb3RDb21wb25lbnQgfSBmcm9tICcuLi9zaGFyZWQvcGxvdC9wbG90LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBQbG90bHlTZXJ2aWNlIH0gZnJvbSAnLi4vc2hhcmVkL3Bsb3RseS5zZXJ2aWNlJztcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uL3NoYXJlZC9zaGFyZWQubW9kdWxlJztcblxuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGUsIFNoYXJlZE1vZHVsZV0sXG4gICAgZGVjbGFyYXRpb25zOiBbXSxcbiAgICBleHBvcnRzOiBbUGxvdENvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgUGxvdGx5VmlhV2luZG93TW9kdWxlIHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgY29uc3QgcGxvdGx5ID0gKHdpbmRvdyBhcyBhbnkpLlBsb3RseTtcblxuICAgICAgICBpZiAodHlwZW9mIHBsb3RseSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgUGxvdGx5IG9iamVjdCBub3QgZm91bmQgb24gd2luZG93LmApO1xuICAgICAgICB9XG5cbiAgICAgICAgUGxvdGx5U2VydmljZS5zZXRQbG90bHkocGxvdGx5KTtcbiAgICB9XG5cbiAgICBzdGF0aWMgZm9yUm9vdCgpOiBuZXZlciB7XG4gICAgICAgIGNvbnN0IHVybCA9IFwiaHR0cHM6Ly9naXRodWIuY29tL3Bsb3RseS9hbmd1bGFyLXBsb3RseS5qcyNwbG90bHktdmlhLXdpbmRvdy1tb2R1bGVcIjtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBbUGxvdGx5VmlhV2luZG93TW9kdWxlXSBmb3JSb290IG1ldGhvZCBpcyBkZXByZWNhdGVkLiBQbGVhc2Ugc2VlOiAke3VybH1gKTtcbiAgICB9XG59XG4iXX0=