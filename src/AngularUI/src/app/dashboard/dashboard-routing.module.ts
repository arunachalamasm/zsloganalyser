import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { UploadComponent} from './upload/upload.component'
const routes: Routes = [
  {
    path: '',

    component: DashboardComponent,
    data: { title: ':: Dashboard ::' }
  },
  {
    path: '',
    children: [

      {
        path: 'upload', component: UploadComponent,
        data: { title: ':: Upload  ::' }
      },
]
  }]


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DashboardRoutingModule { }
