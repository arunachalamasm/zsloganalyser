import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GlobalModule } from '../global/global.module';
import { UploadComponent } from './upload/upload.component';


@NgModule({
  declarations: [
    DashboardComponent,
    UploadComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    GlobalModule,
  ],

})
export class DashboardModule { }  
