import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class loginService {
  constructor(private _http: HttpClient) {
  }
  login(object, otp) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data'
      })
    };
    const body = '';
    return this._http.post(environment.backend.baseURL + body, httpOptions);
  }
}