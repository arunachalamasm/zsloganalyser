import { ToastrService } from 'ngx-toastr';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs";
import { Injectable, Compiler } from "@angular/core";
import { Router } from "@angular/router";
import { tap } from 'rxjs/operators';
    
// import swal from 'sweetalert';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(
        private _router: Router,
        private _toastr: ToastrService,
        private _compiler: Compiler
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this._compiler.clearCache();
        const clonedreq = req.clone({
            setHeaders: {
                Authorization: "BEARER  " + localStorage.getItem('LAVELLE_ACCESS_TOKEN'),
                "Access-Control-Allow-Origin":"*"
            }
        });

        return next.handle(clonedreq)
        .pipe(tap(
                succ => { },
                err => {
                    if (err.status === 401) {
                        this.clearData();
                    }
                    else if (err.status === 404) {
                        const validationError = err.error;
                        //   swal("Warning!", validationError.description, "warning");
                    }
                    else if (err.status === 400) {
                        const validationError = err.error;
                        //  swal("Warning!", validationError.description, "warning");
                    }
                }
                ));
    }
    clearData() {
        this._toastr.error("Session Timed Out", "Oops");
        localStorage.clear();
        this._router.navigate(['/authentication/page-login']);
    }
}