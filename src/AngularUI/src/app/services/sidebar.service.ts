import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';

@Injectable({
	providedIn: 'root'
})
export class SidebarService {

	private messageSource = new BehaviorSubject(false);
	currentMessage = this.messageSource.asObservable();

	public sidebarVisible: boolean = true;

	constructor() { }

	toggle() {
		this.sidebarVisible = !this.sidebarVisible;
	}

	userRole: string;
	getStatus() {
		return this.sidebarVisible;
	}

	changeMessage(message: boolean) {
		this.messageSource.next(message);
	}

	//to set fullScreen mode for master and sys-admin And sideBar for net-admin and user
	setStatus() {
		this.userRole = localStorage.getItem('user_role');
		if (this.userRole == 'sys-admin' || this.userRole == 'master') {
			this.sidebarVisible = false;
		} else {
			this.sidebarVisible = true;
		}
		return this.sidebarVisible;
	}
}
