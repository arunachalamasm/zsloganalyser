import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { HttpClient } from '@angular/common/http';
@Injectable({
	providedIn: 'root'
})
export class LanguageService {
	constructor(private http: HttpClient) { }
	private English = 'assets/En/En.json';
	//private Chinese = 'assets/En/Ch.json';
	getlanguage() {
		var _language = localStorage.getItem("language");
		if (_language == 'en') {
			return this.http.get(this.English)
		}
		else if (_language == 'ch') {
			//return this.http.get(this.Chinese)
		}
	}
}
