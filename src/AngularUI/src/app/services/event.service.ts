import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable({
	providedIn: 'root'
})
export class EventService {

	public getEvents(): Observable<any> {
		const dateObj = new Date();
		const yearMonth = dateObj.getUTCFullYear() + '-' + (dateObj.getUTCMonth() + 1);
		let data: any = [];
		return of(data);
	}
}
