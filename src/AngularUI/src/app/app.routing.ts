﻿import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { GlobalComponent } from './global/global.component';

export const routes: Routes = [
    { path: '', redirectTo: 'authentication', pathMatch: 'full' },
    {
        path: '',
        component: GlobalComponent,
        children: [
            {
                path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').
                    then(m => m.DashboardModule),
            },
        ]
    },
    {
        path: 'authentication', loadChildren: () => import('./authentication/authentication.module').
            then(m => m.AuthenticationModule)
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true, scrollPositionRestoration: 'enabled' });