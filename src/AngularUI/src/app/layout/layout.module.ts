import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { PageLoaderComponent } from './page-loader/page-loader.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
	imports: [
		CommonModule,
		NgbModule,
		RouterModule,
		FormsModule,
		ReactiveFormsModule,
		NgxPaginationModule
	],
	declarations: [HeaderComponent, SidebarComponent, PageLoaderComponent],
	exports: [HeaderComponent, SidebarComponent, PageLoaderComponent]
})
export class LayoutModule { }
