import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class MenuService {

    constructor(private _http: HttpClient) {
    }
    //Send menu data to other components
    private data: BehaviorSubject<string> = new BehaviorSubject<any>([])
    menuData = this.data.asObservable();
    SendMenuData(arg) {
        this.data.next(arg);
    }

    private Headerdata: BehaviorSubject<string> = new BehaviorSubject<any>([])
    _Headerdata = this.Headerdata.asObservable();
    sendDataToHeader(arg) {
        this.Headerdata.next(arg);
    }

    private Commitdata: BehaviorSubject<string> = new BehaviorSubject<any>([])
    _Commitdata = this.Commitdata.asObservable();
    sendCommitToHeader(arg) {
        this.Commitdata.next(arg);
    }



    getUserNameRoles() {
        var userDetails;
        return userDetails = {
            "username": localStorage.getItem("username"),
            "id": localStorage.getItem("user_id"),
            "role": localStorage.getItem("user_role"),
            "allow_manage": localStorage.getItem("allow_manage"),
            "customer_id": localStorage.getItem("customer_id"),
            "hex_id": localStorage.getItem("hex_id"),
            "customer_hex_id": localStorage.getItem("customer_hex_id"),
            "enable_notifications": localStorage.getItem("enable_notifications"),
            "permission": localStorage.getItem("permission")
        }
    }

}