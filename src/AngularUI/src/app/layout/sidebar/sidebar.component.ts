import { Component, Input, Output, EventEmitter, OnDestroy, ChangeDetectorRef, HostListener } from '@angular/core';
import { ThemeService } from '../../services/theme.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { MenuService } from './menu.service';
import { SidebarService } from '../../services/sidebar.service';

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {

	hamburgerclicked: boolean = false;
	@Input() sidebarVisible: boolean = true;
	@Input() navTab: string = "menu";
	@Input() currentActiveMenu;
	@Input() currentActiveSubMenu;
	@Output() changeNavTabEvent = new EventEmitter();
	@Output() activeInactiveMenuEvent = new EventEmitter();
	@Output() MenuData = new EventEmitter();
	public themeClass: string = "theme-orange";
	public darkClass: string = "";
	private ngUnsubscribe = new Subject();

	public isResizing: boolean = false;
	sidebarclicked: boolean = false;
	@HostListener('document:click')
	clickout() {
		if (this.hamburgerclicked) {
			this.hamburgerclicked = false;
			this.sidebarService.changeMessage(false);
			return;
		}
		if (!this.sidebarclicked && this.themeService.smallScreenMenu) {
			this.themeService.showHideMenu();
		}
		this.sidebarclicked = false;
	}

	toggleFullWidth() {
		this.isResizing = true;
		this.sidebarService.toggle();
		this.sidebarVisible = this.sidebarService.getStatus();
		let that = this;
		setTimeout(function () {
			that.isResizing = false;
			that.cdr.detectChanges();
		}, 400);
	}

	userRole: string;
	TempData = [
		{
			"Path": "javascript:void(0)",
			"Title": "dashboard",
			"Icon": "fas fa-th-large",
			"Class": "has-arrow",
			"Label": "",
			"LabelClass": "",
			"ExtraLink": false,
			"lstOfMainMenuWithSubMenu": [
				{
					"Path": "/dashboard/upload",
					"Title": "Upload",
					"Icon": "fal fa-arrow-square-up",
					"IconColor": "#FF7A5A",
					"Class": "has-arrow",
					"Label": "",
					"LabelClass": "",
					"ExtraLink": false,
					"PopupText": "",
					"lstOfMainMenuWithSubMenu": []
				},
			]
		},
	
	]




	userDetails = <any>{};
	constructor(private themeService: ThemeService, private _route: Router,
		private _menuService: MenuService, private sidebarService: SidebarService,
		private cdr: ChangeDetectorRef) {
	
		this.themeService.themeClassChange.pipe(takeUntil(this.ngUnsubscribe)).subscribe(themeClass => {
			this.themeClass = themeClass;
		});
		this.themeService.darkClassChange.pipe(takeUntil(this.ngUnsubscribe)).subscribe(darkClass => {
			this.darkClass = darkClass;
		});
	}

	ngAfterViewInit() {
		setTimeout(() => {
			this.sidebarVisible = this.sidebarService.setStatus();//to set fullScreen for sys-admin and master
		});
	}

	tooglemaster: boolean = false;
	tooglesys: boolean = false;
	tooglenet: boolean = true;
	toogleUser: boolean = false;
	ngOnInit() {

		this.sidebarService.currentMessage.pipe(takeUntil(this.ngUnsubscribe)).subscribe(value => this.hamburgerclicked = value);
	
		
	}

	ngOnDestroy() {
		this.ngUnsubscribe.next();
		this.ngUnsubscribe.complete();
	}

	changeNavTab(tab: string) {
		this.navTab = tab;
	}

	logout() {
		localStorage.clear();
		this._route.navigate(['/authentication/page-login']);
	}

	activeInactiveMenu(menuItem: string) {
		this.activeInactiveMenuEvent.emit({ 'item': menuItem });
	}

	changeTheme(theme: string) {
		this.themeService.themeChange(theme);
	}

	changeDarkMode(darkClass: string) {
		this.themeService.changeDarkMode(darkClass);
	}

	getMenuData(arg) {
		this._menuService.SendMenuData(arg)
	}

}
