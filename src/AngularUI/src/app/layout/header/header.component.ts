import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, HostListener } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { ThemeService } from '../../services/theme.service';
import { Router } from '@angular/router';
import { MenuService } from '../sidebar/menu.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SidebarService } from '../../services/sidebar.service';


@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css'],
	providers: [NgbDropdownConfig]
})
export class HeaderComponent implements OnInit {
	@Input() sidebarVisible: boolean = true;
	@Input() showNotifMenu: boolean = false;
	@Input() showToggleMenu: boolean = false;
	@Input() darkClass: string = "";
	@Output() toggleSettingDropMenuEvent = new EventEmitter();
	@Output() toggleNotificationDropMenuEvent = new EventEmitter();

	user = <any>{};
	constructor(private toastr: ToastrService, private config: NgbDropdownConfig, private themeService: ThemeService, private _menuService: MenuService,
		private _route: Router,
		private fb: FormBuilder,
		private sidebarService: SidebarService,
		private cdr: ChangeDetectorRef,) {
		config.placement = 'bottom-right';
	}

	headerForm: FormGroup;
	viewlabel: string = 'view more....';
	lefticon = 'block';
	downicon = 'none';
	ngOnInit() {
		const mediaQuery = window.matchMedia('(max-width: 768px)')
		mediaQuery.addListener(this.handleTabletChange)
		this.headerForm = this.fb.group({
			Key: null,
			Value: null
		})
	}

	handleTabletChange(e) {
		var div = document.getElementById("collapseExample");
		if (e.matches) {
			div.classList.remove("show");
		}
		else {
			div.classList.add("show");
		}
	}

	headerFormModal = {
		Key: null,
		Value: null
	}

	showtable(index) {
		var div = document.getElementById("tbl" + index);
		if (div.style.display === "none") {
			div.style.display = "block";
		} else {
			div.style.display = "none";
		}
	}

	hideshow() {
		if (this.lefticon == 'block') {
			this.downicon = 'block';
			this.lefticon = 'none';
		}
		else {
			this.downicon = 'none';
			this.lefticon = 'block';
		}
	}


	//sidebar
	public isResizing: boolean = false;

	toggleFullWidth() {
		this.isResizing = true;
		this.sidebarService.toggle();
		this.sidebarVisible = this.sidebarService.getStatus();
		let that = this;
		setTimeout(function () {
			that.isResizing = false;
			that.cdr.detectChanges();
		}, 400);
	}


	toggleSettingDropMenu() {
		this.toggleSettingDropMenuEvent.emit();
	}

	toggleNotificationDropMenu() {
		this.toggleNotificationDropMenuEvent.emit();
	}

	toggleSideMenu() {
		this.sidebarService.changeMessage(true);
		this.themeService.showHideMenu();
	}

	logout() {

		localStorage.clear();
		this._route.navigate(['/authentication/page-login']);
	}
	@HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
		this.themeService.hideMenu();
	}


}
