<?php
	
    /* Author: Abhinav Bansal
     * Script to decrypt files generated from Zscaler App
     */
    /*Algorithm for encryption:
     
     File:         ${svn-home}/mobile/client/trunk/zs/sdk/platform/common/AuthenticationLibrary/AuthenticationLibrary/ZSAuth.cpp
     Function:     bool createEncryptedZipArchive(std::string &destination, std::vector<std::string> &fileList) {
     Keys          ${svn-home}/mobile/server/MobileAdmin2.0/trunk/certs/clientLogsEncryption{Private|Public}Key.pem
     
     1. Generate a session key - 16 bytes
     2. Generate a random IV - 16 bytes
     3. EncryptedLog = Encrypt the zip-file using this key and IV with aes-128-cbc mode
     4. EncryptedKeyAndIV = Encrypt the hex(key):hex(IV) using RSA 2048 algorithm
     5. Append the EncyrptedKeyAndIV to EncryptedLog
     6. Append "+" Length of EncryptedKeyAndIV;
     
     Algoritm for decryption:
     
     1. pos = Fetch the pos of "+"
     2. length = Fetch the Length of encryptedKeyAndIV from pos to Length of data
     3. EncryptedKeyAndIV = Extract encryptedKeyAndIV from pos to length characters
     4. (Key,IV) = Decypt the EncryptedKeyAndIV using RSA private key
     5. Decrypt log data using (key,IV) with aes-128-cbc mode
     6. Output the logs with Content-Type application/zip
     */
    function endsWith($string, $endString) 
    { 
        $len = strlen($endString); 
        if ($len == 0) { 
            return true; 
        } 
         return (substr($string, -$len) === $endString); 
    } 

    $tmpFile = "AZscaler-".time()."tmp.zip";

    /* After Changes */
    $encFile =$argv[1];
    $privFile = $argv[2];
    $outputFile = $argv[3];
    
    $x = file_get_contents($encFile);


    $pos = strrpos($x,"+");
    #echo "pos:$pos\n";die;
    $len = substr($x,$pos+1);
	#echo "len:$len\n";die;
    if($len > 500){
        echo 'Invalid Input File';
        exit;
    }
    $encryptedKeyAndIV = substr($x,$pos-$len,$len);
    #echo "encryptedKeyAndIV:$encryptedKeyAndIV\n";die;
    $data = substr($x, 0, $pos-$len-1);
	#echo "data:$data";die;
    file_put_contents($encFile,$data);
    $privatekeycontents = file_get_contents($privFile);
    $pkey = openssl_pkey_get_private($privatekeycontents,"pass");
	#echo $pkey;die;
    openssl_private_decrypt(base64_decode($encryptedKeyAndIV),$decryptedKeyAndIv,$pkey);
    #echo "decryptedKeyAndIV:$decryptedKeyAndIv";die;
    $pos1 = strpos($decryptedKeyAndIv,":");
    $hexkey = trim(substr($decryptedKeyAndIv,0,$pos1));
    #echo "hexkey:$hexkey";
    $hexiv=trim(substr($decryptedKeyAndIv,$pos1+1));
    #echo "hexiv:$hexiv";die;
    if(strlen($hexkey) < 16 || strlen($hexiv) < 16){
        echo "key retrieval failed";
        exit;
    }
    system("openssl enc -in $encFile -out $tmpFile -d -a");
	sleep(3);
    system("openssl enc -d -aes-128-cbc -in $tmpFile -out $outputFile -K $hexkey -iv $hexiv");
    
    if(file_exists($outputFile) == false){
        echo "Script failed. Output file: ".$outputFile;
        exit;
    }

	unlink($tmpFile);
    
?>
