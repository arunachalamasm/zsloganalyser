package com.zscalar.log.reader.utility.batch.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zscalar.log.reader.utility.batch.model.LogsUploadJobSettings;
import com.zscalar.log.reader.utility.constants.ZscalarConstants;
import com.zscalar.log.reader.utility.domain.ConfigDetails;
import com.zscalar.log.reader.utility.domain.ErrorToken;
import com.zscalar.log.reader.utility.entities.Settings;
import com.zscalar.log.reader.utility.repository.ESRepository;
import com.zscalar.log.reader.utility.service.SettingsService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class LogsUploadJobListener implements JobExecutionListener {

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private LogsUploadJobSettings logsUploadJobSettings;

    @Autowired
    private ConfigDetails configDetails;

    @Autowired
    private ESRepository eSRepository;

    private Map<String, Settings> settingsMap;

    ObjectMapper mapper = new ObjectMapper();

    Logger logger = LoggerFactory.getLogger(LogsUploadJobListener.class);

    @Override
    public void beforeJob(JobExecution jobExecution) {
        logger.info("Fetching Job Settings");
        List<Settings> settingsList = settingsService.getAllSetting();
        settingsMap = new HashMap<>();
        if (!settingsList.isEmpty()) {
            for (Settings settings : settingsList) {
                settingsMap.put(settings.getName(), settings);
            }
        }
        try {
            constructSettingsForJob(settingsMap);
        } catch (JsonProcessingException e) {
            logger.error("Error parsing settings. Exception is : {}", e.getMessage());
            e.printStackTrace();
        }
    }

    private void constructSettingsForJob(Map<String, Settings> settingsMap) throws JsonProcessingException {
        logger.debug("Constructing settings for the Job");
        if (settingsMap.containsKey("OtherSettings")) {
            constructOtherSettings();
        }
        if (settingsMap.containsKey("ErrorTokens")) {
            constructTokenSettings();
        }
    }

    private void constructOtherSettings() throws JsonProcessingException {
        logger.info("Fetching Other Settings");
        String otherSettings = settingsMap.get(ZscalarConstants.OTHER_SETTINGS).getContent();
        Map<String, Object> otherSettingsmap = mapper.readValue(otherSettings, Map.class);
        constructFileNameFilter(otherSettingsmap);
        String retain = (String) otherSettingsmap.get(ZscalarConstants.RETAIN);
        logger.info("Retain data is set to: {}", retain);
        logsUploadJobSettings.setRetain(retain);
    }

    private void constructTokenSettings() throws JsonProcessingException {
        logger.info("Fetching Token Settings");
        String errorTokenSettings = settingsMap.get(ZscalarConstants.ERROR_TOKENS).getContent();
        if (!StringUtils.isEmpty(errorTokenSettings)) {
            List<ErrorToken> errorTokenList = (List<ErrorToken>) mapper.readValue(errorTokenSettings, new TypeReference<List<ErrorToken>>() {
            });
            List<ErrorToken> filterTokensList = new ArrayList<>();
            Map<String, ErrorToken> errorValuesMap = new HashMap<>();
            errorTokenList.forEach(errorToken -> {
                if (errorToken.getType() != null) {
                    if (errorToken.getType().equalsIgnoreCase(ZscalarConstants.FIELD)) {
                        filterTokensList.add(errorToken);
                    } else if (errorToken.getType().equalsIgnoreCase(ZscalarConstants.ERROR) && !StringUtils.isEmpty(errorToken.getToken())) {
                        errorValuesMap.put(errorToken.getToken().replaceAll(ZscalarConstants.ALPHA_REGEX, ZscalarConstants.ALL_REGEX), errorToken);
                    }
                }
            });
            logsUploadJobSettings.setErrorTokenMap(errorValuesMap);
            logger.debug("ErrorTokens available : {}", errorValuesMap);
            logsUploadJobSettings.setFilterTokenList(filterTokensList);
            logger.debug("Fetched FilterTokens: {}", filterTokensList);
        }
    }

    private void constructFileNameFilter(Map<String, Object> otherSettingsmap) {
        logger.info("Fetching fileName filters");
        Map<String, String> fileNameMap = new HashMap<>();
        List<LinkedHashMap<String, String>> nameFilter = (ArrayList) otherSettingsmap.get(ZscalarConstants.FILE_NAME_FILTER);
        for (LinkedHashMap<String, String> nameFilterMap : nameFilter) {
            for (Map.Entry<String, String> fileNametest : nameFilterMap.entrySet()) {
                if (fileNameMap.containsKey(fileNametest.getKey())) {
                    String value = fileNameMap.get(fileNametest.getKey()) + ZscalarConstants.COMMA + fileNametest.getValue();
                    fileNameMap.put(fileNametest.getKey(), value);
                } else {
                    fileNameMap.put(fileNametest.getKey(), fileNametest.getValue());
                }
            }
        }
        logsUploadJobSettings.setNameFilterMap(fileNameMap);
        logger.debug("FileName filter which are available are: {}", fileNameMap);
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        logger.info("Saving Config details to ES");
        try {
            eSRepository.save(configDetails, logsUploadJobSettings.getConfigIndexName());
            configDetails.setAppInfo(null);
            configDetails.setForwardingProfile(null);
            configDetails.setSystemInfo(null);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occurred while saving config details to ES. Exception is : {}", e.getMessage());
        }
    }
}
