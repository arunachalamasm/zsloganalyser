package com.zscalar.log.reader.utility.batch.model;


import com.zscalar.log.reader.utility.domain.ErrorToken;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@Data
public class LogsUploadJobSettings {

    private String retain;

    private   Map<String, ErrorToken> errorTokenMap;

    private Map<String, String> nameFilterMap;

    private List<ErrorToken> filterTokenList;
    
    private String os;
    
    private String configIndexName;
}
