package com.zscalar.log.reader.utility.service.impl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileSystems;

import java.util.*;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.zscalar.log.reader.utility.dto.response.BatchJobStatusResponse;
import com.zscalar.log.reader.utility.dto.response.DateRangeResponse;
import com.zscalar.log.reader.utility.exception.ErrorCode;
import com.zscalar.log.reader.utility.exception.LogReaderException;
import com.zscalar.log.reader.utility.utils.FileEncryption;
import javafx.fxml.LoadException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobExecutionNotRunningException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.launch.NoSuchJobExecutionException;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.zscalar.log.reader.utility.batch.model.LogsUploadJobSettings;
import com.zscalar.log.reader.utility.constants.ResponseMessage;
import com.zscalar.log.reader.utility.constants.ZscalarConstants;
import com.zscalar.log.reader.utility.dto.CustomJobParameter;
import com.zscalar.log.reader.utility.dto.CustomJobParameters;
import com.zscalar.log.reader.utility.dto.response.ZipUploadResponse;
import com.zscalar.log.reader.utility.repository.ESRepository;
import com.zscalar.log.reader.utility.repository.FieldTokensRepo;
import com.zscalar.log.reader.utility.service.LogParserService;

@Service
public class LogParserServiceImpl implements LogParserService {

	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	Job job;

	@Autowired
	private ESRepository eSRepository;

	@Autowired
	private FieldTokensRepo fieldTokensRepo;
	
	@Autowired
	private ConfigParserImpl configParserImpl;
	
	@Autowired
	private LogsUploadJobSettings logsUploadJobSettings;

	@Autowired
	private FileEncryption fileEncryption;

	@Autowired
	private SimpleJobOperator jobOperator;

	@Autowired
	private JobExplorer jobExplorer;
	
	@Autowired
	private JobRepository jobRepository;
	
	List<File> files = new ArrayList<>();
	List<File> xmlZipfiles = new ArrayList<>();

	Logger logger = LoggerFactory.getLogger(LogParserServiceImpl.class);
	
	@Override
	public ZipUploadResponse parseLog(MultipartFile file, String urlFileName, String userName, String obfuscate,Date startDate,Date endDate) throws Exception {
		ZipUploadResponse response = new ZipUploadResponse();

		//check if the file is encrypted

		String decryptedFilePath = fileEncryption.checkIfFileIsEncrypted(file,urlFileName);

		List<File> logFiles = getLogFiles(file,urlFileName,startDate,endDate,decryptedFilePath);
		String fileName = (null!=urlFileName)?urlFileName:file.getOriginalFilename();

		CustomJobParameter customJobParameter = new CustomJobParameter(logFiles,file.getOriginalFilename(),userName.toLowerCase(), obfuscate);

		JobParameters jobParameters = new JobParametersBuilder()
				.addParameter("customparam", new CustomJobParameters<CustomJobParameter>(customJobParameter))
				.toJobParameters();

		logsUploadJobSettings.setConfigIndexName(("config_"+userName+"_"+fileName).toLowerCase());

		try{
			jobLauncher.run(job, jobParameters);
		}
		catch (JobExecutionAlreadyRunningException | JobRestartException |
		  JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
		  throw new LogReaderException(e, ErrorCode.JOB_EXCEPTION);
		}
		 
		logFiles.stream().forEach(logFile -> {
			logFile.delete();
		});
		files = new ArrayList<>();
		
		response.setMessage(ResponseMessage.SUCCESS_RESPONSE);
		response.setStatus(HttpStatus.OK.value());
		// response.setData(logDetails);
		return response;
	}

	@Override
	public DateRangeResponse getDateRange(MultipartFile file) {
		try{
			DateRangeResponse dateRangeResponse = new DateRangeResponse();
			List<File> logFiles = getLogFiles(file, null, null, null, null);
			logFiles.sort(new Comparator() {
				public int compare(Object o1, Object o2) {
					return new Long(((File) o2).lastModified()).compareTo(new Long(((File) o1).lastModified()));
				}
			});
			String startDate = logFiles.get(0).getName().split("_")[1].split(".log")[0];
			dateRangeResponse.setStartDate(startDate);
			String endDate = logFiles.get(logFiles.size() - 1).getName().split("_")[1].split(".log")[0];
			dateRangeResponse.setEndDate(endDate);
			return dateRangeResponse;
		}
		catch (Exception e){
			throw new LogReaderException(e, ErrorCode.INPUT_OUTPUT_EXCEPTION);
		}
	}

	private List<File> getLogFiles(MultipartFile multipartFile, String urlFileName, Date startDate, Date endDate, String decryptedFilePath) {
		try{
			if (StringUtils.isEmpty(decryptedFilePath)) {
				List<File> files = new ArrayList<>();
				String tempDir = System.getProperty("java.io.tmpdir");
				String fileName = (null != urlFileName) ? urlFileName : multipartFile.getOriginalFilename();
				File targetFile = new File(tempDir + fileName);

				InputStream initialStream = multipartFile.getInputStream();
				byte[] buffer = new byte[initialStream.available()];
				initialStream.read(buffer);


				try (OutputStream outStream = new FileOutputStream(targetFile)) {
					outStream.write(buffer);
				} catch (IOException e) {
					throw new LogReaderException(e, ErrorCode.INPUT_OUTPUT_EXCEPTION);
				}
				files.addAll(unzip(tempDir + fileName, startDate, endDate));
				targetFile.delete();
			} else {
				files.addAll(unzip(decryptedFilePath, startDate, endDate));
			}
			return files;
		}
		catch (IOException e){
			throw new LogReaderException(e, ErrorCode.INPUT_OUTPUT_EXCEPTION);
		}
	}
	
	
	public List<File> unzip(String zipFile,Date startDate,Date endDate) {
		String newPath = FileSystems.getDefault().getPath("").toAbsolutePath().toString();
		int BUFFER = 2048;
		File file = new File(zipFile);

		ZipFile zip = null;
		try {
			zip = new ZipFile(file);
		} catch (IOException e) {
			throw new LogReaderException(e, ErrorCode.INPUT_OUTPUT_EXCEPTION);
		}

		new File(newPath).mkdir();
		Enumeration zipFileEntries = zip.entries();

// Process each entry
		while (zipFileEntries.hasMoreElements()) {
			// grab a zip file entry
			ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();

			String currentEntry = entry.getName();

			File destFile = new File(newPath, currentEntry);
			destFile = new File(newPath, destFile.getName());
			File destinationParent = destFile.getParentFile();

			// create the parent directory structure if needed
			destinationParent.mkdirs();
			if (!entry.isDirectory()) {
				try{
					BufferedInputStream is = new BufferedInputStream(zip.getInputStream(entry));
					int currentByte;
					// establish buffer for writing file
					byte data[] = new byte[BUFFER];

					// write the current file to disk
					FileOutputStream fos = new FileOutputStream(destFile);
					BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER);

					// read and write until last byte is encountered
					while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
						dest.write(data, 0, currentByte);
					}
					dest.flush();
					dest.close();
					is.close();
				}
				catch (IOException e){
					throw new LogReaderException(e, ErrorCode.INPUT_OUTPUT_EXCEPTION);
				}
			}
			if (currentEntry.endsWith(".log")) {
				if (destFile.getName().equalsIgnoreCase("AppInfo.log") || destFile.getName().equalsIgnoreCase("SystemInfo.log")) {
					configParserImpl.parse(destFile.getAbsolutePath());
				} else {
					/* Check if file has to be processed or not*/
					try {
						String dateTime = currentEntry.split("_")[1].split(".log")[0];
						DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd-HH-mm-ss.SSSSSS");
						Date fileDate = format.parseLocalDateTime(dateTime).toDate();
						if (checkIfFileHasToBeProcessed(startDate, endDate,
								fileDate))
							files.add(destFile);
					}catch(Exception exception){
						files.add(destFile);
						logger.info("We are not processing {} file",destFile);
					}
				}
			} else if (currentEntry.endsWith(".xml")) {
				xmlZipfiles.add(destFile);
				configParserImpl.parse(destFile.getAbsolutePath());
			} else if (currentEntry.endsWith(".zip")) {
				xmlZipfiles.add(destFile);
				unzip(destFile.getAbsolutePath(), startDate, endDate);
			}
			xmlZipfiles.stream().forEach(File::delete);
		}
		return files;
	}

	private boolean checkIfFileHasToBeProcessed(Date startDate, Date endDate, Date fileDate){
		if(startDate != null && endDate != null){
			return fileDate.after(startDate) && fileDate.before(endDate);
		}
		else if(startDate != null){
           return  fileDate.after(startDate);
		}
		else if(endDate != null){
            return fileDate.before(endDate);
		}
		else{
            return Boolean.TRUE;
		}
	}

	@Override
	public AtomicReference<String> stopBatch(String userName) {
		AtomicReference<String> jobStatus =  new AtomicReference<>("No job is running");
		try {
			Set<Long> executions = (Set<Long>) jobOperator.getRunningExecutions("logsUploadJob");
			if (!executions.isEmpty()) {
				executions.parallelStream().forEach(job -> {
					try {
						JobExecution jobExecution = jobExplorer.getJobExecution(job);
						BatchStatus batchStatus = jobExecution.getStatus();
						if (!batchStatus.equals(BatchStatus.STOPPING)) {
							String params = jobOperator.getParameters(job);
							if (params.contains(userName)) {
								jobOperator.stop(job);
								jobExecution.setEndTime(new Date());
								jobRepository.update(jobExecution);
								jobStatus.set(ZscalarConstants.BATCH_STOP_SUCCESS_MESSAGE);
							}
						}
					} catch (NoSuchJobExecutionException | JobExecutionNotRunningException e) {
						jobStatus.set(ZscalarConstants.BATCH_NO_RUNNING_MESSAGE);
					}
				});
			}
		}catch (NoSuchJobException e){
			jobStatus.set(ZscalarConstants.BATCH_NO_RUNNING_MESSAGE);
		}
		return jobStatus;
	}

	@Override
	public BatchJobStatusResponse getJobStatus(String userName) {
		BatchJobStatusResponse batchJobStatusResponse = new BatchJobStatusResponse();
		AtomicReference<Boolean> jobStatus = new AtomicReference<>(false);
		try {
			Set<Long> executions = (Set<Long>) jobOperator.getRunningExecutions("logsUploadJob");
			if (!executions.isEmpty()) {
				for(Long job : executions) {
					try {
						JobExecution jobExecution = jobExplorer.getJobExecution(job);
						BatchStatus batchStatus = jobExecution.getStatus();
						if (!batchStatus.equals(BatchStatus.STOPPING)) {
							String params = jobOperator.getParameters(job);
							if (params.contains(userName)) {
								logger.info("Job is Running");
								batchJobStatusResponse.setZipFileName(
										params.substring(62).split(ZscalarConstants.COMMA)[0].split(ZscalarConstants.EQUALS)[1].replace("\'", ZscalarConstants.EMPTY));
								batchJobStatusResponse.setRunning(Boolean.TRUE);
								return batchJobStatusResponse; // Only 1 batch will be running at a time
							}
						}
					} catch (NoSuchJobExecutionException e) {
						batchJobStatusResponse.setRunning(Boolean.FALSE);
					}

				}
			}
		}catch (NoSuchJobException e){
			batchJobStatusResponse.setRunning(Boolean.FALSE);
		}
		return batchJobStatusResponse;
	}
}
