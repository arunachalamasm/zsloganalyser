package com.zscalar.log.reader.utility.repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zscalar.log.reader.utility.annotation.Document;
import com.zscalar.log.reader.utility.config.ElasticsearchConfig;
import com.zscalar.log.reader.utility.domain.ConfigDetails;
import com.zscalar.log.reader.utility.domain.LogDetail;
import com.zscalar.log.reader.utility.dto.QueryRequestWithPagination;
import com.zscalar.log.reader.utility.dto.response.LogQueryResponse;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.client.IndicesClient;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.*;

@Repository
public class ESRepository<T> {

	@Autowired
	private RestHighLevelClient client;

	@Autowired
	private Gson gson;

	private Document document;

	public LogQueryResponse getLogs(QueryRequestWithPagination request, SearchRequest searchRequest)
			throws IOException {
		SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
		return LogQueryResponse.newInstanceFrom(response, request.getOffset(), System.currentTimeMillis());
	}




	private <T extends LogDetail> List<T> findAll(SearchResponse searchResponse, Class<T> tClass) {
        List<T> list = new ArrayList<>();
        SearchHit[] searchHits = searchResponse.getHits().getHits();
        for (SearchHit searchHit : searchHits) {
            if (!StringUtils.isEmpty(searchHit.getSourceAsString())) {
                T t = gson.fromJson(searchHit.getSourceAsString(), tClass);
                if (Objects.nonNull(t))
                list.add(t);
            }
        }
        return list;
    }

	public <T extends LogDetail> List<LogDetail> getAllLogsByFilesName(List<String> fileNames, Class<LogDetail> tClass) throws IOException {
		QueryBuilder queryBuilder = QueryBuilders.termsQuery("fileName",fileNames);
		document = tClass.getAnnotation(Document.class);
		SearchRequest searchRequest = new SearchRequest(document.indexName());
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		searchSourceBuilder.query(queryBuilder);
		searchSourceBuilder.trackTotalHits(true);
		searchRequest.source(searchSourceBuilder);
		SearchResponse searchResponse= client.search(searchRequest, RequestOptions.DEFAULT);
		List<LogDetail> findAll = findAll(searchResponse, tClass);
		return null;
	}

	public  void createNewIndex(Class<T> tClass , Map<String, Object> mapping) throws IOException {
		document = tClass.getAnnotation(Document.class);
		GetIndexRequest getRequest = new GetIndexRequest(document.indexName());
		getRequest.humanReadable(true);
		getRequest.includeDefaults(false);

		IndicesClient indices = client.indices();
		if (client.indices().exists(getRequest, RequestOptions.DEFAULT)) {
			return;
		}
		CreateIndexRequest createRequest = new CreateIndexRequest(document.indexName());
		createRequest.settings(Settings.builder().put("index.number_of_shards", 1).put("index.number_of_replicas", 1));
		createRequest.mapping(mapping);
		CreateIndexResponse indexResponse = client.indices().create(createRequest, RequestOptions.DEFAULT);
	}

	public void deleteAllIndexDocument(Class<T> tClass) throws IOException {
		document = tClass.getAnnotation(Document.class);
		DeleteByQueryRequest request = new DeleteByQueryRequest(document.indexName())
				.setQuery(QueryBuilders.matchAllQuery());
		request.setRefresh(true);
		request.setBatchSize(100);
		BulkByScrollResponse deleteByQuery = client.deleteByQuery(request, RequestOptions.DEFAULT);
	}

	public void deleteUserIndex(String indexName) throws IOException {
		GetIndexRequest getRequest = new GetIndexRequest(indexName);
		getRequest.humanReadable(true);
		getRequest.includeDefaults(false);
		if (!client.indices().exists(getRequest, RequestOptions.DEFAULT)) {
			return;
		}
		DeleteIndexRequest request = new DeleteIndexRequest(indexName);
		AcknowledgedResponse deleteIndexResponse = client.indices().delete(request, RequestOptions.DEFAULT);
	}

	public void createNewUserIndex(String userName, Map<String, Object> mapping) throws IOException {
		GetIndexRequest getRequest = new GetIndexRequest(userName);
		getRequest.humanReadable(true);
		getRequest.includeDefaults(false);
		if (client.indices().exists(getRequest, RequestOptions.DEFAULT)) {
			return;
		}
		CreateIndexRequest createRequest = new CreateIndexRequest(userName);
		createRequest.settings(Settings.builder().put("index.number_of_shards", 1).put("index.number_of_replicas", 1));
		createRequest.mapping(mapping);
		CreateIndexResponse indexResponse = client.indices().create(createRequest, RequestOptions.DEFAULT);
	}

	public <T extends LogDetail> void saveAll(List<? extends LogDetail> tList, String indexName) throws IOException {
		BulkRequest bulkRequest = new BulkRequest();
		tList.stream().forEach(t->{
			IndexRequest indexRequest = new IndexRequest(indexName);
			gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
			indexRequest.source(gson.toJson(t), XContentType.JSON);
			bulkRequest.add(indexRequest);
		});
		bulkRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
		Optional<BulkRequest> opt= Optional.of(bulkRequest);
		if(opt.isPresent()) 
		client.bulk(bulkRequest, RequestOptions.DEFAULT);
	}
	
	public  void save(ConfigDetails confDetails, String indexName) {
		try {
			IndexRequest indexRequest = new IndexRequest(indexName);
			indexRequest.source(gson.toJson(confDetails), XContentType.JSON);
			indexRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
			IndexResponse response=client.index(indexRequest, RequestOptions.DEFAULT);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
