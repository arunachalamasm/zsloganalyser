package com.zscalar.log.reader.utility.service.impl;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import com.zscalar.log.reader.utility.exception.ErrorCode;
import com.zscalar.log.reader.utility.exception.LogReaderException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zscalar.log.reader.utility.domain.ConfigDetails;

@Service
public class ConfigParserImpl {

	@Autowired
	private ConfigDetails configDetails;
	
	private String osType = null;

	public void parse(String path) {
		try{
			String xmlString = readFile(path, Charset.defaultCharset());
			if (path.contains("AppInfo")) {
				configDetails.setAppInfo(Arrays.asList(xmlString));
			} else if (path.contains("SystemInfo")) {
				configDetails.setSystemInfo(Arrays.asList(xmlString));
			}
		}
		catch (Exception e){
			throw new LogReaderException(e, ErrorCode.EXCEPTION);
		}
	}

	public static String readFile(String path, Charset encoding) {
		try{
			byte[] encoded = Files.readAllBytes(Paths.get(path));
			return new String(encoded, encoding);
		}
		catch (IOException e){
			throw new LogReaderException(e, ErrorCode.INPUT_OUTPUT_EXCEPTION);
		}
	}

}

