package com.zscalar.log.reader.utility.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zscalar.log.reader.utility.entities.Settings;

import java.util.Optional;

@Repository
public interface SettingsRepo extends JpaRepository<Settings, Long> {

    boolean existsByUserAndName(String user, String name);

    boolean existsByName(String name);

    Optional<Settings> findByUserAndName(String user, String name);

    Settings findByName(String name);
}
