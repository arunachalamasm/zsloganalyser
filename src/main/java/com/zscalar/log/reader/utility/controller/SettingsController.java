package com.zscalar.log.reader.utility.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.zscalar.log.reader.utility.dto.SettingsDto;
import com.zscalar.log.reader.utility.entities.Settings;
import com.zscalar.log.reader.utility.service.impl.SettingsServiceImpl;

@RequestMapping("/api/v1/settings")
@RestController
public class SettingsController {

    @Autowired
    private SettingsServiceImpl settingsService;

    @PostMapping("/saveSetting")
    public ResponseEntity<String> postMethod(@RequestBody SettingsDto settingsDto) {
        if (settingsService.existsByUserAndName(settingsDto.getUser(), settingsDto.getName())) {
            return ResponseEntity.ok(settingsService.updateSetting(settingsDto));
        } else {
            return ResponseEntity.ok(settingsService.saveSetting(settingsDto));
        }
    }

    @GetMapping("/getSetting")
    public ResponseEntity<Settings> getSetting(@RequestParam String user, String name) {
        return ResponseEntity.ok(settingsService.getSetting(user, name));
    }
}
