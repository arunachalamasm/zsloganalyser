package com.zscalar.log.reader.utility.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.zscalar.log.reader.utility.entities.FieldTokens;

@Repository
public interface FieldTokensRepo extends CrudRepository<FieldTokens,Integer> {
	
}
