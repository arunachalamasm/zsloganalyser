package com.zscalar.log.reader.utility.constants;

public class ZscalarConstants {

    public static final String LEVEL = "level";

    public static  final String TRANSACTION_ID = "transactionID";

    public static final String LOG_DATE = "logDate";

    public static final String MESSAGE = "message";

    public static final String FILE_NAME = "fileName";

    public static  final String OR = "or";

    public static final String AND = "and";

    public static final String FIRST_NAME = "firstName";

    public static final String LAST_NAME = "lastName";

    public static final String EMAIL = "email";

    public static final String ADMIN = "admin";

    public static final String USER = "user";

    public static final String SET_ADMIN = "ADMIN";

    public static final String SET_USER = "USER";

    public static final String No = "no";

    public static final String zeroTo255 = "(\\d{1,2}|(0|1)\\d{2}|2[0-4]\\d|25[0-5])";

    public static final String ipRegex = zeroTo255 + "\\." + zeroTo255 + "\\." + zeroTo255 + "\\." + zeroTo255;

    public static final String domainRegex = ".*?\\.(.*?\\.[a-zA-Z]+)";

    public static final String emailRegex = "^(.+)@(.+)$";

    public static final String OTHERS = "Others";

    public static final String SPACE = " ";

    public static final String OPEN_SQUARE_BRACKET = "[";

    public static final String CLOSE_SQUARE_BRACKET = "]";

    public static final String OPEN_COMMON_BRACKET = "(";

    public static final String CLOSE_COMMON_BRACKET = ")";

    public static final String COLON = ":";

    public static final String EMPTY = "";

    public static final String COMMA = ",";

    public static final String MODULUS = "%";

    public static final String UNDER_SCORE = "_";

    public static final String EQUALS = "=";

    public static final String OTHER_SETTINGS = "OtherSettings";

    public static final String RETAIN = "Retain";

    public static final String ERROR_TOKENS = "ErrorTokens";

    public static final String FIELD = "field";

    public static final String ERROR = "error";

    public static final String ALPHA_REGEX = "%[a-z]";

    public static final String ALL_REGEX = ".*";

    public static final String FILE_NAME_FILTER = "FileNameFilter";

    public static final String FORWARDING_PROFILE = "forwarding profile";
    
    public static final String BATCH_STOP_SUCCESS_MESSAGE = "Job Stopped Successfully";
    
    public static final String BATCH_NO_RUNNING_MESSAGE = "Job is not running currently and cannot be stopped now";




}
