package com.zscalar.log.reader.utility.batch.config;

import org.springframework.batch.core.repository.JobRepository;

public interface BatchConfigurer {

    JobRepository getJobRepository() throws Exception;

}
