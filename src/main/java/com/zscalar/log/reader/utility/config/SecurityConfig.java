package com.zscalar.log.reader.utility.config;

import static org.springframework.security.extensions.saml2.config.SAMLConfigurer.saml;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig {
 @Configuration
        
        @Order(1)
        public static class SamlConfig extends WebSecurityConfigurerAdapter{
        /* Saml configuration including key details and metadata for IDP*/
            @Value("${security.saml2.metadata-url}")
            String metadataUrl;

            @Value("${server.ssl.key-alias}")
            String keyAlias;

            @Value("${server.ssl.key-store-password}")
            String password;

            @Value("${server.port}")
            String port;

            @Value("${server.networkHost}")
            String networkHost;

            @Value("${server.ssl.key-store}")
            String keyStoreFilePath;
            //@Override
            protected void configure(final HttpSecurity http) throws Exception {
                 http
                    .csrf().disable()
                    .cors().disable()
                    .authorizeRequests()
                       	.antMatchers("/saml*").permitAll()
                        .antMatchers("/api/v1/settings/**").permitAll()
                        .antMatchers("/api/v1/auth/saml/SSO/user").permitAll()
                        .antMatchers("/logs/**").permitAll()
                        .anyRequest().authenticated()
                        //.anyRequest().permitAll()
                        .and()
                    .apply(saml())
                        .serviceProvider()
                            .keyStore()
                                .storeFilePath(this.keyStoreFilePath)
                                .password(this.password)
                                .keyname(this.keyAlias)
                                .keyPassword(this.password)
                            .and()
                        .protocol("https")
                        .hostname(String.format("%s:%s", this.networkHost, this.port))
                        .basePath("/")
                        .and()
                    .identityProvider()
                    .metadataFilePath(this.metadataUrl);
            }
        }
}
