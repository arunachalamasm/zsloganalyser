package com.zscalar.log.reader.utility.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@AllArgsConstructor
@NoArgsConstructor
public class SettingsDto {
    private String user;
    private String name;
    private String content;

}
