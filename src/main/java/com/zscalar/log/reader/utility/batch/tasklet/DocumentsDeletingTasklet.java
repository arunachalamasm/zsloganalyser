package com.zscalar.log.reader.utility.batch.tasklet;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.zscalar.log.reader.utility.batch.model.LogsUploadJobSettings;
import com.zscalar.log.reader.utility.constants.ZscalarConstants;
import com.zscalar.log.reader.utility.dto.CustomJobParameter;
import com.zscalar.log.reader.utility.repository.ESRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@JobScope
public class DocumentsDeletingTasklet implements Tasklet {

	@Autowired
	private ESRepository eSRepository;

	@Autowired
	LogsUploadJobSettings logsUploadJobSettings;

	@Value("#{jobParameters[customparam]}")
	public CustomJobParameter source;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		log.debug("Checking if index has to be deleted");

		String configIndex= logsUploadJobSettings.getConfigIndexName();

		if (logsUploadJobSettings.getRetain() != null &&
				logsUploadJobSettings.getRetain().equalsIgnoreCase(ZscalarConstants.No)) {
			log.debug("Deleting index {} from ES",source.getIndexName());
			eSRepository.deleteUserIndex("user_" + source.getIndexName());
			eSRepository.deleteUserIndex(configIndex);
		}
		eSRepository.createNewUserIndex("user_" + source.getIndexName(), getMappingForIndex());
		eSRepository.createNewUserIndex(configIndex, getMappingForIndex());
		return RepeatStatus.FINISHED;
	}

	private Map<String, Object> getMappingForIndex() {
		Map<String, Object> properties = new HashMap<>();
		Map<String, Object> mapping = new HashMap<>();
		mapping.put("properties", properties);
		return mapping;
	}
}
