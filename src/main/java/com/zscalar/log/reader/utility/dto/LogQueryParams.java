package com.zscalar.log.reader.utility.dto;

import java.util.List;

public class LogQueryParams {

    private String logDateStart;
    private String logDateEnd;
    private List<String> transcationID;
    private String message;
    private List<String> level;
    private String fileName;

    public LogQueryParams(String logDateStart, String logDateEnd, List<String> transcationID, String message, List<String> level, String fileName) {
        this.logDateStart = logDateStart;
        this.logDateEnd = logDateEnd;
        this.transcationID = transcationID;
        this.message = message;
        this.level = level;
        this.fileName = fileName;
    }

    public String getLogDateStart() {
        return logDateStart;
    }

    public void setLogDateStart(String logDateStart) {
        this.logDateStart = logDateStart;
    }

    public String getLogDateEnd() {
        return logDateEnd;
    }

    public void setLogDateEnd(String logDateEnd) {
        this.logDateEnd = logDateEnd;
    }

    public List<String> getTranscationID() {
        return transcationID;
    }

    public void setTranscationID(List<String> transcationID) {
        this.transcationID = transcationID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getLevel() {
        return level;
    }

    public void setLevel(List<String> level) {
        this.level = level;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        return "LogQueryParams{" +
                "logDateStart='" + logDateStart + '\'' +
                ", logDateEnd='" + logDateEnd + '\'' +
                ", transcationID='" + transcationID + '\'' +
                ", message='" + message + '\'' +
                ", level='" + level + '\'' +
                ", fileName='" + fileName + '\'' +
                '}';
    }
}
