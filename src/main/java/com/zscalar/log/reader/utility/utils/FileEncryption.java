package com.zscalar.log.reader.utility.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zscalar.log.reader.utility.entities.Settings;
import com.zscalar.log.reader.utility.exception.ErrorCode;
import com.zscalar.log.reader.utility.exception.LogReaderException;
import com.zscalar.log.reader.utility.service.SettingsService;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

@Component
public class FileEncryption {

    @Autowired
    private SettingsService settingsService;

    ObjectMapper mapper = new ObjectMapper();
    Logger logger = LoggerFactory.getLogger(FileEncryption.class);
    String tempDir = System.getProperty("java.io.tmpdir");
    String private_key_file = tempDir + "private-key.pem";
    String encryptedFile = tempDir + "encrypted.zip";

    public String checkIfFileIsEncrypted(MultipartFile file, String urlFileName) throws Exception {
        logger.info("Checking if the uploaded file is encrypted");
        String fileName = (null != urlFileName) ? urlFileName : file.getOriginalFilename();
        String extension = FilenameUtils.getExtension(fileName);
        if (!extension.equalsIgnoreCase("zip")) {
            logger.info("Decrypting file");
            createPrivateKey(extension);
            downloadMultipartFile(file);
            File uploadFile = ResourceUtils.getFile("classpath:upload.php");
            String decryptedFile = tempDir + FilenameUtils.removeExtension(fileName);
            Process process = Runtime.getRuntime().exec("php " + uploadFile + " " + encryptedFile + " " + private_key_file + " " + decryptedFile);
            Files.deleteIfExists(Paths.get(encryptedFile));
            Files.deleteIfExists(Paths.get(private_key_file));
            String errorString;
            BufferedReader stdErr = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            while ((errorString = stdErr.readLine()) != null)
                throw new Exception(errorString);
            return decryptedFile;
        } else {
            return null;
        }
    }

    private void createPrivateKey(String extension) throws JsonProcessingException {
        logger.debug("Creating private key by fetching details from settings");
        Settings decryptSetting = settingsService.getSetting("DecryptKey");
        String decryptSettingContent = decryptSetting.getContent();
        List<HashMap<String, String>> list = mapper.readValue(decryptSettingContent, new TypeReference<List<HashMap<String, String>>>() {
        });
        list.forEach(o -> {
            if (o.containsKey(extension)) {
                String fileContent = o.get(extension);
                try (PrintWriter out = new PrintWriter(private_key_file)) {
                    out.println(fileContent);
                } catch (FileNotFoundException e) {
                    throw new LogReaderException(e, ErrorCode.FILE_NOT_FOUND);
                }
            }
        });
    }

    private void downloadMultipartFile(MultipartFile file) throws IOException {
        logger.debug("Copying uploaded file to temp location");
        File targetFile = new File(encryptedFile);
        InputStream initialStream = file.getInputStream();
        byte[] buffer = new byte[initialStream.available()];
        initialStream.read(buffer);
        try (OutputStream outStream = new FileOutputStream(targetFile)) {
            outStream.write(buffer);
        } catch (Exception e) {
            logger.error("Not able to copy uploaded file to temp location");
            throw new LogReaderException(e, ErrorCode.EXCEPTION);
        }
    }
}
