package com.zscalar.log.reader.utility.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class SSOUser {
    String firstName;
    String lastName;
    String email;
    String group;

}
