package com.zscalar.log.reader.utility.service;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

import com.zscalar.log.reader.utility.dto.response.BatchJobStatusResponse;
import com.zscalar.log.reader.utility.dto.response.DateRangeResponse;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.web.multipart.MultipartFile;

import com.zscalar.log.reader.utility.dto.response.ZipUploadResponse;

public interface LogParserService {

	public ZipUploadResponse parseLog(MultipartFile file, String urlFileName, String userName, String Obfuscate, Date startDate,Date endDate) throws Exception;

	public DateRangeResponse getDateRange(MultipartFile file) throws IOException;

	public AtomicReference<String> stopBatch(String userName);

	public BatchJobStatusResponse getJobStatus(String userName) throws NoSuchJobException;
}

