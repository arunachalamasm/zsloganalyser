package com.zscalar.log.reader.utility.dto.request;

public class AuthRequest {
    //required username and password to login
    private String USERNAME;
    private String PASSWORD;

    //constructor

    public AuthRequest() {
    }

    public AuthRequest(String USERNAME, String PASSWORD) {
        this.USERNAME = USERNAME;
        this.PASSWORD = PASSWORD;
    }

    //getter and setter method

    public String getUSERNAME() {
        return USERNAME;
    }

    public void setUSERNAME(String USERNAME) {
        this.USERNAME = USERNAME;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public void setPASSWORD(String PASSWORD) {
        this.PASSWORD = PASSWORD;
    }
}
