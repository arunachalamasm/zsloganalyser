package com.zscalar.log.reader.utility.exception;

import org.springframework.http.HttpStatus;

public enum ErrorCode {
    JOB_NOT_FOUND("Job not found", HttpStatus.NOT_FOUND),
    JOB_NOT_RUNNING("Job not running", HttpStatus.NOT_FOUND),
    JOB_EXCEPTION("Job exception", HttpStatus.EXPECTATION_FAILED),
    FILE_NOT_FOUND("File not found", HttpStatus.NOT_FOUND),
    INPUT_OUTPUT_EXCEPTION("Input Output Exception", HttpStatus.BAD_REQUEST),
    EMPTY_INPUT("Input should not be Empty", HttpStatus.BAD_REQUEST),
    IN_VALID_INPUT("Input was not valid or Mandatory fileds should not be empty", HttpStatus.BAD_REQUEST),
    ERROR_WHILE_RETRIEVING_DATA("Error while retrieving data, please try again..!", HttpStatus.INTERNAL_SERVER_ERROR),
    NO_DATA_FOUND("No data found", HttpStatus.NOT_FOUND),
    EXCEPTION("Unidentified Exception occurred", HttpStatus.NOT_FOUND),
    ;

    private final String message;
    private final HttpStatus httpStatus;

    ErrorCode(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

}
