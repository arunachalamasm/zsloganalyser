package com.zscalar.log.reader.utility.dto;

import java.util.UUID;

import org.springframework.batch.core.JobParameter;

public class CustomJobParameters<CustomJobParameter> extends JobParameter {

	private CustomJobParameter customParam;

	public CustomJobParameters(CustomJobParameter customParam) {
		// This is to avoid duplicate JobInstance error
		super(UUID.randomUUID().toString());
		this.customParam = customParam;
	}

	public CustomJobParameter getValue() {
		return customParam;
	}
}