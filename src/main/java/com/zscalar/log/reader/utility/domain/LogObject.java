package com.zscalar.log.reader.utility.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Setter
@Getter
@Component
public class LogObject {

	private LogDetail logDetail;
	
	private String ofsLine;
	
}
