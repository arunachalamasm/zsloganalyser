package com.zscalar.log.reader.utility.service;

import java.io.IOException;

import com.zscalar.log.reader.utility.dto.LogQueryParams;
import com.zscalar.log.reader.utility.dto.QueryRequestWithPagination;
import com.zscalar.log.reader.utility.dto.response.LogQueryResponse;

public interface DataProcessService {

    public LogQueryResponse getLogs(LogQueryParams queryParams, QueryRequestWithPagination request, String operator, String userName, String zipFileName) throws IOException;
}
