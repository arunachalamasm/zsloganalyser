package com.zscalar.log.reader.utility.dto.response;

import lombok.Data;

@Data
public class BatchJobStatusResponse {

    private Boolean running;
    private String zipFileName;
}
