package com.zscalar.log.reader.utility.dto.response;

public class JwtResponse {

    //sending jwt token as response
    private String JWT_RESPONSE;

    // constructor

    public JwtResponse(String JWT_RESPONSE) {
        this.JWT_RESPONSE = JWT_RESPONSE;
    }

    //getter and setter method

    public String getJWT_RESPONSE() {
        return JWT_RESPONSE;
    }

    public void setJWT_RESPONSE(String JWT_RESPONSE) {
        this.JWT_RESPONSE = JWT_RESPONSE;
    }
}
