package com.zscalar.log.reader.utility.dto.response;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component
public class LogQueryResponse extends Response{

    private Long searchTimeMillis;
    private Long currentServerTimeMilliseconds;
    private Boolean hasMore;

    public LogQueryResponse() {
        this(0, 0, 0, 0, false, Collections.emptyList());
    }

    private LogQueryResponse(final long searchTimeMillis, final long serverTimeMillis, final long totalCount, final int resultCount, final boolean hasMore, final List<Map<String, ?>> results) {
        setSearchTimeMillis(searchTimeMillis);
        setCurrentServerTimeMilliseconds(serverTimeMillis);
        setTotalCount(totalCount);
        setResultCount(resultCount);
        setHasMore(hasMore);
        setResults(results);
    }

    public static LogQueryResponse newInstanceFrom(final SearchResponse searchResponse, final int offset, final long serverTimeMillis) {
        final long searchTimeMillis = getSearchTimeMillis(searchResponse);
        final long totalCount = getTotalCount(searchResponse);
        final int resultCount = getResultCount(searchResponse);
        final boolean hasMore = getHasMore(searchResponse, offset);
        final List<Map<String, ?>> results = getResults(searchResponse);

        return new LogQueryResponse(searchTimeMillis, serverTimeMillis, totalCount, resultCount, hasMore, results);
    }

    public static LogQueryResponse newInstanceWithNoResults(final long serverTimeMillis) {
        final List<Map<String, ?>> emptyListOfResults = Collections.emptyList();
        return new LogQueryResponse(0, serverTimeMillis, 0, 0, false, emptyListOfResults);
    }

    public static LogQueryResponse newInstanceFromResultsList(List<Map<String, ?>> results, Long searchTimeMillis, long totalCount, boolean hasMore) {
        return new LogQueryResponse(searchTimeMillis, System.currentTimeMillis(), totalCount, results.size(), hasMore, results);
    }

    private static List<Map<String, ?>> getResults(final SearchResponse searchResponse) {
        final List<Map<String, ?>> results = new ArrayList<>();

        for (final SearchHit searchHit : searchResponse.getHits().getHits()) {
            results.add(searchHit.getSourceAsMap());
        }

        return results;
    }

    private static boolean getHasMore(final SearchResponse searchResponse, final int offset) {
        return (offset + getResultCount(searchResponse)) < getTotalCount(searchResponse);
    }

    private static int getResultCount(final SearchResponse searchResponse) {
        return searchResponse.getHits().getHits().length;
    }

    private static long getTotalCount(final SearchResponse searchResponse) {
        return searchResponse.getHits().getTotalHits().value;
    }

    private static long getSearchTimeMillis(final SearchResponse searchResponse) {
        return searchResponse.getTook().getMillis();
    }

    public long getSearchTimeMillis() {
        return searchTimeMillis;
    }

    public long getCurrentServerTimeMilliseconds() {
        return currentServerTimeMilliseconds;
    }
    private void setCurrentServerTimeMilliseconds(final long serverTimeMillis) {
        this.currentServerTimeMilliseconds = serverTimeMillis;
    }

    private void setSearchTimeMillis(final long searchTimeMillis) {
        this.searchTimeMillis = searchTimeMillis;
    }

    private void setHasMore(final boolean hasMore) {
        this.hasMore = hasMore;
    }
    public boolean getHasMore() {
        return hasMore;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("results", results)
                .append("totalCount", totalCount)
                .append("resultCount", resultCount)
                .toString();
    }
}
