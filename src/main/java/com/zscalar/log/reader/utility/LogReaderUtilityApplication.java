package com.zscalar.log.reader.utility;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class LogReaderUtilityApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(LogReaderUtilityApplication.class, args);
	}

}
