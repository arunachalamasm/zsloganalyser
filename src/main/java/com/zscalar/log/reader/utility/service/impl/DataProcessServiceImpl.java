package com.zscalar.log.reader.utility.service.impl;

import java.io.IOException;

import com.zscalar.log.reader.utility.exception.ErrorCode;
import com.zscalar.log.reader.utility.exception.LogReaderException;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zscalar.log.reader.utility.config.ElasticsearchConfig;
import com.zscalar.log.reader.utility.domain.LogDetail;
import com.zscalar.log.reader.utility.dto.LogQueryParams;
import com.zscalar.log.reader.utility.dto.QueryRequestWithPagination;
import com.zscalar.log.reader.utility.dto.response.LogQueryResponse;
import com.zscalar.log.reader.utility.repository.ESRepository;
import com.zscalar.log.reader.utility.search.LogQuerySearchHelper;
import com.zscalar.log.reader.utility.service.DataProcessService;

@Service
public class DataProcessServiceImpl implements DataProcessService {

    @Autowired
    private ElasticsearchConfig elasticSearchClient;

    @Autowired
    private RestHighLevelClient client;

    @Autowired
    private LogQueryResponse logQueryResponse;

    @Autowired
    private ESRepository eSRepository;

    @Override
    public LogQueryResponse getLogs(LogQueryParams queryParams, QueryRequestWithPagination request, String operator, String userName, String zipFileName) {
        try{
            String indexName;
            if (StringUtils.isEmpty(zipFileName))
                indexName = userName;
            else
                indexName = "config_" + userName + "_" + zipFileName;
            SearchRequest searchRequest = LogQuerySearchHelper.buildLogSearchRequest(queryParams, request, operator, indexName);
            return eSRepository.getLogs(request, searchRequest);
        }
        catch (IOException e){
            throw new LogReaderException(e, ErrorCode.INPUT_OUTPUT_EXCEPTION);
        }
    }
}
