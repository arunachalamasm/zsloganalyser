package com.zscalar.log.reader.utility.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class FieldTokens {
    
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;
    
    private String tokens;
}