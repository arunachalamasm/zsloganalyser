package com.zscalar.log.reader.utility.controller;

import com.zscalar.log.reader.utility.dto.response.BatchJobStatusResponse;
import com.zscalar.log.reader.utility.dto.response.DateRangeResponse;
import com.zscalar.log.reader.utility.dto.response.ZipUploadResponse;
import com.zscalar.log.reader.utility.exception.ErrorCode;
import com.zscalar.log.reader.utility.exception.LogReaderException;
import com.zscalar.log.reader.utility.service.LogParserService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

// API's for getting uploaded log files, unzipping them, extract all the necessary fields and push them to ES
@RestController
public class LogAnalyserController {

    @Autowired
    private LogParserService logParserService;

    Logger logger = LoggerFactory.getLogger(LogAnalyserController.class);

    @PostMapping(value = "/logs/upload")
    public ResponseEntity<ZipUploadResponse> uploadLogFiles(
            @RequestPart(value = "file", required = false) MultipartFile file,
            @RequestPart(value = "awsUrl", required = false) String awsUrl,
            @RequestPart(value = "flag", required = false) String obfuscate,
            @RequestPart(value = "userName") String userName,
            @RequestPart(value = "startDate", required = false) String startDate,
            @RequestPart(value = "endDate", required = false) String endDate) {
        try {
            ZipUploadResponse response = null;
            Date start = null, end = null;
            SimpleDateFormat formatter = new SimpleDateFormat("MMM dd yyyy");

            if (!StringUtils.isEmpty(startDate)) start = formatter.parse(startDate);
            if (!StringUtils.isEmpty(endDate)) end = formatter.parse(endDate);

            if (!StringUtils.isEmpty(awsUrl)) {
                String urlFileName = FilenameUtils.getName(new URL(awsUrl).getPath());
                BufferedInputStream inputStream = new BufferedInputStream(new URL(awsUrl).openStream());
                MultipartFile awsMultipartFile = new MockMultipartFile("test.zip", inputStream);
                response = logParserService.parseLog(awsMultipartFile, urlFileName, userName.toLowerCase(), obfuscate, start, end);
            } else {
                response = logParserService.parseLog(file, null, userName.toLowerCase(), obfuscate, start, end);
            }
            return new ResponseEntity<ZipUploadResponse>(response, HttpStatus.OK);
        } catch (ParseException | MalformedURLException e) {
            throw new LogReaderException(e, ErrorCode.ERROR_WHILE_RETRIEVING_DATA);
        } catch (IOException e) {
            throw new LogReaderException(e, ErrorCode.INPUT_OUTPUT_EXCEPTION);
        } catch (Exception e) {
            throw new LogReaderException(e, ErrorCode.EXCEPTION);
        }

    }

    @GetMapping(value = "/logs/getDateRange")
    public ResponseEntity<DateRangeResponse> getDateRange(
            @RequestPart(value = "file", required = true) MultipartFile file) {
        try {
            DateRangeResponse dateRangeResponse = null;
            dateRangeResponse = logParserService.getDateRange(file);
            return new ResponseEntity<DateRangeResponse>(dateRangeResponse, HttpStatus.OK);
        } catch (IOException e) {
            throw new LogReaderException(e, ErrorCode.INPUT_OUTPUT_EXCEPTION);
        }
    }

    @GetMapping(value = "/logs/stopBatch")
    public AtomicReference<String> stopBatch(@Valid String userName) {
        logger.info("Checking if user:{} job is running ", userName);
        return logParserService.stopBatch(userName);
    }

    @GetMapping(value = "logs/jobStatus")
    public ResponseEntity<BatchJobStatusResponse> getJobStatus(@Valid String userName) {
        logger.info("Checking if user:{} job is running ", userName);
        try {
            BatchJobStatusResponse batchJobStatusResponse = null;
            batchJobStatusResponse = logParserService.getJobStatus(userName);
            if (batchJobStatusResponse.getRunning() == null)
                batchJobStatusResponse.setRunning(Boolean.FALSE);
            return new ResponseEntity<BatchJobStatusResponse>(batchJobStatusResponse, HttpStatus.OK);
        } catch (NoSuchJobException e) {
            throw new LogReaderException(e, ErrorCode.JOB_NOT_FOUND);
        }
    }
}
