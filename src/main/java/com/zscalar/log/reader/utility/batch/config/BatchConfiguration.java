package com.zscalar.log.reader.utility.batch.config;

import java.net.MalformedURLException;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.PassThroughLineMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.sun.el.parser.ParseException;
import com.zscalar.log.reader.utility.batch.listener.LogsUploadJobListener;
import com.zscalar.log.reader.utility.batch.partitioner.MultiFileResourcePartitioner;
import com.zscalar.log.reader.utility.batch.processor.LogDetailItemProcessor;
import com.zscalar.log.reader.utility.batch.reader.LogDetailItemReader;
import com.zscalar.log.reader.utility.batch.tasklet.DocumentsDeletingTasklet;
import com.zscalar.log.reader.utility.batch.writer.LogDetailItemWriter;
import com.zscalar.log.reader.utility.domain.LogObject;


@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	private DocumentsDeletingTasklet documentsDeletingTasklet;
	
	@Autowired
	private LogsUploadJobListener logsUploadJobListener;

	@Autowired
	private JobLauncher jobLauncher;
	
	@Value("${spring.batch.job.chunksize}")
	private int chunkSize;
	
	@Bean
	@StepScope
	public LogDetailItemReader reader() {
		FlatFileItemReader<String> reader = new FlatFileItemReader<String>();
		reader.setLineMapper(new PassThroughLineMapper());
		return new LogDetailItemReader(reader);

	}
	@Bean
	@StepScope
	public LogDetailItemProcessor processor() {
		return new LogDetailItemProcessor();
	}

	@Bean
	@StepScope
	public LogDetailItemWriter writer() {
		return new LogDetailItemWriter();
	}

	@Bean("logsUploadJob")
	public Job logsUploadJob() throws UnexpectedInputException, MalformedURLException, ParseException {
		return jobBuilderFactory.get("logsUploadJob")
				.listener(logsUploadJobListener)
				.start(documentsDeleteStep()).next(partitionStep())
				.build();
	}
	
	@Bean
	public Step documentsDeleteStep() {
		return stepBuilderFactory.get("documentsDeleteStep")
				.tasklet(documentsDeletingTasklet)
				.build();
	}

	@Bean
	public Step partitionStep() throws UnexpectedInputException, MalformedURLException, ParseException {
		return stepBuilderFactory.get("partitionStep")
				.partitioner("logsUploadSlaveStep", partitioner())
				.step(logsUploadSlaveStep())
				.taskExecutor(taskExecutor())
				.build();
	}

	@Bean
	public Step logsUploadSlaveStep() {
		return stepBuilderFactory.get("logsUploadSlaveStep")
				.<String, LogObject>chunk(chunkSize)
				.reader(reader())
				.processor(processor())
				.writer(writer())
				.build();
	}

	@Bean
	@StepScope
	public MultiFileResourcePartitioner partitioner() {
		MultiFileResourcePartitioner partitioner = new MultiFileResourcePartitioner();
		return partitioner;
	}

	@Bean
	@StepScope
	public TaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		return taskExecutor;
	}

	@Bean
	public SimpleJobOperator jobOperator(JobExplorer jobExplorer,
										 JobRepository jobRepository,
										 JobRegistry jobRegistry) {

		SimpleJobOperator jobOperator = new SimpleJobOperator();
		jobOperator.setJobExplorer(jobExplorer);
		jobOperator.setJobRepository(jobRepository);
		jobOperator.setJobRegistry(jobRegistry);
		jobOperator.setJobLauncher(jobLauncher);
		return jobOperator;
	}
}
