package com.zscalar.log.reader.utility.exception;

import com.zscalar.log.reader.utility.dto.response.ZipUploadResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Global Exception Handler class
 *
 * @author Savyasachi
 */

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<ZipUploadResponse> handleException(Exception e) {
        ZipUploadResponse response = new ZipUploadResponse();
        response.setError(true);
        response.setMessage(e.getCause().toString());
        response.setStatus(HttpStatus.EXPECTATION_FAILED.value());
        return new ResponseEntity(response, HttpStatus.EXPECTATION_FAILED);

    }

    /**
     * Business Exception handling.
     *
     * @param exception Exception
     * @param request   Request
     * @return ResponseEntity
     */
    @ExceptionHandler(LogReaderException.class)
    @ResponseBody
    public ResponseEntity<Object> handleBusinessException(LogReaderException exception, WebRequest request) {
        LOGGER.error("Exception ", exception);
        return new ResponseEntity<>(exception.getErrorDetail(), exception.getHttpStatus());
    }


    @ExceptionHandler(NoSuchJobException.class)
    @ResponseBody
    public ResponseEntity<ZipUploadResponse> handleJobException(Exception e) {
        ZipUploadResponse response = new ZipUploadResponse();
        response.setError(true);
        response.setMessage(e.getCause().toString());
        response.setStatus(HttpStatus.NOT_FOUND.value());
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);

    }

}
