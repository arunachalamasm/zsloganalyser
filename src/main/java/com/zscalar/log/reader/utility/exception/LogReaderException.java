package com.zscalar.log.reader.utility.exception;

public class LogReaderException extends BusinessException {

    private static final long serialVersionUID = -6177939907218052963L;

    public LogReaderException(ErrorCode errorCode) {
        super(errorCode.getHttpStatus(), errorCode.name(), errorCode.getMessage());
    }

    public LogReaderException(ErrorCode errorCode, String customMessage) {
        super(errorCode.getHttpStatus(), errorCode.name(), errorCode.getMessage(), customMessage);
    }

    public LogReaderException(Throwable cause, ErrorCode errorCode) {
        super(cause, errorCode.getHttpStatus(), errorCode.name(), errorCode.getMessage());
    }

}
