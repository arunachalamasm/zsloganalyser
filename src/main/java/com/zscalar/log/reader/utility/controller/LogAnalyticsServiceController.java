package com.zscalar.log.reader.utility.controller;

import com.zscalar.log.reader.utility.constants.ResponseMessage;
import com.zscalar.log.reader.utility.dto.LogQueryParams;
import com.zscalar.log.reader.utility.dto.QueryRequestWithPagination;
import com.zscalar.log.reader.utility.dto.response.LogQueryResponse;
import com.zscalar.log.reader.utility.exception.ErrorCode;
import com.zscalar.log.reader.utility.exception.LogReaderException;
import com.zscalar.log.reader.utility.service.DataProcessService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

/*
   API's for pulling Analytical data using Data Processing Agents.
 */
@RestController
@RequestMapping("/logs")
@Api(value = "logs", description = "Operation pertaining to Displaying and Searching Logs")
public class LogAnalyticsServiceController {

    @Autowired
    private DataProcessService dataProcessService;

    @RequestMapping(method = RequestMethod.GET, value = "/search")
    @ResponseBody
    @ApiOperation(value = "Logs", notes = "Get Logs")
    @ApiResponses({@ApiResponse(code = 500, message = ResponseMessage.RESPONSE_500)})


    public LogQueryResponse getLogs(@Valid @ModelAttribute LogQueryParams queryParams, @Valid @ModelAttribute QueryRequestWithPagination request,
                                    @Valid String operator, @Valid String userName, @Valid String zipFileName) throws IOException {
        try {
            return dataProcessService.getLogs(queryParams, request, operator, userName.toLowerCase(), zipFileName.toLowerCase());
        } catch (IOException e) {
            throw new LogReaderException(e, ErrorCode.INPUT_OUTPUT_EXCEPTION);
        }
    }
}
