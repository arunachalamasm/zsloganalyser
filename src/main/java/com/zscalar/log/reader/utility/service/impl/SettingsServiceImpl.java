package com.zscalar.log.reader.utility.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zscalar.log.reader.utility.dto.SettingsDto;
import com.zscalar.log.reader.utility.entities.Settings;
import com.zscalar.log.reader.utility.repository.SettingsRepo;
import com.zscalar.log.reader.utility.service.SettingsService;

import java.time.LocalDateTime;
import java.util.List;


@Service
public class SettingsServiceImpl implements SettingsService {
    @Autowired
    private SettingsRepo settingsRepo;

    @Override
    public boolean existsByUserAndName(String user, String name) {
       return settingsRepo.existsByUserAndName(user, name);
    }

    @Override
    public String saveSetting(SettingsDto settingsDto) {
        Settings settings = new Settings();
        settings.setName(settingsDto.getName());
        settings.setUser(settingsDto.getUser());
        settings.setContent(settingsDto.getContent());
        settingsRepo.save(settings);
        return "Settings Saved";
    }


    @Override
    public Settings getSetting(String user, String name) {
    	if(settingsRepo.existsByUserAndName(user, name)){
    		return settingsRepo.findByUserAndName(user, name).get();
    	} else {
    		return new Settings();
    	}
        
    }

    @Override
    public String updateSetting(SettingsDto settingsDto) {
        if (settingsRepo.existsByUserAndName(settingsDto.getUser(), settingsDto.getName())) {
            Settings settings = settingsRepo.findByUserAndName(settingsDto.getUser(), settingsDto.getName()).get();
            settings.setUser(settings.getUser());
            settings.setContent(settingsDto.getContent());
            settings.setName(settingsDto.getName());
            LocalDateTime now = LocalDateTime.now();
            settings.setUpdated(now);
            settingsRepo.save(settings);
        }
        return "Settings Saved";
    }

    @Override
    public List<Settings> getAllSetting() {
        return settingsRepo.findAll();
    }

    @Override
    public Settings getSetting(String name) {
        if (settingsRepo.existsByName(name)) {
            return settingsRepo.findByName(name);
        } else {
            return null;
        }
    }
}
