package com.zscalar.log.reader.utility.batch.processor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.zscalar.log.reader.utility.batch.model.LogsUploadJobSettings;
import com.zscalar.log.reader.utility.constants.ZscalarConstants;
import com.zscalar.log.reader.utility.domain.ErrorToken;
import com.zscalar.log.reader.utility.domain.LogDetail;
import com.zscalar.log.reader.utility.domain.LogObject;
import com.zscalar.log.reader.utility.dto.CustomJobParameter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@StepScope
public class LogDetailItemProcessor implements ItemProcessor<String, LogObject> {

	@Value("#{jobParameters[customparam]}")
    public CustomJobParameter source;
	
	@Autowired
    private LogsUploadJobSettings logsUploadJobSettings;

	@Override
	public LogObject process(String logString) throws Exception {
		log.debug("In Processor: Processing logline : {}", logString);
		Pattern DATE_PATTERN = Pattern.compile(
				"^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}.\\d{6}.*");
		try {
			int firstSpaceIndex = StringUtils.ordinalIndexOf(logString, " ", 1);
			String dateString = logString.substring(firstSpaceIndex + 1, logString.indexOf(ZscalarConstants.OPEN_COMMON_BRACKET));
			if (DATE_PATTERN.matcher(dateString).matches()) {
				LogObject logObject = new LogObject();
				LogDetail logDetail = new LogDetail();
				int secondSpaceIndex = StringUtils.ordinalIndexOf(logString, ZscalarConstants.SPACE, 2);
				int thirdSpaceIndex = StringUtils.ordinalIndexOf(logString, ZscalarConstants.SPACE, 3);
				int fourthSpaceIndex = StringUtils.ordinalIndexOf(logString, ZscalarConstants.SPACE, 4);

				String fileName = logString.substring(0, firstSpaceIndex);
				logDetail.setFileName(fileName);

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				try {
					Date date = formatter.parse(dateString);
					logDetail.setDateTime(date);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				String processAndThreadIds = logString.substring(logString.indexOf(ZscalarConstants.OPEN_SQUARE_BRACKET), thirdSpaceIndex);
				logDetail.setProcessId(
						processAndThreadIds.substring(processAndThreadIds.indexOf(ZscalarConstants.OPEN_SQUARE_BRACKET) + 1, processAndThreadIds.indexOf(ZscalarConstants.COLON)));
				logDetail.setThreadId(
						processAndThreadIds.substring(processAndThreadIds.indexOf(ZscalarConstants.COLON) + 1, processAndThreadIds.indexOf(ZscalarConstants.CLOSE_SQUARE_BRACKET)));

				logDetail.setLogLevel(logString.substring(thirdSpaceIndex + 1, fourthSpaceIndex));
				String line = logString.substring(firstSpaceIndex, logString.length());
				logDetail.setLogLine(line);

				if (logsUploadJobSettings.getFilterTokenList() != null) {
					logDetail.setMessageData(createMessageData(logString));
				}

				ErrorToken errorToken = null;
				if (logsUploadJobSettings.getErrorTokenMap() != null) {
					errorToken = findIfLogLineHasErrorToken(logString);
				}

				if (source.getObsFlag().equalsIgnoreCase("true"))
					logObject.setOfsLine(fileName + ZscalarConstants.SPACE + obsFiles(line));

				if (errorToken != null) {
					logDetail.setHasError(Boolean.TRUE);
					logDetail.setErrorTokens(errorToken);
				} else {
					logDetail.setHasError(Boolean.FALSE);
				}

				logDetail.setZipFileName(source.getZipFileName());

				if (logsUploadJobSettings.getNameFilterMap() != null) {
					logDetail.setLogFileType(findLogFileType(fileName));
				}
				logObject.setLogDetail(logDetail);
				return logObject;
			} else {
				log.error("Pattern matcher error in logline", logString);
				return null;
			}
		} catch (Exception e) {
			log.error("Error in log line - " + logString);
			return null;
		}
	}

    private ErrorToken findIfLogLineHasErrorToken(String logString) {
    	log.debug("Checking if the logline has error token");
        ErrorToken errorToken = null;
        String errorTokenString = null;
        Map<String, ErrorToken> errorValuesMap = logsUploadJobSettings.getErrorTokenMap();
        Set<String> keyset = errorValuesMap.keySet();
        try {
            for (String errorString : keyset) {
				errorTokenString = errorString;
                Pattern pattern = Pattern.compile(errorString);
                Matcher matcher = pattern.matcher(logString);
                while (matcher.find()) {
                    errorToken = new ErrorToken();
                    errorToken = errorValuesMap.get(errorString);
                    log.debug("Found error token {} in the line.",errorToken);
                    return errorToken;
                }
            }
        }catch (PatternSyntaxException exception){
        	errorValuesMap.remove(errorTokenString);
			logsUploadJobSettings.setErrorTokenMap(errorValuesMap);
           log.error("Please check the Syntax of the errortokens which is added. Exception is: {}",exception.getMessage());
        }
        return null;
    }

    private Map<String, String> createMessageData(String logString) {
    	log.debug("Checking for the filter tokens in the input logline");
        Map<String, String> messageData = new HashMap();
        List<ErrorToken> filterTokenList = logsUploadJobSettings.getFilterTokenList();
        filterTokenList.parallelStream().forEach(filterToken -> {
            int filterData = filterToken.getToken().indexOf(ZscalarConstants.MODULUS);
            String filterString = filterToken.getToken().substring(0, filterData);
            int logData = logString.indexOf(filterString);
            if (logData != -1) {
                String data = logString.substring(logData);
                String values[] = data.split(filterString);
                if (!values[0].isEmpty()) {
                    int spaceIndex = StringUtils.ordinalIndexOf(values[0], ZscalarConstants.SPACE, 1);
                    if (spaceIndex != -1) {
                        messageData.put(replaceDelimiters(filterString), values[0].substring(0, spaceIndex).replace(ZscalarConstants.COMMA,ZscalarConstants.EMPTY));
                    } else {
                        messageData.put(replaceDelimiters(filterString), values[0].replace(ZscalarConstants.COMMA,ZscalarConstants.EMPTY));
                    }
                } else {
                    int spaceIndex = StringUtils.ordinalIndexOf(values[1], ZscalarConstants.SPACE, 1);
                    if (spaceIndex != -1) {
                        messageData.put(replaceDelimiters(filterString), values[1].substring(0, spaceIndex).replace(ZscalarConstants.COMMA,ZscalarConstants.EMPTY));
                    } else {
                        messageData.put(replaceDelimiters(filterString), values[1].replace(ZscalarConstants.COMMA,ZscalarConstants.EMPTY));
                    }
                }
            }
        });
        log.debug("Found filter token {} in the line.", messageData);
        return messageData;
    }

    private String replaceDelimiters(String key) {
        return key.trim().replace(ZscalarConstants.SPACE, ZscalarConstants.UNDER_SCORE).replace(ZscalarConstants.COLON, ZscalarConstants.EMPTY).replace(ZscalarConstants.EQUALS, ZscalarConstants.EMPTY);
    }

    private List<String> findLogFileType(String fileNameString) {
    	log.debug("Finding the file type of the input file");
    	String fileName = fileNameString.split(ZscalarConstants.UNDER_SCORE)[0];
    	List<String> fileTypes = new ArrayList<>();
        Map<String, String> fileNameMap = logsUploadJobSettings.getNameFilterMap();
        for (Map.Entry<String, String> name : fileNameMap.entrySet()) {
            String[] nameValueName = name.getValue().split(ZscalarConstants.COMMA);
            List value = Arrays.asList(nameValueName);
            Iterator<String> iter = value.iterator();
            while (iter.hasNext()) {
                String s = iter.next();
                if (fileName.equalsIgnoreCase(s.trim())) {
                	fileTypes.add(name.getKey().trim());
                }
            }
        }
        if (fileTypes.isEmpty()) {
        	fileTypes.add(ZscalarConstants.OTHERS);
        }
		log.debug("File type of the input file is {}",fileTypes);
        return fileTypes;
    }
    
	private String obsFiles(String logString) {
        Matcher ipMatcher = Pattern.compile(ZscalarConstants.ipRegex).matcher(logString);
      	Matcher domainMatcher = Pattern.compile(ZscalarConstants.domainRegex).matcher( logString );
      	Matcher emailMatcher = Pattern.compile(ZscalarConstants.emailRegex).matcher( logString );
      	if(ipMatcher.find() || domainMatcher.find() || emailMatcher.find()) {
      		return obfisticate(logString);
      	} else {
      		return logString +"\n";
      	}
	}

	private String obfisticate(String logLine) {
		try {
			ProcessBuilder pb = new ProcessBuilder("perl", "/home/ubuntu/Downloads/ZsLogAnalyserObfs/fedred_m.pl", logLine);
			Process p = pb.start();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			StringBuilder builder = new StringBuilder();
			String lineIn = null;
			while ((lineIn = reader.readLine()) != null) {
				builder.append(lineIn);
				builder.append(System.getProperty("line.separator"));
			}
			return builder.toString();
		} catch (Exception e) {
			log.error("Error while obfisticating the line " + logLine);
		}
		return null;
	}
}
