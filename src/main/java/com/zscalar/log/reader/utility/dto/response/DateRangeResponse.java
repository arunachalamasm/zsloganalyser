package com.zscalar.log.reader.utility.dto.response;

import lombok.Data;

import java.util.Date;

@Data
public class DateRangeResponse {

    private String startDate;
    private String endDate;

}
