package com.zscalar.log.reader.utility.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ErrorToken {

    @JsonProperty("Platform")
    public String Platform;
    @JsonProperty("Module")
    public String Module;
    @JsonProperty("Submodule1")
    public String Submodule1;
    @JsonProperty("Submodule2")
    public String Submodule2;
    @JsonProperty("Submodule3")
    public String Submodule3;
    @JsonProperty("Submodule4")
    public String Submodule4;
    @JsonProperty("Token")
    public String Token;
    @JsonProperty("Explanation")
    public String Explanation;
    @JsonProperty("Type")
    public String type;
}
