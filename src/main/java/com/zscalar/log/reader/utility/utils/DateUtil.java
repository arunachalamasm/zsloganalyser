package com.zscalar.log.reader.utility.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateUtil {

    public static boolean isDate(String date) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(date.trim());

        } catch (ParseException e) {
            return false;
        }
        return true;

    }

}
