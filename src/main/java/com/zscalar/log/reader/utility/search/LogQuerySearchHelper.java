package com.zscalar.log.reader.utility.search;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.zscalar.log.reader.utility.annotation.Document;
import com.zscalar.log.reader.utility.constants.ZscalarConstants;
import com.zscalar.log.reader.utility.dto.LogQueryParams;
import com.zscalar.log.reader.utility.dto.QueryRequestWithPagination;

@Component
public class LogQuerySearchHelper {

    private static Document document;

    public LogQuerySearchHelper() {
    }

    private static QueryBuilder buildLogSearchQuery(LogQueryParams queryParams, String operator) {
        QueryBuilder q = null;
        if(StringUtils.isEmpty(queryParams.getFileName()) && queryParams.getLevel() == null && StringUtils.isEmpty(queryParams.getLogDateEnd()) && StringUtils.isEmpty(queryParams.getLogDateStart()) && queryParams.getTranscationID() == null && StringUtils.isEmpty(queryParams.getMessage())  ){
            q= QueryBuilders.matchAllQuery();
            return q;
        }
        else if(!StringUtils.isEmpty(operator) && operator.equalsIgnoreCase(ZscalarConstants.OR)){
            return buildLogSearchQueryOr(queryParams);
        }
        else {
             return buildLogSearchQueryAnd(queryParams);
        }
    }

    private static QueryBuilder buildLogSearchQueryOr(LogQueryParams queryParams) {
        BoolQueryBuilder orBoolQueryBuilder = QueryBuilders.boolQuery();
        if(!queryParamValidation(queryParams.getLevel())){
            orBoolQueryBuilder.filter(buildtermsQuery(queryParams.getLevel(), ZscalarConstants.LEVEL));
        }
        if(!queryParamValidation(queryParams.getTranscationID())){
            orBoolQueryBuilder.filter(buildtermsQuery(queryParams.getTranscationID(),ZscalarConstants.TRANSACTION_ID));
        }
        if(!queryParamValidation(queryParams.getLogDateStart()) || !queryParamValidation(queryParams.getLogDateEnd())){
            orBoolQueryBuilder.should(buildRangeQuery(queryParams.getLogDateStart(),queryParams.getLogDateEnd(),ZscalarConstants.LOG_DATE));
        }
        if(!queryParamValidation(queryParams.getMessage())){
            orBoolQueryBuilder.should(buildMatchPhraseQuery(queryParams.getMessage(),ZscalarConstants.MESSAGE));
        }
        if(!queryParamValidation(queryParams.getFileName())){
            orBoolQueryBuilder.should(buildMatchPhraseQuery(queryParams.getFileName(),ZscalarConstants.FILE_NAME));
        }
        return orBoolQueryBuilder;
    }

    private static QueryBuilder buildLogSearchQueryAnd(LogQueryParams queryParams) {
        BoolQueryBuilder andBoolQueryBuilder = QueryBuilders.boolQuery();
        if(!queryParamValidation(queryParams.getLevel())){
            andBoolQueryBuilder.must(buildtermsQuery(queryParams.getLevel(),ZscalarConstants.LEVEL));
        }
        if(!queryParamValidation(queryParams.getTranscationID())){
            andBoolQueryBuilder.must(buildtermsQuery(queryParams.getTranscationID(),ZscalarConstants.TRANSACTION_ID));
        }
        if(!queryParamValidation(queryParams.getLogDateStart()) || !queryParamValidation(queryParams.getLogDateEnd())){
            andBoolQueryBuilder.must(buildRangeQuery(queryParams.getLogDateStart(),queryParams.getLogDateEnd(),ZscalarConstants.LOG_DATE));
        }
        if(!queryParamValidation(queryParams.getMessage())){
            BoolQueryBuilder bool = QueryBuilders.boolQuery();
            bool.should(buildMatchPhraseQuery(queryParams.getMessage(),ZscalarConstants.MESSAGE));
            andBoolQueryBuilder.filter(bool);
        }
        if(!queryParamValidation(queryParams.getFileName())){
            andBoolQueryBuilder.must(buildMatchPhraseQuery(queryParams.getFileName(),ZscalarConstants.FILE_NAME));
        }
        return andBoolQueryBuilder;
    }

    private static boolean queryParamValidation(String param){
        return StringUtils.isEmpty(param);
    }

    private static boolean queryParamValidation(List<String> param){
        return param == null || param.isEmpty();
    }

    private static QueryBuilder buildMatchQuery(String phrase, String fieldName) {
        return QueryBuilders.matchQuery(fieldName,phrase);
    }

    private static QueryBuilder buildtermsQuery(List<String> paramList, String fieldName) {
        return QueryBuilders.termsQuery(fieldName,paramList);
    }

    private static QueryBuilder buildRangeQuery(String greaterThanParam, String lessThanParam, String fieldName) {
        return QueryBuilders.rangeQuery(fieldName).gte(greaterThanParam).lte(lessThanParam);
    }

    private static QueryBuilder buildMatchPhraseQuery(String phrase, String fieldName) {
        return QueryBuilders.matchPhraseQuery(fieldName,phrase);
    }

    public static <T> SearchRequest buildLogSearchRequest(LogQueryParams queryParams, QueryRequestWithPagination request, String operator,String indexName){
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //searchSourceBuilder.size(request.getLimit()); //max is 10000
        searchSourceBuilder.size(25);
        searchSourceBuilder.query(buildLogSearchQuery(queryParams,operator));
        searchRequest.indices(indexName);
        searchRequest.source(searchSourceBuilder);
        return searchRequest;
    }
}
