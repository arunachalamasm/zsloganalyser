package com.zscalar.log.reader.utility.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
public class SwaggerConfig {
	
	@Value("${app.host}")
	private String host;
	
	@Value("${swagger.port}")
	private String port;
	
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2) 
          .host(host+":"+port)
          .select()                                  
          .apis(RequestHandlerSelectors.any())              
          .paths(PathSelectors.any())                          
          .build();                                           
    }

}

