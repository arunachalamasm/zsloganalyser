package com.zscalar.log.reader.utility.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Iterables;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.sort.SortOrder;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Arrays;

public class QueryRequestWithPagination {

    @Min(value = 0)
    @Max(value = 10000)
    protected int limit;

    @Min(value = 0)
    @Max(value = 10000)
    protected int offset;

    @Size(min = 1, max = 320)
    protected String sortField;
    protected SortOrder sortOrder;

    @Size(max = 2000)
    protected QueryBuilder query;

    @Size(min = 1, max = 512)
    protected String[] fields;

    public QueryRequestWithPagination() {
        this(1000000, SortOrder.ASC);
    }

    public QueryRequestWithPagination(int limit, SortOrder sortOrder) {
        this.limit = limit;
        this.sortOrder = sortOrder;
    }

    public int getOffset() {
        return this.offset;
    }

    public int getLimit() {
        return this.limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getSortField() {
        return this.sortField;
    }

    public SortOrder getSortOrder() {
        return this.sortOrder;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }

    public QueryBuilder getQuery() {
        return this.query;
    }

    public void setQuery(QueryBuilder query) {
        this.query = query;
    }

    public String[] getFields() {
        return this.fields;
    }

    @JsonIgnore
    public void setFields(Iterable<String> fields) {
        this.fields = Iterables.toArray(fields, String.class);
    }

    @JsonProperty
    public void setFields(String... fields) {
        this.fields = ArrayUtils.clone(fields);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("limit", limit)
                .append("offset", offset)
                .append("sortField", sortField)
                .append("sortOrder", sortOrder)
                .append("query", query)
                .append("fields", Arrays.toString(fields))
                .toString();
    }
}
