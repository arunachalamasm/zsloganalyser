package com.zscalar.log.reader.utility.dto.response;

import java.io.Serializable;
import lombok.Data;

@Data
public class ZipUploadResponse implements Serializable {

	private static final long SerialVersionUID = 194786l;

	private String message;
	
	private String reason;
	
	private Boolean error = Boolean.FALSE;
	
	private Integer status;
	
	//private Object data;

}
