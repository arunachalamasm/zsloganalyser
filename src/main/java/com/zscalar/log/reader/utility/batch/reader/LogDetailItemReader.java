package com.zscalar.log.reader.utility.batch.reader;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.PassThroughLineMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.zscalar.log.reader.utility.constants.ZscalarConstants;
import com.zscalar.log.reader.utility.domain.ConfigDetails;
import com.zscalar.log.reader.utility.utils.DateUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@StepScope
public class LogDetailItemReader implements ItemReader<String>, ItemStream{

	private FlatFileItemReader<String> reader;

	private Resource resource;
	
	@Autowired
	private ConfigDetails configDetails;

	public LogDetailItemReader( FlatFileItemReader reader) {
		reader.setLineMapper(new PassThroughLineMapper());
		this.reader = reader;
	}

	private StringBuffer oldLog = new StringBuffer();

	@Override
	public String read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		log.debug("In reader");
		String log = "";
		String newLog;
		boolean isForwardingProfile=false;
		boolean isForwardingProfileFlag=false;
		List<String> lst = new ArrayList<>();
		
		while ((newLog = reader.read()) != null) {
			if (newLog.toLowerCase().contains(ZscalarConstants.FORWARDING_PROFILE)) {
				isForwardingProfile = true;
			} else if (isForwardingProfile && newLog.contains(ZscalarConstants.SPACE)
					&& DateUtil.isDate(newLog.substring(0, newLog.indexOf(' ')))) {
				if(isForwardingProfile) {
					if(null == configDetails.getForwardingProfile())
						configDetails.setForwardingProfile(Arrays.asList(oldLog.toString()));
					else {
						lst.addAll(configDetails.getForwardingProfile());
						lst.add(oldLog.toString());
						configDetails.setForwardingProfile(lst);
					}
				}
				isForwardingProfile = false;
				continue;
			}
			if (newLog.contains(ZscalarConstants.SPACE) && DateUtil.isDate(newLog.substring(0, newLog.indexOf(' '))) && oldLog.length() > 0) {
				log = oldLog.toString();
				oldLog.delete(0, oldLog.length());
				oldLog.append(newLog);
				return resource.getFilename()+ZscalarConstants.SPACE+log;
			}
			oldLog.append(newLog);
		}
		if (oldLog.length() > 0) {
			log = oldLog.toString();
			oldLog.delete(0, oldLog.length());
			return resource.getFilename()+ZscalarConstants.SPACE+log;
		}
		return null;
	}

	@Override
	public void open(ExecutionContext executionContext) throws ItemStreamException {
		
		Object file = executionContext.get(ZscalarConstants.FILE_NAME);
		if(file instanceof String) {
			resource = new FileSystemResource(new File((String)file));
			reader.setResource(resource);
		}
		reader.open(executionContext);
	}

	@Override
	public void update(ExecutionContext executionContext) throws ItemStreamException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void close() throws ItemStreamException {
		// TODO Auto-generated method stub
		
	}
}
