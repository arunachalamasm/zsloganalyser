package com.zscalar.log.reader.utility.dto;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class CustomJobParameter implements Serializable {
	
	private List<File> files;
	
	private String zipFileName;

	private String indexName;
	
	private String obsFlag;
	
	public List<File> getResources() {
		return files;
	}

	public CustomJobParameter(List<File> files, String zipFileName, String indexName) {
		this.files = files;
		this.zipFileName = zipFileName;
		this.indexName = indexName;
	}

	public String getZipFileName() {
		return zipFileName;
	}

	public String getIndexName() {
		return indexName;
	}

	@Override
	public String toString() {
		return "CustomJobParameter{" +
				"zipFileName='" + zipFileName + '\'' +
				", indexName='" + indexName + '\'' +
				'}';
	}
}
