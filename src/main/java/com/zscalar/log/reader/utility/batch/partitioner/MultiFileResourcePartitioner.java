package com.zscalar.log.reader.utility.batch.partitioner;

import com.zscalar.log.reader.utility.dto.CustomJobParameter;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class MultiFileResourcePartitioner implements Partitioner {

	private Resource[] resources;

	@Value("#{jobParameters[customparam]}")
	public CustomJobParameter source;

	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {

		if (source instanceof CustomJobParameter) {
			List<File> files = source.getResources();
			resources = convertResourceFromFile(files);
			setResources(resources);
		}

		Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>(resources.length);
		AtomicInteger i = new AtomicInteger();
		Stream.of(resources).parallel().forEach( resource -> {
			ExecutionContext context = new ExecutionContext();
			// give fileName for ExecutionContext
			context.putString("fileName", resource.getFilename());
			//zipFileName
			context.putString("zipFileName", source.getZipFileName());
			// give a thread name for ExecutionContext
			context.putString("name", "Thread" + i);
			partitionMap.put("partition: " + i + ": " + resource.getFilename(), context);
			i.getAndIncrement();
		});
		return partitionMap;
	}

	private Resource[] convertResourceFromFile(List<File> logFiles) {
		Resource[] resources = new FileSystemResource[logFiles.size()];
		int fileIndex = 0;
		for (File logFile : logFiles) {
			resources[fileIndex] = new FileSystemResource(logFile);
			fileIndex++;
		}
		return resources;
	}

	public void setResources(Resource[] resources) {
		this.resources = resources;
	}

	public Resource[] getResources() {
		return resources;
	}

}
