package com.zscalar.log.reader.utility.constants;

public enum TypeEnum {

	ZSAAuth("Client Connector Auth"), ZSAService("Tray Manager"), ZSATray("Tray Manager"),
	ZSATrayHelper("Tray Manager"), ZSATrayManager("Tray Manager"), ZSAUpdate("Tunnel"), ZSATunnel("Tunnel");

	String type;

	TypeEnum(String type) {
		this.type = type;
	}
	
	public String getType() {
        return type;
    }
}
