package com.zscalar.log.reader.utility.batch.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.zscalar.log.reader.utility.domain.LogDetail;
import com.zscalar.log.reader.utility.domain.LogObject;
import com.zscalar.log.reader.utility.dto.CustomJobParameter;
import com.zscalar.log.reader.utility.repository.ESRepository;

import lombok.extern.slf4j.Slf4j;
import net.lingala.zip4j.ZipFile;

@Slf4j
@StepScope
public class LogDetailItemWriter implements ItemWriter<LogObject> {

	@Autowired
	private ESRepository eSRepository;

	@Value("#{jobParameters[customparam]}")
	public CustomJobParameter source;
	
	@Value("${path.obfuscate}")
	public String obfuscatePath;

	List<LogDetail> tList = new ArrayList<>();
	Map<String, List<String>> map = new HashMap<>();

	@Override
	public void write(List<? extends LogObject> items) throws Exception {
		if(!items.isEmpty()) {
			log.debug("In writer");
			List<String> obsString = new ArrayList<>();
			items.forEach(l -> {
				tList.add(l.getLogDetail());
				if (source.getObsFlag().equalsIgnoreCase("true")) {
					int firstSpaceIndex = StringUtils.ordinalIndexOf(l.getOfsLine(), " ", 1);
					String key = l.getOfsLine().substring(0, firstSpaceIndex);
					if (map.containsKey(key)) {
						List<String> obsStringLst = map.get(key);
						obsStringLst.add(l.getOfsLine().substring(firstSpaceIndex, l.getOfsLine().length()));
					} else {
						obsString.add(l.getOfsLine().substring(firstSpaceIndex, l.getOfsLine().length()));
						map.put(l.getOfsLine().substring(0, firstSpaceIndex), obsString);
					}
				}
			});

			eSRepository.saveAll(tList, "user_" + source.getIndexName());

			if (source.getObsFlag().equalsIgnoreCase("true")) {
				String zipFileName = source.getZipFileName().substring(0, source.getZipFileName().length() - 4);
				// String dirPath = "D:\\softwares\\perl\\java\\_" + zipFileName;
				String dirPath = "_" + zipFileName;
				new File(dirPath).mkdirs();
				map.forEach((k, v) -> {
					try {
						FileWriter writer = new FileWriter(dirPath + "/_" + k);

						BufferedWriter buffer = new BufferedWriter(writer);
						if (!v.isEmpty()) {
							v.forEach(value -> {
								try {
									buffer.write(value);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							});

						}

						buffer.close();
					} catch (Exception e) {
						log.error("Error while writing file for data to the file {} in the zipFile {}", k,
								source.getZipFileName());
					}
				});

				ZipFile zipfile = new ZipFile(new File(obfuscatePath + zipFileName) + ".zip");
				zipfile.addFolder(new File("_" + zipFileName));
			}
		}

	}

}
