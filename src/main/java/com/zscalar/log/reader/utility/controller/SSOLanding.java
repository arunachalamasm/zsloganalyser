package com.zscalar.log.reader.utility.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import com.zscalar.log.reader.utility.exception.ErrorCode;
import com.zscalar.log.reader.utility.exception.LogReaderException;
import org.opensaml.saml2.core.Attribute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.providers.ExpiringUsernameAuthenticationToken;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.zscalar.log.reader.utility.constants.ZscalarConstants;
import com.zscalar.log.reader.utility.dto.SSOUser;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/auth/saml/")
public class SSOLanding {

    @Value("${admin.dashboard}")
    private String adminURL;

    @Value("${user.dashboard}")
    private String userURL;

    @Autowired
    HttpServletResponse response;

    //to send the user in cookie
    @RequestMapping("/SSO/dashboard")
    public ResponseEntity<?> localRedirect(ExpiringUsernameAuthenticationToken userToken) {
        log.trace("Redirection to dashboard on level access");
        try {
            SAMLCredential credential = (SAMLCredential) userToken.getCredentials();
            Cookie cookie = new Cookie("user", credential.getNameID().getValue());
            cookie.setPath("/");
            response.addCookie(cookie);
            if ((getUserAccess(userToken)).equalsIgnoreCase(ZscalarConstants.ADMIN)) {
                log.trace("User has admin access");
                log.info("Redirected to admin dashboard");
                response.sendRedirect(adminURL);//dashboard for admin for UI
            } else {
                log.trace("User has user access ");
                log.info("Redirected to user dashboard");
                response.sendRedirect(userURL);//dashboard for user from UI
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (IOException e) {
            throw new LogReaderException(e, ErrorCode.INPUT_OUTPUT_EXCEPTION);
        }
    }

    SSOUser user = new SSOUser();

    //method to get access level of a user
    public String getUserAccess(ExpiringUsernameAuthenticationToken userToken) {
        log.trace("Getting access level for authenticated user");
        SAMLCredential credential = (SAMLCredential) userToken.getCredentials();
        List<Attribute> attributes = credential.getAttributes();
        for (Attribute data : attributes) {
            if (data.getName().equalsIgnoreCase(ZscalarConstants.ADMIN)) {
                user.setGroup(ZscalarConstants.SET_ADMIN);
            }
            if (data.getName().equalsIgnoreCase(ZscalarConstants.USER)) {
                user.setGroup(ZscalarConstants.SET_USER);
            }
        }
        return user.getGroup();
    }
}
