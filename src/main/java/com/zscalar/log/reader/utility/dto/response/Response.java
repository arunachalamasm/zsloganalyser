package com.zscalar.log.reader.utility.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {

    protected Object results;
    protected Long totalCount;
    protected Integer resultCount;

    public Response(final Object results) {
        setResults(results);
    }

    public Response() {
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public Integer getResultCount() {
        return resultCount;
    }

    public Object getResults() {
        return results;
    }

    public Response setTotalCount(final long totalCount) {
        this.totalCount = totalCount;
        return this;
    }

    public Response setResultCount(final int resultCount) {
        this.resultCount = resultCount;
        return this;
    }

    public Response setResults(final Object results) {
        this.results = results;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("results", results)
                .append("totalCount", totalCount)
                .append("resultCount", resultCount)
                .toString();
    }
}
