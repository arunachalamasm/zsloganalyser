package com.zscalar.log.reader.utility.domain;

import com.zscalar.log.reader.utility.annotation.Document;
import lombok.Data;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Data
@Document(indexName = "config_detail")
public class ConfigDetails {
	
	List<String> AppInfo;
	
	List<String> SystemInfo;
	
	List<String> ForwardingProfile;

}
