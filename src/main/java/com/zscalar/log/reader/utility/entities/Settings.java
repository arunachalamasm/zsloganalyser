package com.zscalar.log.reader.utility.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Settings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "user")
    private String user;
    @Column(name = "name")
    private String name;
    @Column(name = "content",columnDefinition = "json")
    private String content;

    @CreationTimestamp
    @Column(name="created",columnDefinition = "TIMESTAMP")
    private LocalDateTime created;

    @UpdateTimestamp
    @Column(name="updated",columnDefinition = "TIMESTAMP")
    private LocalDateTime updated;

    public void setId(long l) {
    }
}
