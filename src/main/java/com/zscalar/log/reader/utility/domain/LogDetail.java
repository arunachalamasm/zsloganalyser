package com.zscalar.log.reader.utility.domain;

import com.zscalar.log.reader.utility.annotation.Document;
import lombok.Data;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@Document(indexName = "")
public class LogDetail {

	@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "uuuu-MM-dd HH:mm:ss.SSSZZ")
	private Date dateTime;

	private String processId;

	private String threadId;

	private String logLevel;

	private Map<String, String> messageData;

	private Boolean hasError=Boolean.FALSE;

	private String fileName;

	private String logLine;

	private ErrorToken errorTokens;

	private List<String> LogFileType;

	private String zipFileName;

	@Override
	public String toString() {
		return "LogDetail [dateTime=" + dateTime + ", processId=" + processId + ", threadId=" + threadId + ", logLevel="
				+ logLevel + ", messageData=" + messageData + ", hasError=" + hasError + ", fileName=" + fileName
				+ ", logLine=" + logLine + ", errorTokens=" + errorTokens + ", LogFileType=" + LogFileType
				+ ", zipFileName=" + zipFileName + "]";
	}
}
