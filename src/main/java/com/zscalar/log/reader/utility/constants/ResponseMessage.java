package com.zscalar.log.reader.utility.constants;

import org.springframework.stereotype.Component;

@Component
public class ResponseMessage {

	public static final String RESPONSE_500 = "Internal Server Error";

	public static final String RESPONSE_409 = "Conflict";

	public static final String RESPONSE_400 = "Bad Request";

	public static final String RESPONSE_404 = "Not Found";

	public static final String SUCCESS_RESPONSE = "Success";

	public static final String FAILURE_RESPONSE = "Failure";


}
