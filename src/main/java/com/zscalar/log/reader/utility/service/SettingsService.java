package com.zscalar.log.reader.utility.service;


import java.util.List;

import com.zscalar.log.reader.utility.dto.SettingsDto;
import com.zscalar.log.reader.utility.entities.Settings;

public interface SettingsService {

    boolean existsByUserAndName(String user, String name);

    String saveSetting(SettingsDto settingsDto);

    Object getSetting(String user, String name);

    String updateSetting(SettingsDto settingsDto);

    List<Settings> getAllSetting();

    Settings getSetting(String name);
}
