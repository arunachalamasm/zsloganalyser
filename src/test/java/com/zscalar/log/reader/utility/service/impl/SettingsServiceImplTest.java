package com.zscalar.log.reader.utility.service.impl;

import com.zscalar.log.reader.utility.dto.SettingsDto;
import com.zscalar.log.reader.utility.entities.Settings;
import com.zscalar.log.reader.utility.repository.SettingsRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class SettingsServiceImplTest {
    @Mock
    private SettingsRepo settingsRepo;

    @InjectMocks
    private SettingsServiceImpl settingsServiceImpl;

    @Test
    public void existsByUserAndNameTest() throws Exception {
        Settings settings = new Settings();
        settings.setId(1L);
        settings.setUser("Admin");
        settings.setName("SamlSetting");
        settings.setContent("This is content");
        settings.setCreated(LocalDateTime.now());
        settings.setUpdated(LocalDateTime.now());

        when(settingsRepo.existsByUserAndName("Admin", "SamlSetting")).thenReturn(true);

        boolean result = settingsServiceImpl.existsByUserAndName("Admin", "SamlSetting");

        assertEquals(true, result);
    }
    @Test
    public void existsByUserAndNameIfNot() throws Exception {
        Settings settings = new Settings();
        settings.setId(1L);
        settings.setUser("Admin");
        settings.setName("SamlSetting");
        settings.setContent("This is content");
        settings.setCreated(LocalDateTime.now());
        settings.setUpdated(LocalDateTime.now());

        when(settingsRepo.existsByUserAndName("Admin", "SamlSetting")).thenReturn(true);

        boolean result = settingsServiceImpl.existsByUserAndName("User", "SamlSetting");

        assertEquals(false, result);
    }
    @Test
    public void getSettingTest() throws Exception{
        Settings settings = new Settings();
        settings.setId(1L);
        settings.setUser("Admin");
        settings.setName("SamlSetting");
        settings.setContent("This is content");
        settings.setCreated(LocalDateTime.now());
        settings.setUpdated(LocalDateTime.now());

        when(settingsRepo.existsByUserAndName("Admin","SamlSetting")).thenReturn(true);
        when(settingsRepo.findByUserAndName("Admin","SamlSetting")).thenReturn(Optional.of(settings));

        Settings result = settingsServiceImpl.getSetting("Admin","SamlSetting");

        assertEquals(settings,result);
    }
    @Test
    public void saveSettingTest() throws Exception {
        Settings settings = new Settings();
        settings.setId(1L);
        settings.setUser("Admin");
        settings.setName("SamlSetting");
        settings.setContent("This is content");
        settings.setCreated(LocalDateTime.now());
        settings.setUpdated(LocalDateTime.now());

        SettingsDto dto = new SettingsDto("Admin","SamlSetting","This is content");

        when(settingsRepo.save(settings)).thenReturn(settings);

        String result = settingsServiceImpl.saveSetting(dto);

        assertEquals("Settings Saved",result);
    }
    @Test
    public void UpdateSettingTest() throws Exception {
        Settings settings = new Settings();
        settings.setId(1L);
        settings.setUser("Admin");
        settings.setName("SamlSetting");
        settings.setContent("This is content");
        settings.setCreated(LocalDateTime.now());
        settings.setUpdated(LocalDateTime.now());

        SettingsDto dto = new SettingsDto("Admin","SamlSetting","This is content1");

        when(settingsRepo.existsByUserAndName("Admin","SamlSetting")).thenReturn(true);
        when(settingsRepo.save(settings)).thenReturn(settings);

        String result = settingsServiceImpl.saveSetting(dto);

        assertEquals("Settings Saved",result);
    }
    @Test
    public void getAllSettingsTest() throws Exception {
        List<Settings> list = new ArrayList<>();
        Settings settings = new Settings();
        settings.setId(1L);
        settings.setUser("Admin");
        settings.setName("SamlSetting");
        settings.setContent("This is content");
        settings.setCreated(LocalDateTime.now());
        settings.setUpdated(LocalDateTime.now());
        list.add(settings);

        Settings settings1 = new Settings();
        settings1.setId(1L);
        settings1.setUser("User");
        settings1.setName("ErrorToken");
        settings1.setContent("This is content");
        settings1.setCreated(LocalDateTime.now());
        settings1.setUpdated(LocalDateTime.now());
        list.add(settings1);

        when(settingsRepo.findAll()).thenReturn(list);

        List<Settings> result = settingsServiceImpl.getAllSetting();

        assertEquals(list,result);
    }
}

