/*
 * package com.zscalar.log.reader.utility.repository;
 * 
 * import static org.mockito.Mockito.mock; import static
 * org.mockito.Mockito.when;
 * 
 * import java.io.IOException; import java.util.ArrayList; import
 * java.util.HashMap; import java.util.List; import java.util.Map; import
 * java.util.UUID;
 * 
 * import org.elasticsearch.action.bulk.BulkRequest; import
 * org.elasticsearch.action.bulk.BulkResponse; import
 * org.elasticsearch.action.index.IndexRequest; import
 * org.elasticsearch.action.index.IndexResponse; import
 * org.elasticsearch.action.support.WriteRequest; import
 * org.elasticsearch.client.IndicesClient; import
 * org.elasticsearch.client.RequestOptions; import
 * org.elasticsearch.client.RestHighLevelClient; import
 * org.elasticsearch.client.indices.CreateIndexRequest; import
 * org.elasticsearch.client.indices.CreateIndexResponse; import
 * org.elasticsearch.client.indices.GetIndexRequest; import
 * org.elasticsearch.common.settings.Settings; import
 * org.elasticsearch.common.xcontent.XContentType; import
 * org.elasticsearch.index.query.QueryBuilders; import
 * org.elasticsearch.index.reindex.BulkByScrollResponse; import
 * org.elasticsearch.index.reindex.DeleteByQueryRequest; import org.junit.Test;
 * import org.junit.runner.RunWith; import org.mockito.InjectMocks; import
 * org.mockito.Mock; import
 * org.springframework.test.context.junit4.SpringRunner;
 * 
 * import com.google.gson.Gson; import
 * com.zscalar.log.reader.utility.config.ElasticsearchConfig; import
 * com.zscalar.log.reader.utility.domain.LogDetail;
 * 
 * @RunWith(SpringRunner.class) public class ESRepositoryTest {
 * 
 * @Mock private ElasticsearchConfig elasticSearchClient;
 * 
 * @Mock private RestHighLevelClient client;
 * 
 * @Mock private Gson gson;
 * 
 * @InjectMocks ESRepository eSRepository;
 * 
 * @Test public void testDeleteAllIndexDocument() throws IOException {
 * BulkByScrollResponse response = null; String indexName = "log_detail";
 * DeleteByQueryRequest request = new
 * DeleteByQueryRequest(indexName).setQuery(QueryBuilders.matchAllQuery());
 * request.setRefresh(true); request.setBatchSize(100);
 * when(client.deleteByQuery(request,
 * RequestOptions.DEFAULT)).thenReturn(response);
 * eSRepository.deleteAllIndexDocument(LogDetail.class);
 * 
 * }
 * 
 * @Test public void testSave() throws IOException {
 * 
 * LogDetail index = new LogDetail(); index.setId(UUID.randomUUID().toString());
 * 
 * String indexName = "log_detail"; IndexResponse response = null; String
 * jsonString = "{}";
 * 
 * when(gson.toJson(index)).thenReturn(jsonString);
 * 
 * IndexRequest indexRequest = new IndexRequest(indexName);
 * indexRequest.id(index.getId()); indexRequest.source(gson.toJson(index),
 * XContentType.JSON);
 * indexRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
 * 
 * when(client.index(indexRequest,
 * RequestOptions.DEFAULT)).thenReturn(response); eSRepository.save(index,
 * LogDetail.class);
 * 
 * }
 * 
 * @Test public void testSaveAll() throws IOException {
 * 
 * List<LogDetail> logDetails = new ArrayList<>(); LogDetail logDetail = new
 * LogDetail(); logDetail.setId(UUID.randomUUID().toString());
 * logDetails.add(logDetail);
 * 
 * String indexName = "log_detail"; String jsonString = "{}";
 * when(gson.toJson(logDetail)).thenReturn(jsonString);
 * 
 * BulkRequest bulkRequest = new BulkRequest();
 * 
 * BulkResponse bulk = null; IndexRequest indexRequest = new
 * IndexRequest(indexName); indexRequest.id(logDetail.getId());
 * indexRequest.source(gson.toJson(logDetail), XContentType.JSON);
 * bulkRequest.add(indexRequest);
 * 
 * bulkRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
 * when(client.bulk(bulkRequest, RequestOptions.DEFAULT)).thenReturn(bulk);
 * eSRepository.saveAll(logDetails, LogDetail.class);
 * 
 * }
 * 
 * @Test public void testCreateNewIndexWithExistingIndex() throws IOException {
 * Map<String, Object> mapping = new HashMap<>(); String indexName =
 * "log_detail";
 * 
 * GetIndexRequest getRequest = new GetIndexRequest(indexName);
 * getRequest.humanReadable(true); getRequest.includeDefaults(false);
 * 
 * IndicesClient indices = mock(IndicesClient.class);
 * when(client.indices()).thenReturn(indices);
 * when(client.indices().exists(getRequest,
 * RequestOptions.DEFAULT)).thenReturn(Boolean.TRUE);
 * eSRepository.createNewIndex(LogDetail.class, mapping);
 * 
 * }
 * 
 * @Test public void testCreateNewIndexWithNonExistIndex() throws IOException {
 * 
 * Map<String, Object> mapping = new HashMap<>(); String indexName =
 * "log_detail";
 * 
 * GetIndexRequest getRequest = new GetIndexRequest(indexName);
 * getRequest.humanReadable(true); getRequest.includeDefaults(false);
 * 
 * CreateIndexRequest createRequest = new CreateIndexRequest(indexName);
 * createRequest.settings(Settings.builder().put("index.number_of_shards",
 * 1).put("index.number_of_replicas", 1)); createRequest.mapping(mapping);
 * 
 * CreateIndexResponse indexResponse = null; IndicesClient indices =
 * mock(IndicesClient.class); when(client.indices()).thenReturn(indices);
 * when(client.indices().exists(getRequest,
 * RequestOptions.DEFAULT)).thenReturn(Boolean.FALSE);
 * when(client.indices().create(createRequest,
 * RequestOptions.DEFAULT)).thenReturn(indexResponse);
 * eSRepository.createNewIndex(LogDetail.class, mapping);
 * 
 * } }
 */