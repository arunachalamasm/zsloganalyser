package com.zscalar.log.reader.utility.controller;

import com.zscalar.log.reader.utility.dto.SettingsDto;
import com.zscalar.log.reader.utility.entities.Settings;
import com.zscalar.log.reader.utility.service.impl.SettingsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class SettingsControllerTest {
    @InjectMocks
    private SettingsController settingsController;
    @Mock
    private SettingsServiceImpl settingsService;

    @Test
    public void getSettingTest() {
        Settings settings = new Settings();
        settings.setId(1L);
        settings.setUser("Admin");
        settings.setName("SamlSetting");
        settings.setContent("This is content");
        settings.setCreated(LocalDateTime.now());
        settings.setUpdated(LocalDateTime.now());

        when(settingsService.getSetting("Admin", "SamlSetting")).thenReturn(settings);

        ResponseEntity<Settings> result = settingsController.getSetting("Admin", "SamlSetting");

        assertEquals(settings, result.getBody());
    }
    @Test
    public void postSettingTest(){
        Settings settings = new Settings();
        settings.setId(1L);
        settings.setUser("Admin");
        settings.setName("SamlSetting");
        settings.setContent("This is content");
        settings.setCreated(LocalDateTime.now());
        settings.setUpdated(LocalDateTime.now());

        SettingsDto dto = new SettingsDto("Admin","SamlSetting","This is content");

        when(settingsService.existsByUserAndName("Admin","SamlSetting")).thenReturn(false);
        when(settingsService.saveSetting(dto)).thenReturn("Settings Saved");

        ResponseEntity<String> result = settingsController.postMethod(dto);

        assertEquals("Settings Saved",result.getBody());

    }
    @Test
    public void postSettingTestForUpdate(){
        Settings settings = new Settings();
        settings.setId(1L);
        settings.setUser("Admin");
        settings.setName("SamlSetting");
        settings.setContent("This is content");
        settings.setCreated(LocalDateTime.now());
        settings.setUpdated(LocalDateTime.now());

        SettingsDto dto = new SettingsDto("Admin","SamlSetting","This is content");

        when(settingsService.existsByUserAndName("Admin","SamlSetting")).thenReturn(true);
        when(settingsService.updateSetting(dto)).thenReturn("Settings Saved");

        ResponseEntity<String> result = settingsController.postMethod(dto);

        assertEquals("Settings Saved",result.getBody());

    }

}