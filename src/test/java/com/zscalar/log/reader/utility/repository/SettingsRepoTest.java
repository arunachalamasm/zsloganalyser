package com.zscalar.log.reader.utility.repository;

import com.zscalar.log.reader.utility.entities.Settings;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SettingsRepoTest {

    @Autowired
    private SettingsRepo settingsRepo;

    @Before
    public void setUp() throws Exception {
        List<Settings> settingsList = Arrays.asList(
                new Settings(1L,"user","samlSetting","samlContent", LocalDateTime.now(),LocalDateTime.now()),
                new Settings(1L,"Admin","ErrorTokens","errorTokenContent", LocalDateTime.now(),LocalDateTime.now())
        );
        settingsRepo.saveAll(settingsList);
    }
    @After
    public void tearDown() throws Exception {
        settingsRepo.deleteAll();
    }

    @Test
    public void existsByUserAndNameBasicTest(){
        boolean result = settingsRepo.existsByUserAndName("Admin","ErrorTokens");
        assertTrue(result);

    }

    @Test
    public void existsByUserAndNameNotFound(){
        boolean result = settingsRepo.existsByUserAndName("User","ErrorTokens");
        assertFalse(result);
    }

    @Test
    public void findByUserAndNameBasic(){
        Optional<Settings> result = settingsRepo.findByUserAndName("Admin","ErrorTokens");
        assertTrue(result.isPresent());
    }

    @Test
    public void findByUserAndNameNotFound(){
        Optional<Settings> result = settingsRepo.findByUserAndName("Admin","samlSetting");
        assertFalse(result.isPresent());
    }
}
